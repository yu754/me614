import os
import sys
import numpy as np
import scipy.sparse as scysparse
from time import sleep
import spatial_discretization
import scipy.sparse as scysparse
import scipy.sparse.linalg as spysparselinalg  # sparse linear algebra
import scipy.linalg as scylinalg               # non-sparse linear algebra
import pylab as plt
from matplotlib import rc as matplotlibrc
from pdb import set_trace
# Figure settings
matplotlibrc('text.latex', preamble='\usepackage{color}')
matplotlibrc('text',usetex=True)
matplotlibrc('font', family='serif')

fig_sol =0
fig_spy =1
figwidth       = 8
figheight      = 6
lineWidth      = 1.5
textFontSize   = 28*0.8
gcafontSize    = 30*0.8

def u_initial(X):
		return c1*np.sin(w1*X-r1)-c2*np.cos(w2*X-r2) # lambda functions are better..
def u_ana(X,t):
		return c1*np.exp(-w1**2*alpha*t)*np.sin(w1*(X-c_x*t)-r1)-c2*np.exp(-w2**2*alpha*t)*np.cos(w2*(X-c_x*t)-r2) # lambda functions are better..
#########################################

machine_epsilon = np.finfo(float).eps

### this example script is hard-coded for periodic problems

#########################################
############### User Input ##############

#N_list=np.array([10,100,1000,10000])
N_list=np.array([10,30,100,300,1000])
ee=0.000125
dt_list=np.array([1000,100,10,3.,1.,0.3,0.03,])*ee
diffusion_scheme = "2nd-order-central"

t_dic={}
t_dic[str(0)]="Explicit-Euler"
t_dic[str(1)]="Crank-Nicolson"



x_dic={}
x_dic[str(0)]="1st-order-upwind"
x_dic[str(1)]="2nd-order-upwind"
x_dic[str(2)]="2nd-order-central"

legt_dic={}
legt_dic[str(0)]="EE"
legt_dic[str(1)]="CN"
legx_dic={}
legx_dic[str(0)]="1st_upwind"
legx_dic[str(1)]="2nd_upwind"
legx_dic[str(2)]="2nd_cds"
Lx  = 1.
CFL = 1.# Courant-Friedrichs-Lewy number (i.e. dimensionless time step)
c_x_ref = 1.0
c_x   = 1.*c_x_ref  # (linear) convection speed
alpha = 0.2    # diffusion coefficients

#Lx/(c_x_ref+machine_epsilon) # one complete cycle

# At time = 0.175, the max value reached 0.6
w1=2.*np.pi
w2=4.*np.pi
c1=0.5
c2=0.5
r1=0.2*np.pi
r2=0.2*np.pi
Tf  = (w2**2*alpha)**(-1)
plot_every  = 100


rms_x=np.zeros(N_list.shape)
rms_t=np.zeros(dt_list.shape)
if False:
	for xs in range(0,2):
		for iN,Nx in enumerate(N_list):

			advection_scheme=x_dic[str(xs)]	
			#Nx=N_list
			xx = np.linspace(0.,1,Nx+1)
			# actual mesh points are off the boundaries x=0, x=Lx
			# non-periodic boundary conditions created with ghost points
			x_mesh = 0.5*(xx[:-1]+xx[1:])
			dx  = np.diff(xx)[0]
			dx2 = dx*dx
			dt=2e-06

			# for linear advection/diffusion time step is a function
			# of c,alpha,dx only; we use reference limits, ballpark
			# # estimates for Explicit Euler
			# dt_max_advective = dx/(c_x+machine_epsilon)             #   think of machine_epsilon as zero
			# dt_max_diffusive = dx2/(alpha+machine_epsilon)
			# dt_max = np.min([dt_max_advective,dt_max_diffusive])
			# dt = CFL*dt_max
			print dt
			set_trace()


			Ieye = scysparse.identity(Nx)
			print "Generate_Spatial_Operators"
			time_advancement="Crank-Nicolson"
			# Creating first derivative
			Dx = spatial_discretization.Generate_Spatial_Operators(\
															x_mesh,advection_scheme,derivation_order=1)
			# Creating second derivative
			D2x2 = spatial_discretization.Generate_Spatial_Operators(\
															x_mesh,diffusion_scheme,derivation_order=2)	


			if time_advancement=="Explicit-Euler":
				#print "222"
				A = Ieye
				B = Ieye-dt*c_x*Dx+dt*alpha*D2x2
			if time_advancement=="Crank-Nicolson":
				#print "?????"
				adv_diff_Op = -dt*c_x*Dx+dt*alpha*D2x2
				A = Ieye-0.5*adv_diff_Op
				B = Ieye+0.5*adv_diff_Op
				#set_trace()

			A , B = scysparse.csr_matrix(A),scysparse.csr_matrix(B)
			u = u_initial(x_mesh)
			time = 0.
			it   = 0
			while time < Tf:

			    it   += 1
			    time += dt
			   
			    print "Iteration = " + repr(it)	   
			    u = spysparselinalg.spsolve(A,B.dot(u))

			rms_x[iN]=(np.sum((u_ana(x_mesh,Tf)-u)**2 )/Nx   )**0.5
		#set_trace()
		fname="pp2_d_rmsx"+legx_dic[str(xs)] +".dat"	
		fpath='./../data/'
		fout = open(fpath+fname, 'w')
		for k in range(len(rms_x)):
		     fout.write(str(rms_x[k])+" "+"\n")
		fout.close()







if True:
	for xs in range(3):
		for iT,dt in enumerate(dt_list):

			advection_scheme=x_dic[str(xs)]	
			Nx=200
			xx = np.linspace(0.,1,Nx+1)
			# actual mesh points are off the boundaries x=0, x=Lx
			# non-periodic boundary conditions created with ghost points
			x_mesh = 0.5*(xx[:-1]+xx[1:])
			dx  = np.diff(xx)[0]
			dx2 = dx*dx


			# for linear advection/diffusion time step is a function
			# of c,alpha,dx only; we use reference limits, ballpark
			# estimates for Explicit Euler
			# dt_max_advective = dx/(c_x+machine_epsilon)             #   think of machine_epsilon as zero
			# dt_max_diffusive = dx2/(alpha+machine_epsilon)
			# dt_max = np.min([dt_max_advective,dt_max_diffusive])
			# dt = CFL*dt_max
			# print dt
			# set_trace()


			Ieye = scysparse.identity(Nx)
			print "Generate_Spatial_Operators"
			time_advancement="Crank-Nicolson"
			# Creating first derivative
			Dx = spatial_discretization.Generate_Spatial_Operators(\
															x_mesh,advection_scheme,derivation_order=1)
			# Creating second derivative
			D2x2 = spatial_discretization.Generate_Spatial_Operators(\
															x_mesh,diffusion_scheme,derivation_order=2)	


			if time_advancement=="Explicit-Euler":
				#print "222"
				A = Ieye
				B = Ieye-dt*c_x*Dx+dt*alpha*D2x2
			if time_advancement=="Crank-Nicolson":
				#print "?????"
				adv_diff_Op = -dt*c_x*Dx+dt*alpha*D2x2
				A = Ieye-0.5*adv_diff_Op
				B = Ieye+0.5*adv_diff_Op
				#set_trace()

			A , B = scysparse.csr_matrix(A),scysparse.csr_matrix(B)
			u = u_initial(x_mesh)
			time = 0.
			it   = 0
			while time < Tf:

			    it   += 1
			    time += dt
			   
			    print "Iteration = " + repr(it)	   
			    u = spysparselinalg.spsolve(A,B.dot(u))

			rms_t[iT]=(np.sum((u_ana(x_mesh,Tf)-u)**2 )/Nx   )**0.5
		#set_trace()
		fname="pp2_d_rmst"+legx_dic[str(xs)] +".dat"	
		fpath='./../data/'
		fout = open(fpath+fname, 'w')
		for k in range(len(rms_t)):
		     fout.write(str(rms_t[k])+" "+"\n")
		fout.close()	



