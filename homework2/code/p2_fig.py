import os
import sys
import numpy as np
import scipy.sparse as scysparse
from time import sleep
import spatial_discretization
import scipy.sparse as scysparse
import scipy.sparse.linalg as spysparselinalg  # sparse linear algebra
import scipy.linalg as scylinalg               # non-sparse linear algebra
import pylab as plt
from matplotlib import rc as matplotlibrc
from pdb import set_trace
# Figure settings
matplotlibrc('text.latex', preamble='\usepackage{color}')
matplotlibrc('text',usetex=True)
matplotlibrc('font', family='serif')

fig_sol =0
fig_spy =1
figwidth       = 8
figheight      = 6
lineWidth      = 1.5
textFontSize   = 28*0.8
gcafontSize    = 30*0.8

fname="pp2_d_rmsx1st_upwind.dat"
fpath='./../data/'
uw1=np.loadtxt(fpath+fname)

fname="pp2_d_rmsx2nd_upwind.dat"
fpath='./../data/'
uw2=np.loadtxt(fpath+fname)

fname="pp2_d_rmsx2nd_cds.dat"
fpath='./../data/'
cd2=np.loadtxt(fpath+fname)

fname="pp2_d_rmst1st_upwind.dat"
fpath='./../data/'
tuw1=np.loadtxt(fpath+fname)

fname="pp2_d_rmst2nd_upwind.dat"
fpath='./../data/'
tuw2=np.loadtxt(fpath+fname)

fname="pp2_d_rmst2nd_cds.dat"
fpath='./../data/'
tcd2=np.loadtxt(fpath+fname)

N_list=np.array([10,30,100,300,1000])
dx_list=1./(np.array([10,30,100,300,1000]))

ee=0.000125
cfl_list=np.array([1000,100,10,3.,1.,0.3,0.03,])
dt_list=np.array([0.03,0.3,1,3,10,100,1000])*ee

fig = plt.figure(0, figsize=(figwidth*1.8,figheight))

ax = fig.add_subplot(121)
ax.loglog(N_list,uw1,'o-',label='1st-upwind')
ax.loglog(N_list,uw2,'o-',label='2nd-upwind')
ax.loglog(N_list,cd2,'o-',label='2nd-cds')
ax.loglog(N_list,N_list**(-1.),'r--',label='1st-ref',lw=lineWidth)
ax.loglog(N_list,N_list**(-2.),'k--',label='2nd-ref',lw=lineWidth)
m=1.2
ax.set_xlabel(r"$1/dx$",fontsize=textFontSize*m)
ax.set_ylabel(r"$RMS$",fontsize=textFontSize*m,rotation=90)
plt.setp(ax.get_xticklabels(),fontsize=gcafontSize*m)
plt.setp(ax.get_yticklabels(),fontsize=gcafontSize*m)
leg = plt.legend(loc='best', ncol=1,  shadow=False, fancybox=True,fontsize=textFontSize)
leg.get_frame().set_alpha(0.8)


ax = fig.add_subplot(122)
ax.loglog(dt_list,tuw1,'o-',label='1st-upwind')
ax.loglog(dt_list,tuw2,'o-',label='2nd-upwind')
ax.loglog(dt_list,tcd2,'o-',label='2nd-cds')

ax.loglog(dt_list,dt_list**(-2.)/1000000000000,'k--',label='2nd-ref',lw=lineWidth)
m=1.2
ax.set_xlabel(r"$1/dt$",fontsize=textFontSize*m)
ax.set_ylabel(r"$RMS$",fontsize=textFontSize*m,rotation=90)
plt.setp(ax.get_xticklabels(),fontsize=gcafontSize*m)
plt.setp(ax.get_yticklabels(),fontsize=gcafontSize*m)
leg = plt.legend(loc='best', ncol=1,  shadow=False, fancybox=True,fontsize=textFontSize)
leg.get_frame().set_alpha(0.8)



fig.tight_layout()
#fig_name = 'p3_diff.png'


fig_name = 'p2c_rms_.png'
figure_path = './../figure/'
fig_fullpath = figure_path + fig_name
plt.savefig(fig_fullpath)
plt.show()
plt.close()


