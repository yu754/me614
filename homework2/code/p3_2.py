import os
import sys
import numpy as np
import scipy.sparse as scysparse
from time import sleep
import spatial_discretization
import scipy.sparse as scysparse
import scipy.sparse.linalg as spysparselinalg  # sparse linear algebra
import scipy.linalg as scylinalg               # non-sparse linear algebra
import pylab as plt
from matplotlib import rc as matplotlibrc
from pdb import set_trace

# Figure settings
matplotlibrc('text.latex', preamble='\usepackage{color}')
matplotlibrc('text',usetex=True)
matplotlibrc('font', family='serif')

fig_sol =0
fig_spy =1
figwidth       = 10
figheight      = 6
lineWidth      = 1.5
textFontSize   = 28*0.5
gcafontSize    = 30*0.5
Nx=500

CFL = 0.2# Courant-Friedrichs-Lewy number (i.e. dimensionless time step)

c_x   = 0.0 # (linear) convection speed
c=c_x
alpha = 0.0   # diffusion coefficients
beta=0.0003
machine_epsilon = np.finfo(float).eps
def u_initial(X):
		#return np.sin(2*np.pi*X)+np.sin(8*np.pi*X)
		#return (X-0.5)**2*4
		return np.exp(-2000*(X-0.5)**2)*2	
		#return np.ones(len(X))	
def u(X):
		return np.sin(2*np.pi*X)
def ux_ana(X):
		return 2*np.pi*np.cos(2*np.pi*X)
def uxx_ana(X):
		return -2*np.pi*2*np.pi*np.sin(2*np.pi*X)
def uxxx_ana(X):
		return -2*np.pi*2*np.pi*ux_ana(X)



machine_epsilon = np.finfo(float).eps

### this example script is hard-coded for periodic problems

#########################################
############### User Input ##############

#N_list=np.array([10,25,50,100])
N_list=np.array([10])
diffusion_scheme = "2nd-order-central"

t_dic={}
t_dic[str(0)]="Explicit-Euler"
t_dic[str(1)]="Crank-Nicolson"



x_dic={}
x_dic[str(0)]="1st-order-upwind"
x_dic[str(1)]="2nd-order-upwind"
x_dic[str(2)]="2nd-order-central"


# Lx  = 1.
# CFL = 0.3# Courant-Friedrichs-Lewy number (i.e. dimensionless time step)
# c_x_ref = 1.0
# c_x   = 1.*c_x_ref  # (linear) convection speed
# c=c_x
# alpha = 0.2    # diffusion coefficients

#Lx/(c_x_ref+machine_epsilon) # one complete cycle

# At time = 0.175, the max value reached 0.6
w1=2.*np.pi
w2=4.*np.pi
c1=0.5
c2=0.5
r1=0.2*np.pi
r2=0.2*np.pi
#Tf  = (w2**2*alpha)**(-1)
plot_every  = 100

time_advancement="Explicit-Euler"
#time_advancement="Crank-Nicolson"
dispersion_scheme="four_point"



xx = np.linspace(0.,1,Nx+1)
# actual mesh points are off the boundaries x=0, x=Lx
# non-periodic boundary conditions created with ghost points
x_mesh = 0.5*(xx[:-1]+xx[1:])
dx  = np.diff(xx)[0]
dx2 = dx*dx
dx3 = dx*dx*dx			


dt_max_advective = dx/(c_x+machine_epsilon)             #   think of machine_epsilon as zero
dt_max_diffusive = dx2/(alpha+machine_epsilon)
dt_max_dispersion = dx3/(beta+machine_epsilon)
dtt_max = np.min([dt_max_advective,dt_max_diffusive])

dt_max = np.min([dtt_max,dt_max_dispersion ])
dt = CFL*dt_max


Ieye = scysparse.identity(Nx)

# Creating first derivative
Dxxx = spatial_discretization.Generate_Spatial_Operators(\
												x_mesh,dispersion_scheme,derivation_order=3)

# Creating A,B matrices such that: 
#     A*u^{n+1} = B*u^{n} + q
if time_advancement=="Explicit-Euler":
		A = Ieye
		B = Ieye+dt*beta*Dxxx
if time_advancement=="Crank-Nicolson":
		adv_diff_Op = dt*beta*Dxxx
		A = Ieye-0.5*adv_diff_Op
		B = Ieye+0.5*adv_diff_Op

			#plt.show()

A , B = scysparse.csr_matrix(A),scysparse.csr_matrix(B)

# T = (scylinalg.inv(A.todense())).dot(B.todense())  # T = A^{-1}*B
# lambdas,_ = scylinalg.eig(T); 
# r=np.real(lambdas)
# I=np.imag(lambdas)
# plt.scatter(r,I)
# plt.show()
# set_trace()
# set_trace()
#########################################
####### Eigenvalue an# alysis #############
# T = (scylinalg.inv(A.todense())).dot(B.todense())  # T = A^{-1}*B
# lambdas,_ = scylinalg.eig(T); plt.plot(np.abs(lambdas)); plt.show()
# keyboard()

#########################################
########## Time integration #############

u = u_initial(x_mesh) # initializing solution

time = 0.
it   = 0


while (it<32000):

	if (it%1000==0):


		if True:			
			fig = plt.figure(0, figsize=(figwidth*0.6,figheight*0.8))
			ax = fig.add_subplot(111)
			plt.plot(x_mesh,u)
			plt.scatter(x_mesh[-1],u[-1],marker='s')
			#ax.x_lim([-1,1])
			ax.set_ylim([-1,3])
			m=1.5
			ax.set_xlabel(r"$x$",fontsize=textFontSize*m)
			ax.set_ylabel(r"$u$",fontsize=textFontSize*m,rotation=90)
			plt.setp(ax.get_xticklabels(),fontsize=gcafontSize*m)
			plt.setp(ax.get_yticklabels(),fontsize=gcafontSize*m)	
			text="time :"+str(np.round(time,5))
			ax.text(0.6,2.5,text,fontsize=textFontSize*m)		
			fig_name = 'p3_test'+str(it)+'.png'
			figure_path = './../figure/P3/convecion/'
			fig_fullpath = figure_path + fig_name
			fig.tight_layout()
			plt.savefig(fig_fullpath)
			plt.close()

	u = spysparselinalg.spsolve(A,B.dot(u))
	it   += 1
	time += dt


