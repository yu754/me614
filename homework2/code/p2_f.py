import os
import sys
import numpy as np
import scipy.sparse as scysparse
from time import sleep
import spatial_discretization
import scipy.sparse as scysparse
import scipy.sparse.linalg as spysparselinalg  # sparse linear algebra
import scipy.linalg as scylinalg               # non-sparse linear algebra
import pylab as plt
from matplotlib import rc as matplotlibrc
from pdb import set_trace
# Figure settings
matplotlibrc('text.latex', preamble='\usepackage{color}')
matplotlibrc('text',usetex=True)
matplotlibrc('font', family='serif')

fig_sol =0
fig_spy =1
figwidth       = 8
figheight      = 6
lineWidth      = 1.5
textFontSize   = 28*0.8
gcafontSize    = 30*0.8

def u_initial(X):
		return c1*np.sin(w1*X-r1)-c2*np.cos(w2*X-r2) # lambda functions are better..
def u_ana(X,t):
		return c1*np.exp(-w1**2*alpha*t)*np.sin(w1*(X-c*t)-r1)-c2*np.exp(-w2**2*alpha*t)*np.cos(w2*(X-c*t)-r2) # lambda functions are better..
#########################################

machine_epsilon = np.finfo(float).eps

### this example script is hard-coded for periodic problems

#########################################
############### User Input ##############

N_list=np.array([10,100,1000,10000])
N_list=np.array([10,30,100,300,1000])
ee=1.6e-05
dt_list=np.array([3.,1.,0.3,0.03,])*ee*1000
diffusion_scheme = "2nd-order-central"

t_dic={}
t_dic[str(0)]="Explicit-Euler"
t_dic[str(1)]="Crank-Nicolson"



x_dic={}
x_dic[str(0)]="1st-order-upwind"
x_dic[str(1)]="2nd-order-upwind"
x_dic[str(2)]="2nd-order-central"

legt_dic={}
legt_dic[str(0)]="EE"
legt_dic[str(1)]="CN"
legx_dic={}
legx_dic[str(0)]="1st-upwind"
legx_dic[str(1)]="2nd-upwind"
legx_dic[str(2)]="2nd-cds"
Lx  = 1.
CFL = 0.4# Courant-Friedrichs-Lewy number (i.e. dimensionless time step)
c_x_ref = 1.0
c_x   = 1.*c_x_ref  # (linear) convection speed
c=c_x
alpha = 0.2    # diffusion coefficients

#Lx/(c_x_ref+machine_epsilon) # one complete cycle

# At time = 0.175, the max value reached 0.6
w1=2.*np.pi
w2=4.*np.pi
c1=0.5
c2=0.5
r1=0.2*np.pi
r2=0.2*np.pi
Tf  = (w2**2*alpha)**(-1)
plot_every  = 100


rms_x=np.zeros(N_list.shape)
rms_t=np.zeros(dt_list.shape)

for xs in range(3):
	for ts in range(2):
		time_advancement=t_dic[str(ts)]	
		advection_scheme=x_dic[str(xs)]	
		Nx=10
		xx = np.linspace(0.,1,Nx+1)
		# actual mesh points are off the boundaries x=0, x=Lx
		# non-periodic boundary conditions created with ghost points
		x_mesh = 0.5*(xx[:-1]+xx[1:])
		dx  = np.diff(xx)[0]
		dx2 = dx*dx
		dt=0.1

		nn=21
		EIG=np.zeros((nn,nn))

		C_c=np.linspace(0,2,nn)
		c_list=C_c*dx/dt
		C_a=np.linspace(0,2,nn)
		alpha_list=C_a*dx2/dt
		Ieye = scysparse.identity(Nx)
		print "Generate_Spatial_Operators"

		# Creating first derivative
		Dx = spatial_discretization.Generate_Spatial_Operators(\
														x_mesh,advection_scheme,derivation_order=1)
		# Creating second derivative
		D2x2 = spatial_discretization.Generate_Spatial_Operators(\
														x_mesh,diffusion_scheme,derivation_order=2)	


		if time_advancement=="Explicit-Euler":
			#print "222"
			A = Ieye
			B = Ieye-dt*c_x*Dx+dt*alpha*D2x2
		if time_advancement=="Crank-Nicolson":
			#print "?????"
			adv_diff_Op = -dt*c_x*Dx+dt*alpha*D2x2
			A = Ieye-0.5*adv_diff_Op
			B = Ieye+0.5*adv_diff_Op

		fig = plt.figure(0, figsize=(figwidth*1.8,figheight))
		ax = fig.add_subplot(121)
		ax.spy(A)
		m=1.2
		ax.set_xlabel(r"$x$",fontsize=textFontSize*m)
		ax.set_ylabel(r"$y$",fontsize=textFontSize*m,rotation=90)
		plt.title('A',fontsize=textFontSize*m)
		plt.setp(ax.get_xticklabels(),fontsize=gcafontSize*m)
		plt.setp(ax.get_yticklabels(),fontsize=gcafontSize*m)

		ax = fig.add_subplot(122)
		ax.spy(B)
		ax.set_xlabel(r"$x$",fontsize=textFontSize*m)
		ax.set_ylabel(r"$y$",fontsize=textFontSize*m,rotation=90)
		plt.title('B',fontsize=textFontSize*m)
		plt.setp(ax.get_xticklabels(),fontsize=gcafontSize*m)
		plt.setp(ax.get_yticklabels(),fontsize=gcafontSize*m)

		fig.tight_layout()
		#fig_name = 'p3_diff.png'


		fig_name = 'pp2d_'+legx_dic[str(xs)]+'_'+legt_dic[str(ts)]+'.png'
		figure_path = './../figure/'
		fig_fullpath = figure_path + fig_name
		plt.savefig(fig_fullpath)
		plt.show()
		plt.close()


