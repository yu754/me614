import os
import sys
import numpy as np
import scipy.sparse as scysparse
from time import sleep
import spatial_discretization as sd
import spac_disc as yusd
import scipy.sparse as scysparse
import scipy.sparse.linalg as spysparselinalg  # sparse linear algebra
import scipy.linalg as scylinalg               # non-sparse linear algebra
import pylab as plt
from matplotlib import rc as matplotlibrc
from pdb import set_trace
# Figure settings
matplotlibrc('text.latex', preamble='\usepackage{color}')
matplotlibrc('text',usetex=True)
matplotlibrc('font', family='serif')

fig_sol =0
fig_spy =1
figwidth       = 10
figheight      = 6
lineWidth      = 1.5*2
textFontSize   = 28*0.5
gcafontSize    = 30*0.5





N=8
error=np.zeros((4,N))

###########################################

a=1.0*np.float16(np.pi)
b=1.0*np.float64(np.pi)

nn=np.logspace(1,2.79,N)

for ii,n in enumerate(nn):
	print "float16:",n
	n=np.int(n)
	for i in range(n):
		a=a*np.float16(np.pi)
	for i in range(n):
		a=a/np.float16(np.pi)

	error[0,ii]=np.abs(a-b)


###########################################

a=1.0*np.float32(np.pi)
b=1.0*np.float64(np.pi)

nn=np.logspace(1,2.79,N)
for ii,n in enumerate(nn):
	print "float32:",n	
	n=np.int(n)
	for i in range(n):
		a=a*np.float32(np.pi)
	for i in range(n):
		a=a/np.float32(np.pi)

	error[1,ii]=np.abs(a-b)

###########################################

a=1.0*np.float64(np.pi)
b=1.0*np.float64(np.pi)

nn=np.logspace(1,2.79,N)
for ii,n in enumerate(nn):
	print "float64:",n	
	n=np.int(n)
	for i in range(n):
		a=a*np.float64(np.pi)
	for i in range(n):
		a=a/np.float64(np.pi)

	error[2,ii]=np.abs(a-b)

print "here??"
###########################################
a=1.0*np.float128(np.pi)
b=1.0*np.float64(np.pi)

nnn=np.logspace(1,3.996,N)
for ii,n in enumerate(nnn):
	print "float128:",n
	n=np.int(n)
	for i in range(n):
		a=a*np.float128(np.pi)
	for i in range(n):
		a=a/np.float128(np.pi)

	error[3,ii]=np.abs(a-b)



###########################################
###########################################
###########################################


fig = plt.figure(0, figsize=(figwidth,figheight))

ax = fig.add_subplot(221)
ax.loglog(nn,error[0,:],label='float16')
#ax.loglog(nn,error[1,:],label='float32')
#ax.loglog(nn,error[2,:],label='float64')
# ax.loglog(nnn,error[3,:],label='float128')
ax.grid('on',which='both')
plt.setp(ax.get_xticklabels(),fontsize=gcafontSize)
plt.setp(ax.get_yticklabels(),fontsize=gcafontSize)
ax.set_xlabel(r"$n$",fontsize=textFontSize)
ax.set_ylabel(r"$error$",fontsize=textFontSize,rotation=90)
plt.title('float16')

ax = fig.add_subplot(222)
#ax.loglog(nn,error[0,:],label='float32')
ax.loglog(nn,error[1,:],label='float32')
#ax.loglog(nn,error[2,:],label='float64')
# ax.loglog(nnn,error[3,:],label='float128')
ax.grid('on',which='both')
plt.setp(ax.get_xticklabels(),fontsize=gcafontSize)
plt.setp(ax.get_yticklabels(),fontsize=gcafontSize)
ax.set_xlabel(r"$n$",fontsize=textFontSize)
ax.set_ylabel(r"$error$",fontsize=textFontSize,rotation=90)
plt.title('float32')

ax = fig.add_subplot(223)
#ax.loglog(nn,error[0,:],label='float16')
#ax.loglog(nn,error[1,:],label='float32')
ax.loglog(nn,error[2,:],label='float64')
# ax.loglog(nnn,error[3,:],label='float128')
ax.grid('on',which='both')
plt.setp(ax.get_xticklabels(),fontsize=gcafontSize)
plt.setp(ax.get_yticklabels(),fontsize=gcafontSize)
ax.set_xlabel(r"$n$",fontsize=textFontSize)
ax.set_ylabel(r"$error$",fontsize=textFontSize,rotation=90)
plt.title('float64')

ax = fig.add_subplot(224)
#ax.loglog(nn,error[0,:],label='float16')
#ax.loglog(nn,error[1,:],label='float32')
#ax.loglog(nn,error[2,:],label='float64')
ax.loglog(nnn,error[3,:],label='float128')
ax.grid('on',which='both')
plt.setp(ax.get_xticklabels(),fontsize=gcafontSize)
plt.setp(ax.get_yticklabels(),fontsize=gcafontSize)
ax.set_xlabel(r"$n$",fontsize=textFontSize)
ax.set_ylabel(r"$error$",fontsize=textFontSize,rotation=90)
plt.title('float128')

fig.tight_layout()
fig_name = 'p1_a.png'
figure_path = './../figure/'
fig_fullpath = figure_path + fig_name
plt.savefig(fig_fullpath)
plt.close()
print fig_name+' saved!'
	
m=1.5
fig = plt.figure(0, figsize=(figwidth,figheight))
ax = fig.add_subplot(111)
ax.loglog(nn,error[0,:],label='float16',lw=lineWidth)
ax.loglog(nn,error[1,:],label='float32',lw=lineWidth)
ax.loglog(nn,error[2,:],label='float64',lw=lineWidth)
ax.loglog(nnn,error[3,:],label='float128',lw=lineWidth)
ax.grid('on',which='both')
plt.setp(ax.get_xticklabels(),fontsize=gcafontSize*m)
plt.setp(ax.get_yticklabels(),fontsize=gcafontSize*m)
ax.set_xlabel(r"$n$",fontsize=textFontSize*m)
ax.set_ylabel(r"$error$",fontsize=textFontSize*m,rotation=90)
leg = plt.legend(loc='best', ncol=1,  shadow=False, fancybox=True,fontsize=textFontSize*m)
leg.get_frame().set_alpha(0.8)
fig.tight_layout()
fig_name = 'p1_b.png'
figure_path = './../figure/'
fig_fullpath = figure_path + fig_name
plt.savefig(fig_fullpath)
plt.close()
print fig_name+' saved!'

# leg = plt.legend(loc='lower center', ncol=2, mode="expand", shadow=False, fancybox=True,fontsize=textFontSize*0.7)
# leg.get_frame().set_alpha(0.8)