import os
import sys
import numpy as np
import scipy.sparse as scysparse
from time import sleep
import spatial_discretization as sd
import spac_disc as yusd
import scipy.sparse as scysparse
import scipy.sparse.linalg as spysparselinalg  # sparse linear algebra
import scipy.linalg as scylinalg               # non-sparse linear algebra
import pylab as plt
from matplotlib import rc as matplotlibrc
from pdb import set_trace
# Figure settings
matplotlibrc('text.latex', preamble='\usepackage{color}')
matplotlibrc('text',usetex=True)
matplotlibrc('font', family='serif')

fig_sol =0
fig_spy =1
figwidth       = 10
figheight      = 6
lineWidth      = 1.5
textFontSize   = 28*0.5
gcafontSize    = 30*0.5

N=80
time_advancement="Explicit-Euler"

CFL = 0.1# Courant-Friedrichs-Lewy number (i.e. dimensionless time step)

c_x   = 0. # (linear) convection speed
alpha = 0. # diffusion coefficients
beta=3*(1./N**3)
#beta=0.000
machine_epsilon = np.finfo(float).eps
def u_initial(X):
		xx=np.zeros(X.shape)
		#xx[0:30]=1.5
		xx =2.0*(np.cos(np.pi*X) + 1.0)/2.0
		#return (X-0.5)**2*8
		#return xx
		return np.sin(2*np.pi*X)
def u(X):
		return np.sin(2*np.pi*X)
def ux_ana(X):
		return 2*np.pi*np.cos(2*np.pi*X)
def uxx_ana(X):
		return -2*np.pi*2*np.pi*np.sin(2*np.pi*X)
def uxxx_ana(X):
		return -2*n.pi*2*np.pi*ux_ana(X)


B_a=0
B_b=0
xx = np.linspace(0.,1,N+1)
# actual mesh points are off the boundaries x=0, x=Lx
# non-periodic boundary conditions created with ghost points
x_mesh = 0.5*(xx[:-1]+xx[1:])
dx=x_mesh[1]-x_mesh[0]
dx2=dx*dx
dx3=dx*dx*dx
x_ghost=np.zeros(N+4)
x_ghost[2:N+2]=x_mesh

x_ghost[0]=x_mesh[0]-2*dx
x_ghost[1]=x_mesh[0]-dx
x_ghost[-1]=x_mesh[-1]+2*dx
x_ghost[-2]=x_mesh[-1]+dx

Dx=np.zeros((N,N+4))
for i,ix in enumerate(x_ghost):
	if (i>=2)&(i<=N+1):
		stencil=x_ghost[i-2:i+1]
		weight=sd.Generate_Weights(stencil,x_ghost[i],1)
		Dx[i-2,i-2]=weight[0]
		Dx[i-2,i-1]=weight[1]
		Dx[i-2,i]=weight[2]
#scysparse.csr_matrix(Dx)

Dxx=np.zeros((N,N+4))
for i,ix in enumerate(x_ghost):
	if (i>=2)&(i<=N+1):
		stencil=x_ghost[i-1:i+2]
		weight=sd.Generate_Weights(stencil,x_ghost[i],2)
		Dxx[i-2,i-1]=weight[0]
		Dxx[i-2,i]=weight[1]
		Dxx[i-2,i+1]=weight[2]

Dxxx=np.zeros((N,N+4))
Dxxx_prateek = np.zeros((N,N))
for i,ix in enumerate(x_ghost):
	if (i>=2)&(i<=N+1):
		stencil=x_ghost[i-2:i+3]
		weight=sd.Generate_Weights(stencil,x_ghost[i],3)
		Dxxx[i-2,i-2:i+3]=weight



L=np.zeros((N+4,N+4))
L[2:N+2,:]=-c_x*Dx+alpha*Dxx+beta*Dxxx

stencil=x_ghost[0:4]
L[0,0:4]=sd.Generate_Weights(stencil,0,0)
stencil=x_ghost[1:5]
L[1,1:5]=sd.Generate_Weights(stencil,0,0)
stencil=x_ghost[N:N+4] 
L[N+3,N:N+4]=sd.Generate_Weights(stencil,1,0)
stencil=x_ghost[N-1:N+3]
L[N+2,N-1:N+3]=sd.Generate_Weights(stencil,1,0)




qb,qk=yusd.get_q(L,B_a,B_b)

LN=L[2:N+2,2:N+2]




dt_max_advective = dx/(c_x+machine_epsilon)             #   think of machine_epsilon as zero
dt_max_diffusive = dx2/(alpha+machine_epsilon)
dt_max_dispersion = dx3/(beta+machine_epsilon)
dtt_max = np.min([dt_max_advective,dt_max_diffusive])

dt_max = np.min([dtt_max,dt_max_dispersion ])
dt = dt_max *0.1
print dt,"?>>>>"

# Creating A,B matrices such that: 
#     A*u^{n+1} = B*u^{n} + q
Ieye = scysparse.identity(N+4)
if time_advancement=="Explicit-Euler":
	adv_diff_Op = dt*L
	A = Ieye
	B = Ieye+adv_diff_Op
	q=dt*qb		
if time_advancement=="Crank-Nicolson":
	adv_diff_Op = dt*(LN-qk)
	A = Ieye-0.5*adv_diff_Op
	B = Ieye+0.5*adv_diff_Op
	q=dt*qb
A , B = scysparse.csr_matrix(A),scysparse.csr_matrix(B)
u = u_initial(x_mesh) # initializing solution
u_ghost = u_initial(x_ghost)

######################################
x,y=yusd.get_xy(L,B_a,u)
m,n=yusd.get_mn(L,B_b,u)
temp=np.zeros(5)
temp[0]=x
temp[1]=y
temp[2:5]=u[0:3]
mm=np.dot(L[0,0:5],temp)
nn=np.dot(L[1,0:5],temp)
######################################


time = 0.
it   = 0
ctr_fig = 0
#while (np.abs(time - Tf)>dt):
while (it<50):
	if (it%1==0):
		#print u[-1]
		#print u[0]
		#set_trace()		
		#print np.linalg.det(A.toarray())
		T = (scylinalg.inv(A.todense())).dot(B.todense())  # T = A^{-1}*B
		lambdas,_ = scylinalg.eig(T); 
		#print np.max(lambdas)
		#if False:
		if True:			
			fig = plt.figure(0, figsize=(figwidth,figheight))
			ax = fig.add_subplot(111)
			plt.plot(x_ghost,u_ghost)
			plt.plot(0,L[0,0:5].dot(u_ghost[0:5]),'rs')
			plt.plot(0,L[1,0:5].dot(u_ghost[0:5]),'ks')
			plt.plot(1,L[-1,-5:].dot(u_ghost[-5:]),'rs')
			plt.plot(1,L[-2,-5:].dot(u_ghost[-5:]),'ks')
			#plt.scatter(x_mesh[-1],u[-1],marker='s')
			#ax.x_lim([-1,1])
			ax.set_ylim([-2,2])
			fig_name = 'p3_test'+str(ctr_fig)+'.png'
			figure_path = './../figure/P3/'
			fig_fullpath = figure_path + fig_name
			plt.savefig(fig_fullpath)
			plt.close()
			ctr_fig+=1 
		plt.show()
	print u_ghost[2:-2]
	u_ghost = spysparselinalg.spsolve(A,(B.dot(u_ghost)))
	x,y=yusd.get_xy(L,B_a,u_ghost[2:N+2])
	m,n=yusd.get_mn(L,B_b,u_ghost[2:N+2])	
	u_ghost[0:2]=x,y
	u_ghost[-2:]=m,n

	#set_trace()
	#x,y=yusd.get_xy(L,B_a,u)
	#m,n=yusd.get_xy(L,B_b,u)


	it   += 1
	time += dt
#ux_sol=L.dot(u(x_ghost))
#ux_ana=ux_ana(x_ghost)
#plt.plot(ux_sol,'.')
#plt.plot(ux_ana,"r+")
#plt.show()

#set_trace()





##########################
#a=L[0,0]
#b=L[1,1]
#c=L[2,0]  =
#L[2,:]=L[2,:]*a/(-c)+L[0,:]
#d=L[2,1]
#L[2,:]=L[2,:]*b/(-d)+L[1,:]

#a=L[-1,-1]#N+3
#b=L[-2,-2]#N+2
#c=L[N+1,-1]# -3
#L[N+1,:]=L[N+1,:]*a/(-c)+L[-1,:]
#d=L[N+1,-2]
#L[N+1,:]=L[N+1,:]*b/(-d)+L[-2,:]
#set_trace()
##########################
