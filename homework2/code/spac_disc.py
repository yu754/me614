import os
import sys
import numpy as np 
import scipy.sparse as scysparse
from pdb import set_trace as keyboard 
import matplotlib.pyplot as plt
import copy

def Lagr_base(xg,x):
	h=np.ones((len(xg)))
	for j in range(len(xg)):
		for i in range(len(xg)):
			if (i!=j):
				m=(x-xg[i])/(xg[j]-xg[i])
				h[j]=h[j]*m
	return h
# first order base func need orign y_value		

def Lagr_der_base(xl,x):
	k={}
	h=np.ones((len(xl)))		

	temp=np.zeros(len(xl))		

	for j in range(len(xl)):
		b=np.ones((len(xl)-1))		

		xx=np.ones((len(xl)-1))*x
		for i in range(len(xl)):
			k=list(copy.deepcopy(xl))
			bb=1
			k.remove(xl[j])
			b=xl[j]-k
			t=xx-k
			tt=np.ones((len(xl)-1))			
			for z,value in enumerate(t):
				bb=bb*b[z]#over
				for zz in range(len(t)):
					if (zz!=z):
						tt[z]=tt[z]*t[zz]
		temp[j]=np.sum(tt)
		h[j]=temp[j]/bb
		#set_trace()
	return h	

def get_q(L,B_a,B_b):
	N=L.shape[0]-4
	qb=np.zeros(N)
	qkk=np.zeros((N,N))
	a=L[0,0]
	e=L[0,1]
	b=L[1,1]
	c=L[2,0]  
	d=L[2,1]
	f=L[3,1]
	
	bb=c*B_a/a+(d-c*e/a)*B_a/b	
	kk=(d-c*e/a)/b*L[1,2:N+2]+c/a*L[0,2:N+2]
	
	qb[0]=bb	
	qkk[0,:]=kk

	bb2=f/b*B_a
	kk2=f/b*L[1,2:N+2]

	qb[1]=bb2	
	qkk[1,:]=kk2

	####################################################

	a=L[-1,-1]#N+3
	e=L[-1,-2]#N+3
	b=L[-2,-2]#N+2
	c=L[N+1,-1]# -3
	d=L[N+1,-2]# -3
	f=L[N,-2]
	#print "11,",f
	bb=c*B_b/a+(d-c*e/a)*B_b/b
	kk=(d-c*e/a)/b*L[-2,2:N+2]+c/a*L[-1,2:N+2]
	qb[-1]=bb
	qkk[-1,:]=kk

	bb2=f/b*B_b
	kk2=f/b*L[-2,2:N+2]
	qb[-2]=bb2
	qkk[-2,:]=kk2
	return qb,qkk	

def get_xy(L,B_a,u):	
	N=L.shape[0]-4	
	a=L[0,0]
	e=L[0,1]
	b=L[1,1]
	c=L[2,0]  
	d=L[2,1]
	f=L[3,1]
	y=(B_a-np.dot(L[1,2:N+2],u))/b
	x=(B_a-np.dot(L[0,2:N+2],u)-e*y)/a
	return x,y

def get_mn(L,B_b,u):	
	N=L.shape[0]-4	
	a=L[-1,-1]#N+3
	e=L[-1,-2]#N+3
	b=L[-2,-2]#N+2
	c=L[N+1,-1]# -3
	d=L[N+1,-2]# -3
	f=L[N,-2]
	m=(B_b-np.dot(L[-2,2:N+2],u))/b
	n=(B_b-np.dot(L[-1,2:N+2],u)-e*m)/a
	return m,n	