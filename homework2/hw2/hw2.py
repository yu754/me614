import os
import sys
import numpy as np
import scipy.sparse as scysparse
from time import sleep
import spatial_discretization as sd
import spac_disc as yusd
import scipy.sparse as scysparse
import scipy.sparse.linalg as spysparselinalg  # sparse linear algebra
import scipy.linalg as scylinalg               # non-sparse linear algebra
import pylab as plt
from matplotlib import rc as matplotlibrc
from pdb import set_trace

# Figure settings
matplotlibrc('text.latex', preamble='\usepackage{color}')
matplotlibrc('text',usetex=True)
matplotlibrc('font', family='serif')

fig_sol =0
fig_spy =1
figwidth       = 10
figheight      = 6
lineWidth      = 1.5*2
textFontSize   = 28*0.5
gcafontSize    = 30*0.5
######################################################################################
										P1
######################################################################################
N=8
error=np.zeros((4,N))

###########################################

a=1.0*np.float16(np.pi)
b=1.0*np.float64(np.pi)

nn=np.logspace(1,2.79,N)

for ii,n in enumerate(nn):
	print "float16:",n
	n=np.int(n)
	for i in range(n):
		a=a*np.float16(np.pi)
	for i in range(n):
		a=a/np.float16(np.pi)

	error[0,ii]=np.abs(a-b)


###########################################

a=1.0*np.float32(np.pi)
b=1.0*np.float64(np.pi)

nn=np.logspace(1,2.79,N)
for ii,n in enumerate(nn):
	print "float32:",n	
	n=np.int(n)
	for i in range(n):
		a=a*np.float32(np.pi)
	for i in range(n):
		a=a/np.float32(np.pi)

	error[1,ii]=np.abs(a-b)

###########################################

a=1.0*np.float64(np.pi)
b=1.0*np.float64(np.pi)

nn=np.logspace(1,2.79,N)
for ii,n in enumerate(nn):
	print "float64:",n	
	n=np.int(n)
	for i in range(n):
		a=a*np.float64(np.pi)
	for i in range(n):
		a=a/np.float64(np.pi)

	error[2,ii]=np.abs(a-b)

print "here??"
###########################################
a=1.0*np.float128(np.pi)
b=1.0*np.float64(np.pi)

nnn=np.logspace(1,3.996,N)
for ii,n in enumerate(nnn):
	print "float128:",n
	n=np.int(n)
	for i in range(n):
		a=a*np.float128(np.pi)
	for i in range(n):
		a=a/np.float128(np.pi)

	error[3,ii]=np.abs(a-b)



###########################################
###########################################
###########################################


fig = plt.figure(0, figsize=(figwidth,figheight))

ax = fig.add_subplot(221)
ax.loglog(nn,error[0,:],label='float16')
#ax.loglog(nn,error[1,:],label='float32')
#ax.loglog(nn,error[2,:],label='float64')
# ax.loglog(nnn,error[3,:],label='float128')
ax.grid('on',which='both')
plt.setp(ax.get_xticklabels(),fontsize=gcafontSize)
plt.setp(ax.get_yticklabels(),fontsize=gcafontSize)
ax.set_xlabel(r"$n$",fontsize=textFontSize)
ax.set_ylabel(r"$error$",fontsize=textFontSize,rotation=90)
plt.title('float16')

ax = fig.add_subplot(222)
#ax.loglog(nn,error[0,:],label='float32')
ax.loglog(nn,error[1,:],label='float32')
#ax.loglog(nn,error[2,:],label='float64')
# ax.loglog(nnn,error[3,:],label='float128')
ax.grid('on',which='both')
plt.setp(ax.get_xticklabels(),fontsize=gcafontSize)
plt.setp(ax.get_yticklabels(),fontsize=gcafontSize)
ax.set_xlabel(r"$n$",fontsize=textFontSize)
ax.set_ylabel(r"$error$",fontsize=textFontSize,rotation=90)
plt.title('float32')

ax = fig.add_subplot(223)
#ax.loglog(nn,error[0,:],label='float16')
#ax.loglog(nn,error[1,:],label='float32')
ax.loglog(nn,error[2,:],label='float64')
# ax.loglog(nnn,error[3,:],label='float128')
ax.grid('on',which='both')
plt.setp(ax.get_xticklabels(),fontsize=gcafontSize)
plt.setp(ax.get_yticklabels(),fontsize=gcafontSize)
ax.set_xlabel(r"$n$",fontsize=textFontSize)
ax.set_ylabel(r"$error$",fontsize=textFontSize,rotation=90)
plt.title('float64')

ax = fig.add_subplot(224)
#ax.loglog(nn,error[0,:],label='float16')
#ax.loglog(nn,error[1,:],label='float32')
#ax.loglog(nn,error[2,:],label='float64')
ax.loglog(nnn,error[3,:],label='float128')
ax.grid('on',which='both')
plt.setp(ax.get_xticklabels(),fontsize=gcafontSize)
plt.setp(ax.get_yticklabels(),fontsize=gcafontSize)
ax.set_xlabel(r"$n$",fontsize=textFontSize)
ax.set_ylabel(r"$error$",fontsize=textFontSize,rotation=90)
plt.title('float128')

fig.tight_layout()
fig_name = 'p1_a.png'
figure_path = './../figure/'
fig_fullpath = figure_path + fig_name
plt.savefig(fig_fullpath)
plt.close()
print fig_name+' saved!'
	
m=1.5
fig = plt.figure(0, figsize=(figwidth,figheight))
ax = fig.add_subplot(111)
ax.loglog(nn,error[0,:],label='float16',lw=lineWidth)
ax.loglog(nn,error[1,:],label='float32',lw=lineWidth)
ax.loglog(nn,error[2,:],label='float64',lw=lineWidth)
ax.loglog(nnn,error[3,:],label='float128',lw=lineWidth)
ax.grid('on',which='both')
plt.setp(ax.get_xticklabels(),fontsize=gcafontSize*m)
plt.setp(ax.get_yticklabels(),fontsize=gcafontSize*m)
ax.set_xlabel(r"$n$",fontsize=textFontSize*m)
ax.set_ylabel(r"$error$",fontsize=textFontSize*m,rotation=90)
leg = plt.legend(loc='best', ncol=1,  shadow=False, fancybox=True,fontsize=textFontSize*m)
leg.get_frame().set_alpha(0.8)
fig.tight_layout()
fig_name = 'p1_b.png'
figure_path = './../figure/'
fig_fullpath = figure_path + fig_name
plt.savefig(fig_fullpath)
plt.close()
print fig_name+' saved!'
















######################################################################################
										P2
######################################################################################

def u_initial(X):
		return c1*np.sin(w1*X-r1)-c2*np.cos(w2*X-r2) # lambda functions are better..
def u_ana(X,t):
		return c1*np.exp(-w1**2*alpha*t)*np.sin(w1*(X-c_x*t)-r1)-c2*np.exp(-w2**2*alpha*t)*np.cos(w2*(X-c_x*t)-r2) # lambda functions are better..
#########################################

machine_epsilon = np.finfo(float).eps

### this example script is hard-coded for periodic problems

#########################################
############### User Input ##############

#N_list=np.array([10,100,1000,10000])
N_list=np.array([10,30,100,300,1000])
ee=0.000125
dt_list=np.array([1000,100,10,3.,1.,0.3,0.03,])*ee
diffusion_scheme = "2nd-order-central"

t_dic={}
t_dic[str(0)]="Explicit-Euler"
t_dic[str(1)]="Crank-Nicolson"



x_dic={}
x_dic[str(0)]="1st-order-upwind"
x_dic[str(1)]="2nd-order-upwind"
x_dic[str(2)]="2nd-order-central"

legt_dic={}
legt_dic[str(0)]="EE"
legt_dic[str(1)]="CN"
legx_dic={}
legx_dic[str(0)]="1st_upwind"
legx_dic[str(1)]="2nd_upwind"
legx_dic[str(2)]="2nd_cds"
Lx  = 1.
CFL = 1.# Courant-Friedrichs-Lewy number (i.e. dimensionless time step)
c_x_ref = 1.0
c_x   = 1.*c_x_ref  # (linear) convection speed
alpha = 0.2    # diffusion coefficients

#Lx/(c_x_ref+machine_epsilon) # one complete cycle

# At time = 0.175, the max value reached 0.6
w1=2.*np.pi
w2=4.*np.pi
c1=0.5
c2=0.5
r1=0.2*np.pi
r2=0.2*np.pi
Tf  = (w2**2*alpha)**(-1)
plot_every  = 100


rms_x=np.zeros(N_list.shape)
rms_t=np.zeros(dt_list.shape)
if False:
	for xs in range(0,2):
		for iN,Nx in enumerate(N_list):

			advection_scheme=x_dic[str(xs)]	
			#Nx=N_list
			xx = np.linspace(0.,1,Nx+1)
			# actual mesh points are off the boundaries x=0, x=Lx
			# non-periodic boundary conditions created with ghost points
			x_mesh = 0.5*(xx[:-1]+xx[1:])
			dx  = np.diff(xx)[0]
			dx2 = dx*dx
			dt=2e-06

			# for linear advection/diffusion time step is a function
			# of c,alpha,dx only; we use reference limits, ballpark
			# # estimates for Explicit Euler
			# dt_max_advective = dx/(c_x+machine_epsilon)             #   think of machine_epsilon as zero
			# dt_max_diffusive = dx2/(alpha+machine_epsilon)
			# dt_max = np.min([dt_max_advective,dt_max_diffusive])
			# dt = CFL*dt_max
			print dt
			set_trace()


			Ieye = scysparse.identity(Nx)
			print "Generate_Spatial_Operators"
			time_advancement="Crank-Nicolson"
			# Creating first derivative
			Dx = spatial_discretization.Generate_Spatial_Operators(\
															x_mesh,advection_scheme,derivation_order=1)
			# Creating second derivative
			D2x2 = spatial_discretization.Generate_Spatial_Operators(\
															x_mesh,diffusion_scheme,derivation_order=2)	


			if time_advancement=="Explicit-Euler":
				#print "222"
				A = Ieye
				B = Ieye-dt*c_x*Dx+dt*alpha*D2x2
			if time_advancement=="Crank-Nicolson":
				#print "?????"
				adv_diff_Op = -dt*c_x*Dx+dt*alpha*D2x2
				A = Ieye-0.5*adv_diff_Op
				B = Ieye+0.5*adv_diff_Op
				#set_trace()

			A , B = scysparse.csr_matrix(A),scysparse.csr_matrix(B)
			u = u_initial(x_mesh)
			time = 0.
			it   = 0
			while time < Tf:

			    it   += 1
			    time += dt
			   
			    print "Iteration = " + repr(it)	   
			    u = spysparselinalg.spsolve(A,B.dot(u))

			rms_x[iN]=(np.sum((u_ana(x_mesh,Tf)-u)**2 )/Nx   )**0.5
		#set_trace()
		fname="pp2_d_rmsx"+legx_dic[str(xs)] +".dat"	
		fpath='./../data/'
		fout = open(fpath+fname, 'w')
		for k in range(len(rms_x)):
		     fout.write(str(rms_x[k])+" "+"\n")
		fout.close()







if True:
	for xs in range(3):
		for iT,dt in enumerate(dt_list):

			advection_scheme=x_dic[str(xs)]	
			Nx=200
			xx = np.linspace(0.,1,Nx+1)
			# actual mesh points are off the boundaries x=0, x=Lx
			# non-periodic boundary conditions created with ghost points
			x_mesh = 0.5*(xx[:-1]+xx[1:])
			dx  = np.diff(xx)[0]
			dx2 = dx*dx


			# for linear advection/diffusion time step is a function
			# of c,alpha,dx only; we use reference limits, ballpark
			# estimates for Explicit Euler
			# dt_max_advective = dx/(c_x+machine_epsilon)             #   think of machine_epsilon as zero
			# dt_max_diffusive = dx2/(alpha+machine_epsilon)
			# dt_max = np.min([dt_max_advective,dt_max_diffusive])
			# dt = CFL*dt_max
			# print dt
			# set_trace()


			Ieye = scysparse.identity(Nx)
			print "Generate_Spatial_Operators"
			time_advancement="Crank-Nicolson"
			# Creating first derivative
			Dx = spatial_discretization.Generate_Spatial_Operators(\
															x_mesh,advection_scheme,derivation_order=1)
			# Creating second derivative
			D2x2 = spatial_discretization.Generate_Spatial_Operators(\
															x_mesh,diffusion_scheme,derivation_order=2)	


			if time_advancement=="Explicit-Euler":
				#print "222"
				A = Ieye
				B = Ieye-dt*c_x*Dx+dt*alpha*D2x2
			if time_advancement=="Crank-Nicolson":
				#print "?????"
				adv_diff_Op = -dt*c_x*Dx+dt*alpha*D2x2
				A = Ieye-0.5*adv_diff_Op
				B = Ieye+0.5*adv_diff_Op
				#set_trace()

			A , B = scysparse.csr_matrix(A),scysparse.csr_matrix(B)
			u = u_initial(x_mesh)
			time = 0.
			it   = 0
			while time < Tf:

			    it   += 1
			    time += dt
			   
			    print "Iteration = " + repr(it)	   
			    u = spysparselinalg.spsolve(A,B.dot(u))

			rms_t[iT]=(np.sum((u_ana(x_mesh,Tf)-u)**2 )/Nx   )**0.5
		#set_trace()
		fname="pp2_d_rmst"+legx_dic[str(xs)] +".dat"	
		fpath='./../data/'
		fout = open(fpath+fname, 'w')
		for k in range(len(rms_t)):
		     fout.write(str(rms_t[k])+" "+"\n")
		fout.close()	

def u_initial(X):
		return c1*np.sin(w1*X-r1)-c2*np.cos(w2*X-r2) # lambda functions are better..
def u_ana(X,t):
		return c1*np.exp(-w1**2*alpha*t)*np.sin(w1*(X-c*t)-r1)-c2*np.exp(-w2**2*alpha*t)*np.cos(w2*(X-c*t)-r2) # lambda functions are better..
#########################################

machine_epsilon = np.finfo(float).eps

### this example script is hard-coded for periodic problems

#########################################
############### User Input ##############

N_list=np.array([10,100,1000,10000])
N_list=np.array([10,30,100,300,1000])
ee=1.6e-05
dt_list=np.array([3.,1.,0.3,0.03,])*ee*1000
diffusion_scheme = "2nd-order-central"

t_dic={}
t_dic[str(0)]="Explicit-Euler"
t_dic[str(1)]="Crank-Nicolson"



x_dic={}
x_dic[str(0)]="1st-order-upwind"
x_dic[str(1)]="2nd-order-upwind"
x_dic[str(2)]="2nd-order-central"

legt_dic={}
legt_dic[str(0)]="EE"
legt_dic[str(1)]="CN"
legx_dic={}
legx_dic[str(0)]="1st-upwind"
legx_dic[str(1)]="2nd-upwind"
legx_dic[str(2)]="2nd-cds"
Lx  = 1.
CFL = 0.4# Courant-Friedrichs-Lewy number (i.e. dimensionless time step)
c_x_ref = 1.0
c_x   = 1.*c_x_ref  # (linear) convection speed
c=c_x
alpha = 0.2    # diffusion coefficients

#Lx/(c_x_ref+machine_epsilon) # one complete cycle

# At time = 0.175, the max value reached 0.6
w1=2.*np.pi
w2=4.*np.pi
c1=0.5
c2=0.5
r1=0.2*np.pi
r2=0.2*np.pi
Tf  = (w2**2*alpha)**(-1)
plot_every  = 100


rms_x=np.zeros(N_list.shape)
rms_t=np.zeros(dt_list.shape)

for xs in range(1):

	time_advancement="Explicit-Euler"
	advection_scheme=x_dic[str(xs)]	
	Nx=100
	xx = np.linspace(0.,1,Nx+1)
	# actual mesh points are off the boundaries x=0, x=Lx
	# non-periodic boundary conditions created with ghost points
	x_mesh = 0.5*(xx[:-1]+xx[1:])
	dx  = np.diff(xx)[0]
	dx2 = dx*dx
	dt=0.1

	nn=21
	EIG=np.zeros((nn,nn))

	C_c=np.linspace(0,2,nn)
	c_list=C_c*dx/dt
	C_a=np.linspace(0,2,nn)
	alpha_list=C_a*dx2/dt
	Ieye = scysparse.identity(Nx)
	print "Generate_Spatial_Operators"
	time_advancement="Explicit-Euler"
	# Creating first derivative
	Dx = spatial_discretization.Generate_Spatial_Operators(\
													x_mesh,advection_scheme,derivation_order=1)
	# Creating second derivative
	D2x2 = spatial_discretization.Generate_Spatial_Operators(\
													x_mesh,diffusion_scheme,derivation_order=2)	

	for ic,c_x in enumerate(c_list):
		for ia,alpha in enumerate(alpha_list):	

			if time_advancement=="Explicit-Euler":
					#print "222"
					A = Ieye
					B = Ieye-dt*c_x*Dx+dt*alpha*D2x2
			if time_advancement=="Crank-Nicolson":
					#print "?????"
					adv_diff_Op = -dt*c_x*Dx+dt*alpha*D2x2
					A = Ieye-0.5*adv_diff_Op
					B = Ieye+0.5*adv_diff_Op
			set_trace()

			A , B = scysparse.csr_matrix(A),scysparse.csr_matrix(B)


			T = (scylinalg.inv(A.todense())).dot(B.todense())  # T = A^{-1}*B
			lambdas,_ = scylinalg.eig(T); 
			length=(lambdas*np.conj(lambdas))**0.5

			lambdas_D2x2,_ = scylinalg.eig(D2x2.todense());
			lambdas_Dx,_ = scylinalg.eig(Dx.todense());

			#set_trace()
			#print ",,,,:",np.max(np.abs(eig(Dx)))
			#lambdas_2, _ = scylinalg.eig(dt*alpha*D2x2.todense())

			#print ",,,,:",np.max(np.abs(lambdas_Dx))

			#length=np.real(lambdas)
			#plt.plot(np.abs(lambdas)); 
			print "!!!!!!!!!!!!!!!!!!!!!!!"
			text="cfl_c="+str(c_x*dt/dx)+"  cfl_a="+str(alpha*dt/dx2)			
			print text
			print ",,,,:",np.max(np.abs(lambdas))			
			#II = np.argsort(np.real(lambdas_Dx))
			#print lambdas[II]
			#plt.show()
			#EIG[ic,ia]=np.max(length)
			EIG[ic,ia]=np.max(np.abs(lambdas))		
	print "min eig::",np.min(EIG)
	fig = plt.figure(0, figsize=(figwidth,figheight))
	ax = fig.add_subplot(111)
	CY,CX=np.meshgrid(C_a,C_c)
	c=ax.pcolor(CX, CY, EIG)
	#c=plt.pcolor(CY,CX,EIG,30)
	index=np.where(np.abs(EIG-1)<0.01)
	index2=np.where((np.abs(EIG-1)<0.1)&(np.abs(EIG-1)>=0.01))
	levels=[1.0]
	# CS4 = ax.contour(C_c,C_a,EIG,levels,
 #                  colors=('w',),
 #                  linewidths=(3,),)
	cbar= fig.colorbar(c)
	ax.plot(C_c[index[0]],C_a[index[1]],'ws')	
	ax.plot(C_c[index2[0]],C_a[index2[1]],'s',mfc="None",mec='w')
	cbar.ax.tick_params(labelsize=1.0*gcafontSize)                            
	cbar.ax.set_ylabel(r'$Eig_{max}$', fontsize=1.2*gcafontSize)
	cl = plt.getp(cbar.ax, 'ymajorticklabels')
	plt.setp(cl, fontsize=gcafontSize)                
	plt.setp(ax.get_xticklabels(),fontsize=gcafontSize)
	plt.setp(ax.get_yticklabels(),fontsize=gcafontSize)
	ax.set_xlabel(r'$CFL_c$',fontsize=1.2*gcafontSize)
	ax.set_ylabel(r'$CFL_{\alpha}$',fontsize=1.2*gcafontSize)
	#ax.set_xticklabels([-1,0,1],fontsize=gcafontSize)
	#ax.set_yticklabels([-1,0,1],fontsize=gcafontSize)
	ax.set_title(legx_dic[str(xs)],fontsize=gcafontSize)   
	fig_name = 'p2c'+legx_dic[str(xs)]+'.png'
	figure_path = './../figure/'
	fig_fullpath = fig_name
	fig.tight_layout()
	plt.savefig(fig_fullpath)
	plt.axis('equal')
	plt.show()

	plt.close()					









######################################################################################
										P3
######################################################################################


N=80
time_advancement="Crank-Nicolson"

CFL = 1.0# Courant-Friedrichs-Lewy number (i.e. dimensionless time step)

c_x   = 0. # (linear) convection speed
alpha = 0.0 # diffusion coefficients
#beta=3*(1./N**3)
beta=0.003
machine_epsilon = np.finfo(float).eps
def u_initial(X):
		xx=np.zeros(X.shape)
		#xx[0:30]=1.5
		#xx =2.0*(np.cos(np.pi*X) + 1.0)/2.0
		#return (X-0.5)**2*8
		return xx
		#return np.sin(2*np.pi*X)
		r#eturn np.exp(-1000.0*(X-0.2)**2)		
def u(X):
		return np.sin(2*np.pi*X)
def ux_ana(X):
		return 2*np.pi*np.cos(2*np.pi*X)
def uxx_ana(X):
		return -2*np.pi*2*np.pi*np.sin(2*np.pi*X)
def uxxx_ana(X):
		return -2*np.pi*2*np.pi*ux_ana(X)


B_a=1.0
B_b=0.0
xx = np.linspace(0.,1,N+1)
# actual mesh points are off the boundaries x=0, x=Lx
# non-periodic boundary conditions created with ghost points
x_mesh = 0.5*(xx[:-1]+xx[1:])
dx=x_mesh[1]-x_mesh[0]
dx2=dx*dx
dx3=dx*dx*dx
x_ghost=np.zeros(N+4)
x_ghost[2:N+2]=x_mesh

x_ghost[0]=x_mesh[0]-2*dx
x_ghost[1]=x_mesh[0]-dx
x_ghost[-1]=x_mesh[-1]+2*dx
x_ghost[-2]=x_mesh[-1]+dx

Dx=np.zeros((N,N+4))
for i,ix in enumerate(x_ghost):
	if (i>=2)&(i<=N+1):
		stencil=x_ghost[i-2:i+1]
		weight=sd.Generate_Weights(stencil,x_ghost[i],1)
		Dx[i-2,i-2]=weight[0]
		Dx[i-2,i-1]=weight[1]
		Dx[i-2,i]=weight[2]
#scysparse.csr_matrix(Dx)

Dxx=np.zeros((N,N+4))
for i,ix in enumerate(x_ghost):
	if (i>=2)&(i<=N+1):
		stencil=x_ghost[i-1:i+2]
		weight=sd.Generate_Weights(stencil,x_ghost[i],2)
		Dxx[i-2,i-1]=weight[0]
		Dxx[i-2,i]=weight[1]
		Dxx[i-2,i+1]=weight[2]

Dxxx=np.zeros((N,N+4))
Dxxx_prateek = np.zeros((N,N))
for i,ix in enumerate(x_ghost):
	if (i>=2)&(i<=N+1):
		stencil=x_ghost[i-2:i+3]
		weight=sd.Generate_Weights(stencil,x_ghost[i],3)
		Dxxx[i-2,i-2:i+3]=weight


L=np.zeros((N+4,N+4))
L[2:N+2,:]=-c_x*Dx+alpha*Dxx+beta*Dxxx

# stencil=x_ghost[0:4]
# L[0,0:4]=sd.Generate_Weights(stencil,0.,0)
# stencil=x_ghost[1:5]
# L[1,1:5]=sd.Generate_Weights(stencil,0.,0)
# stencil=x_ghost[N:N+4] 
# L[N+3,N:N+4]=sd.Generate_Weights(stencil,1.,0)
# stencil=x_ghost[N-1:N+3]
# L[N+2,N-1:N+3]=sd.Generate_Weights(stencil,1.,0)

stencil=x_ghost[0:3]
L[0,0:3]=sd.Generate_Weights(stencil,0.,0)
stencil=x_ghost[1:3]
L[1,1:3]=sd.Generate_Weights(stencil,0.,0)
stencil=x_ghost[N:N+4] 
L[N+3,N:N+4]=sd.Generate_Weights(stencil,1.,1)
stencil=x_ghost[N-1:N+3]
L[N+2,N-1:N+3]=sd.Generate_Weights(stencil,1.,1)



qb,qk=yusd.get_q(L,B_a,B_b)

LN=L[2:N+2,2:N+2]




dt_max_advective = dx/(c_x+machine_epsilon)             #   think of machine_epsilon as zero
dt_max_diffusive = dx2/(alpha+machine_epsilon)
dt_max_dispersion = dx3/(beta+machine_epsilon)
dtt_max = np.min([dt_max_advective,dt_max_diffusive])

dt_max = np.min([dtt_max,dt_max_dispersion ])
dt = dt_max *0.1
print dt

# Creating A,B matrices such that: 
#     A*u^{n+1} = B*u^{n} + q
Ieye = scysparse.identity(N)
if time_advancement=="Explicit-Euler":
	adv_diff_Op = dt*(LN-qk)	
	A = Ieye
	B = Ieye+adv_diff_Op
	q=dt*qb		
if time_advancement=="Crank-Nicolson":
	adv_diff_Op = dt*(LN-qk)
	A = Ieye-0.5*adv_diff_Op
	B = Ieye+0.5*adv_diff_Op
	q=dt*qb
A , B = scysparse.csr_matrix(A),scysparse.csr_matrix(B)
u = u_initial(x_mesh) # initializing solution

T = (scylinalg.inv(A.todense())).dot(B.todense())  # T = A^{-1}*B
lambdas,_ = scylinalg.eig(T); 
r=np.real(lambdas)
I=np.imag(lambdas)
plt.scatter(r,I)
plt.show()
set_trace()
######################################
x,y=yusd.get_xy(L,B_a,u)
temp=np.zeros(5)
temp[0]=x
temp[1]=y
temp[2:5]=u[0:3]
mm=np.dot(L[0,0:5],temp)
nn=np.dot(L[1,0:5],temp)
######################################



time = 0.
it   = 0
ctr_fig = 0
#while (np.abs(time - Tf)>dt):
fig = plt.figure(0, figsize=(figwidth,figheight))
ax = fig.add_subplot(111)

TT=50000
while (it<=TT):
	# x,y=yusd.get_xy(L,B_a,u)
	# m,n=yusd.get_mn(L,B_b,u)	
	if (it%(TT/10)==0):		
		print it,'/',TT	


		#plt.scatter(x_mesh[-1],u[-1],marker='s')
		# plt.scatter(x_ghost[0],x,marker='s')
		# plt.scatter(x_ghost[1],y,marker='s')
		# plt.scatter(x_ghost[-2],m,marker='s')
		# plt.scatter(x_ghost[-1],n,marker='s')			
		#ax.x_lim([-1,1])
		if (it==0):
			plt.plot(x_mesh,u,'k--')
		elif (it==TT):
			plt.plot(x_mesh,u,'r')
		else:
			plt.plot(x_mesh,u,'k')



	#set_trace()
	u = spysparselinalg.spsolve(A,(B.dot(u))+q)

	#set_trace()	
	it   += 1
	time += dt

ax.set_ylim([-2,2])
m=1.5
ax.set_xlabel(r"$x$",fontsize=textFontSize*m)
ax.set_ylabel(r"$u$",fontsize=textFontSize*m,rotation=90)
plt.setp(ax.get_xticklabels(),fontsize=gcafontSize*m)
plt.setp(ax.get_yticklabels(),fontsize=gcafontSize*m)
text="c="+str(c_x)+" alpha= "+str(alpha)+ " beta= "+str(beta)+" a= "+str(B_a)
ax.text(0,-1.5,text,fontsize=textFontSize*1.2)
text2="Tf="+str(np.round(time,5))
ax.text(0,-1.9,text2,fontsize=textFontSize*1.2)
#fig_name = 'p3_diff.png'
fig_name = 'p3_abc'+str(TT)+'_.png'
figure_path = './../figure/P3/Diffusion/'
fig_fullpath = figure_path + fig_name
plt.savefig(fig_fullpath)
plt.close()
ctr_fig+=1 
plt.show()










######################################################################################
										P3_2
######################################################################################

Nx=500

CFL = 0.2# Courant-Friedrichs-Lewy number (i.e. dimensionless time step)

c_x   = 0.0 # (linear) convection speed
c=c_x
alpha = 0.0   # diffusion coefficients
beta=0.0003
machine_epsilon = np.finfo(float).eps
def u_initial(X):
		#return np.sin(2*np.pi*X)+np.sin(8*np.pi*X)
		#return (X-0.5)**2*4
		return np.exp(-2000*(X-0.5)**2)*2	
		#return np.ones(len(X))	
def u(X):
		return np.sin(2*np.pi*X)
def ux_ana(X):
		return 2*np.pi*np.cos(2*np.pi*X)
def uxx_ana(X):
		return -2*np.pi*2*np.pi*np.sin(2*np.pi*X)
def uxxx_ana(X):
		return -2*np.pi*2*np.pi*ux_ana(X)



machine_epsilon = np.finfo(float).eps

### this example script is hard-coded for periodic problems

#########################################
############### User Input ##############

#N_list=np.array([10,25,50,100])
N_list=np.array([10])
diffusion_scheme = "2nd-order-central"

t_dic={}
t_dic[str(0)]="Explicit-Euler"
t_dic[str(1)]="Crank-Nicolson"



x_dic={}
x_dic[str(0)]="1st-order-upwind"
x_dic[str(1)]="2nd-order-upwind"
x_dic[str(2)]="2nd-order-central"


# Lx  = 1.
# CFL = 0.3# Courant-Friedrichs-Lewy number (i.e. dimensionless time step)
# c_x_ref = 1.0
# c_x   = 1.*c_x_ref  # (linear) convection speed
# c=c_x
# alpha = 0.2    # diffusion coefficients

#Lx/(c_x_ref+machine_epsilon) # one complete cycle

# At time = 0.175, the max value reached 0.6
w1=2.*np.pi
w2=4.*np.pi
c1=0.5
c2=0.5
r1=0.2*np.pi
r2=0.2*np.pi
#Tf  = (w2**2*alpha)**(-1)
plot_every  = 100

time_advancement="Explicit-Euler"
#time_advancement="Crank-Nicolson"
dispersion_scheme="four_point"



xx = np.linspace(0.,1,Nx+1)
# actual mesh points are off the boundaries x=0, x=Lx
# non-periodic boundary conditions created with ghost points
x_mesh = 0.5*(xx[:-1]+xx[1:])
dx  = np.diff(xx)[0]
dx2 = dx*dx
dx3 = dx*dx*dx			


dt_max_advective = dx/(c_x+machine_epsilon)             #   think of machine_epsilon as zero
dt_max_diffusive = dx2/(alpha+machine_epsilon)
dt_max_dispersion = dx3/(beta+machine_epsilon)
dtt_max = np.min([dt_max_advective,dt_max_diffusive])

dt_max = np.min([dtt_max,dt_max_dispersion ])
dt = CFL*dt_max


Ieye = scysparse.identity(Nx)

# Creating first derivative
Dxxx = spatial_discretization.Generate_Spatial_Operators(\
												x_mesh,dispersion_scheme,derivation_order=3)

# Creating A,B matrices such that: 
#     A*u^{n+1} = B*u^{n} + q
if time_advancement=="Explicit-Euler":
		A = Ieye
		B = Ieye+dt*beta*Dxxx
if time_advancement=="Crank-Nicolson":
		adv_diff_Op = dt*beta*Dxxx
		A = Ieye-0.5*adv_diff_Op
		B = Ieye+0.5*adv_diff_Op

			#plt.show()

A , B = scysparse.csr_matrix(A),scysparse.csr_matrix(B)

# T = (scylinalg.inv(A.todense())).dot(B.todense())  # T = A^{-1}*B
# lambdas,_ = scylinalg.eig(T); 
# r=np.real(lambdas)
# I=np.imag(lambdas)
# plt.scatter(r,I)
# plt.show()
# set_trace()
# set_trace()
#########################################
####### Eigenvalue an# alysis #############
# T = (scylinalg.inv(A.todense())).dot(B.todense())  # T = A^{-1}*B
# lambdas,_ = scylinalg.eig(T); plt.plot(np.abs(lambdas)); plt.show()
# keyboard()

#########################################
########## Time integration #############

u = u_initial(x_mesh) # initializing solution

time = 0.
it   = 0


while (it<32000):

	if (it%1000==0):


		if True:			
			fig = plt.figure(0, figsize=(figwidth*0.6,figheight*0.8))
			ax = fig.add_subplot(111)
			plt.plot(x_mesh,u)
			plt.scatter(x_mesh[-1],u[-1],marker='s')
			#ax.x_lim([-1,1])
			ax.set_ylim([-1,3])
			m=1.5
			ax.set_xlabel(r"$x$",fontsize=textFontSize*m)
			ax.set_ylabel(r"$u$",fontsize=textFontSize*m,rotation=90)
			plt.setp(ax.get_xticklabels(),fontsize=gcafontSize*m)
			plt.setp(ax.get_yticklabels(),fontsize=gcafontSize*m)	
			text="time :"+str(np.round(time,5))
			ax.text(0.6,2.5,text,fontsize=textFontSize*m)		
			fig_name = 'p3_test'+str(it)+'.png'
			figure_path = './../figure/P3/convecion/'
			fig_fullpath = figure_path + fig_name
			fig.tight_layout()
			plt.savefig(fig_fullpath)
			plt.close()

	u = spysparselinalg.spsolve(A,B.dot(u))
	it   += 1
	time += dt

