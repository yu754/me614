import os
import sys
import numpy as np
import scipy.sparse as scysparse
from time import sleep
import scipy.sparse as scysparse
import scipy.sparse.linalg as spysparselinalg  # sparse linear algebra
import scipy.linalg as scylinalg               # non-sparse linear algebra
import pylab as plt
from matplotlib import rc as matplotlibrc
from pdb import set_trace
# Figure settings
matplotlibrc('text.latex', preamble='\usepackage{color}')
matplotlibrc('text',usetex=True)
matplotlibrc('font', family='serif')

fig_sol =0
fig_spy =1
figwidth       = 8
figheight      = 6
lineWidth      = 1.5
textFontSize   = 28*0.65
gcafontSize    = 30*0.65

fname="rms_rk1.dat"
fpath='./../data/'
rk1=np.loadtxt(fpath+fname)

fname="rms_rk2.dat"
fpath='./../data/'
rk2=np.loadtxt(fpath+fname)

fname="rms_rk3.dat"
fpath='./../data/'
rk3=np.loadtxt(fpath+fname)

fname="rms_rk4.dat"
fpath='./../data/'
rk4=np.loadtxt(fpath+fname)

re=np.array([40,60,80,100])
fig = plt.figure(0, figsize=(figwidth*1.8,figheight*0.7))

ax = fig.add_subplot(131)

ax.loglog(re,rk1[:,1],'k-',marker='s',mfc='None',label='rk1')
ax.loglog(re,rk2[:,1],'k-',marker='^',mfc='None',label='rk2')
ax.loglog(re,rk3[:,1],'k-',marker='o',mfc='None',label='rk3')
ax.loglog(re,rk4[:,1],'k-',marker='*',mfc='None',label='rk4')
ax.loglog(re,1./re**2/2.,'r--',label='2nd ref')
m=1.2
ax.set_xlabel(r"$Re$",fontsize=textFontSize*m)
ax.set_ylabel(r"$RMS$",fontsize=textFontSize*m,rotation=90)
plt.setp(ax.get_xticklabels(),fontsize=gcafontSize*m)
plt.setp(ax.get_yticklabels(),fontsize=gcafontSize*m)
plt.title("u",fontsize=textFontSize)
leg = plt.legend(loc='lower left', ncol=1,shadow=True, fancybox=True,fontsize=textFontSize*0.8)
leg.get_frame().set_alpha(0.8)


ax = fig.add_subplot(132)
ax.loglog(re,rk1[:,2],'k-',marker='s',mfc='None',label='rk1')
ax.loglog(re,rk2[:,2],'k-',marker='^',mfc='None',label='rk2')
ax.loglog(re,rk3[:,2],'k-',marker='o',mfc='None',label='rk3')
ax.loglog(re,rk4[:,2],'k-',marker='*',mfc='None',label='rk4')
plt.title("v",fontsize=textFontSize)
ax.loglog(re,1./re**2/2.,'r--',label='2nd ref')
ax.set_xlabel(r"$Re$",fontsize=textFontSize*m)
ax.set_ylabel(r"$RMS$",fontsize=textFontSize*m,rotation=90)
plt.setp(ax.get_xticklabels(),fontsize=gcafontSize*m)
plt.setp(ax.get_yticklabels(),fontsize=gcafontSize*m)


ax = fig.add_subplot(133)
ax.loglog(re,rk1[:,3],'k-',marker='s',mfc='None',label='rk1')
ax.loglog(re,rk2[:,3],'k-',marker='^',mfc='None',label='rk2')
ax.loglog(re,rk3[:,3],'k-',marker='o',mfc='None',label='rk3')
ax.loglog(re,rk4[:,3],'k-',marker='*',mfc='None',label='rk4')
ax.loglog(re,1./re**2/25.,'r--',label='2nd ref')
plt.title("p",fontsize=textFontSize)
ax.set_xlabel(r'$Re$',fontsize=textFontSize*m)
ax.set_ylabel(r"$RMS$",fontsize=textFontSize*m,rotation=90)
plt.setp(ax.get_xticklabels(),fontsize=gcafontSize*m)
plt.setp(ax.get_yticklabels(),fontsize=gcafontSize*m)

#ax.set_yscale('log')
# ax.loglog(dt_list,tuw1,'o-',label='1st-upwind')
# ax.loglog(dt_list,tuw2,'o-',label='2nd-upwind')
# ax.loglog(dt_list,tcd2,'o-',label='2nd-cds')

# ax.loglog(dt_list,dt_list**(-2.)/1000000000000,'k--',label='2nd-ref',lw=lineWidth)
# m=1.2
# ax.set_xlabel(r"$1/dt$",fontsize=textFontSize*m)
# ax.set_ylabel(r"$RMS$",fontsize=textFontSize*m,rotation=90)
# plt.setp(ax.get_xticklabels(),fontsize=gcafontSize*m)
# plt.setp(ax.get_yticklabels(),fontsize=gcafontSize*m)
# leg = plt.legend(loc='best', ncol=1,  shadow=False, fancybox=True,fontsize=textFontSize)
# leg.get_frame().set_alpha(0.8)



fig.tight_layout()
#fig_name = 'p3_diff.png'


fig_name = 'p2_rk_.png'
figure_path = './../figure/'
fig_fullpath = figure_path + fig_name
plt.savefig(fig_fullpath)
plt.show()
plt.close()