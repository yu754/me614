import os
import sys
import numpy as np
import scipy.sparse as scysparse
from pdb import set_trace
from time import sleep
import spatial_operators
import spatial_operators_uv2 as opuv
import scipy.sparse as scysparse
import scipy.sparse.linalg as spysparselinalg  # sparse linear algebra
import scipy.linalg as scylinalg               # non-sparse linear algebra
import pylab as plt
from matplotlib import rc as matplotlibrc
import iteration as it
import time # has the equivalent of tic/toc

data=np.array([[40 ,0.0002650548898876612 ,0.0002650548898876612 ,1.993768541684603e-05],
[60 ,0.00011019928159302973, 0.00011019928159302923,9.134948753362732e-06],
[80 ,5.604988165177808e-05 ,5.604988165177822e-05 ,5.1931799183017715e-06],
[100, 3.099523452064348e-05, 3.099523452064315e-05, 3.339852142262247e-06]])

#data=np.array([[40 ,3.9081867780634505e-07, 3.9081867780082843e-07, 0.0030608812802681374],
#[60 ,1.6267246362379175e-07, 1.626724636275151e-07 ,0.0012785398725477651],
#[80 ,8.27720230051572e-08 ,8.277202300001802e-08 ,0.0006298994622786652],
#[100, 4.578088190965065e-08, 4.578088190954159e-08 ,0.00032522311028369393]])

a=1./data[:,0]
plt.loglog(data[:,0],data[:,2])
plt.loglog(data[:,0],(a)**2,'r--')
plt.show()

A = scysparse.csr_matrix((3.,3.),dtype="float64")

A.setdiag(3.)
print A.toarray()