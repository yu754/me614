import os
import sys
import numpy as np
import scipy.sparse as scysparse
from pdb import set_trace
from time import sleep
import spatial_operators
import scipy.sparse as scysparse
import scipy.sparse.linalg as spysparselinalg  # sparse linear algebra
import scipy.linalg as scylinalg               # non-sparse linear algebra
import pylab as plt
from matplotlib import rc as matplotlibrc
import iteration as it
import time # has the equivalent of tic/toc

# Figure settings 
matplotlibrc('text.latex', preamble='\usepackage{color}')
matplotlibrc('text',usetex=True)
matplotlibrc('font', family='serif')


figwidth       = 10
figheight      = 6
lineWidth      = 1.5*2
textFontSize   = 28*0.5
gcafontSize    = 30*0.5


machine_epsilon = np.finfo(float).eps

Di="Homogeneous Dirichlet"
Nu="Homogeneous Neumann"
BC=Di
Re=1.
n=1
N=128
n_list=np.array([2,8,16])
for iin,n in enumerate(n_list):
	#########################################
	############### User Input ##############

	# number of (pressure) cells = mass conservation cells
	Nxc  = N
	Nyc  = N
	Np   = Nxc*Nyc
	Lx   = 1.
	Ly   = 1.

	#########################################
	######## Preprocessing Stage ############

	# You might have to include ghost-cells here
	# Depending on your application

	# define grid for u and v velocity components first
	xsi_u = np.linspace(0.,1.0,Nxc+1)
	xsi_v = np.linspace(0.,1.0,Nyc+1)
	# uniform grid
	xu = (xsi_u)*Lx
	yv = (xsi_v)*Ly
	# (non-sense non-uniform grid)
	#xu = (xsi_u**4.)*Lx # can be replaced by non-uniform grid
	#yv = (xsi_v**4.)*Ly # can be replaced by non-uniform grid

	# creating ghost cells
	dxu0 = np.diff(xu)[0]
	dxuL = np.diff(xu)[-1]
	xu = np.concatenate([[xu[0]-dxu0],xu,[xu[-1]+dxuL]])

	dyv0 = np.diff(yv)[0]
	dyvL = np.diff(yv)[-1]
	yv = np.concatenate([[yv[0]-dyv0],yv,[yv[-1]+dyvL]])

	dxc = np.diff(xu)  # pressure-cells spacings
	dyc = np.diff(yv)

	xc = 0.5*(xu[:-1]+xu[1:])  # total of Nxc cells
	yc = 0.5*(yv[:-1]+yv[1:])  # total of Nyc cells

	# note that indexing is Xc[j_y,i_x] or Xc[j,i]
	[Xc,Yc]   = np.meshgrid(xc,yc)     # these arrays are with ghost cells
	[Dxc,Dyc] = np.meshgrid(dxc,dyc)   # these arrays are with ghost cells


	pressureCells_Mask = np.zeros(Xc.shape)
	pressureCells_Mask[1:-1,1:-1] = True


	# number of actual pressure cells
	Np = len(np.where(pressureCells_Mask==True)[0])


	f=-2*np.pi*n*np.cos(2*np.pi*n*Xc)*np.sin(2*np.pi*n*Yc)\
		+1./Re*(-8.*np.pi*np.pi*n*n*np.sin(2*np.pi*n*Xc)*np.sin(2*np.pi*n*Yc))

	f=f[np.where(pressureCells_Mask==True)]
	print "constructing operators.."
	DivGrad = spatial_operators.create_DivGrad_operator(Dxc,Dyc,Xc,Yc,pressureCells_Mask,boundary_conditions=BC)
	Grad_x = spatial_operators.Grad_px_operator(Dxc,Dyc,Xc,Yc,pressureCells_Mask,boundary_conditions=Di)

	AA=DivGrad+Grad_x
	#phi = spysparselinalg.spsolve(AA,f) # solving nabla*phi = 1
	#w1=np.array([0.2,0.4,0.6,0.8])


	w_list=np.linspace(1,1.99,10)
	#w_list= w_list[5:7]





	###########################################################
	# 					find w uniformly
	kk=np.zeros(w_list.shape)
	for ii,w in enumerate(w_list):
		phi,rr,kk[ii]=it.Gauss_Seidel_sparse(AA,f,w)

	kk_mask=1.0*kk

	ic=np.argmin(kk)
	a=w_list[ic-1]
	b=w_list[ic+1]
	###########################################################
	# 					find w through half

	a0=1.0*a
	b0=1.0*b
	phi,rr,kka=it.Gauss_Seidel_sparse(AA,f,a)
	phi,rr,kkb=it.Gauss_Seidel_sparse(AA,f,b)
	kka0=kka*1.0
	kkb0=kkb*1.0
	c=np.zeros(10)
	kkk=np.zeros(10)
	print "begin loop"
	for i in range(10):
		c[i]=0.5*(a+b)
		phi,rr,kkk[i]=it.Gauss_Seidel_sparse(AA,f,c[i])
		print '==========================='
		print a,b,c[i]
		print kka,kkb,kkk[i]
		if (kka<np.abs(kkb)):
			b=c[i]*1.0
			phi,rr,kkb=it.Gauss_Seidel_sparse(AA,f,b)
		elif (np.abs(kka)>np.abs(kkb)):
			a=c[i]*1.0	
			phi,rr,kka=it.Gauss_Seidel_sparse(AA,f,a)
		else:
			d=0.25*(b-a)+a
			e=0.75*(b-a)+a		
			phi,rr,kkd=it.Gauss_Seidel_sparse(AA,f,d)
			phi,rr,kke=it.Gauss_Seidel_sparse(AA,f,e)
			if (kkd<kke):
				b=c[i]*1.0
				phi,rr,kkb=it.Gauss_Seidel_sparse(AA,f,b)	
			else :
				a=c[i]*1.0	
				phi,rr,kka=it.Gauss_Seidel_sparse(AA,f,a)				





	fname="p1opt_N_"+str(N)+"_n_"+str(n) +".dat"	
	fpath='./../data/opt/'
	fout = open(fpath+fname, 'w')
	for k in range(len(w_list)):
	     fout.write(str(w_list[k])+" "+str(kk[k])+"\n")

	for k in range(len(c)):
	     fout.write(str(c[k])+" "+str(kkk[k])+"\n")
	fout.close()

	fname="p1res_N_"+str(N)+"_n_"+str(n) +".dat"	
	fpath='./../data/opt/'
	fout = open(fpath+fname, 'w')
	for k in range(len(rr)):
	     fout.write(str(k+1)+' '+str(rr[k])+"\n")

	fout.close()	
	# consider pre-factorization with LU decomposition
	# will speed up Poisson solver by x10

if False:
	# pouring flattened solution back in 2D array
	Phi = np.zeros((Nyc+2,Nxc+2))*np.nan # remember ghost cells
	#set_trace()
	Phi[np.where(pressureCells_Mask==True)] = phi

	fig = plt.figure(0, figsize=(figwidth,figheight))
	ax   = fig.add_axes([0.15,0.15,0.8,0.8])
	plt.axes(ax)#matrix_rank
	plt.contourf(Xc,Yc,Phi)
	plt.colorbar()
	# plt.plot(Phi[3,1:-2])
	ax.grid('on',which='both')
	plt.setp(ax.get_xticklabels(),fontsize=gcafontSize)
	plt.setp(ax.get_yticklabels(),fontsize=gcafontSize)
	ax.set_xlabel(r"$x$",fontsize=textFontSize)
	ax.set_ylabel(r"$y$",fontsize=textFontSize,rotation=90)
	plt.axis("tight")
	#plt.axis("equal")

	plt.show()
