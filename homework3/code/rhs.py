import os
import sys
import numpy as np
import scipy.sparse as scysparse
from pdb import set_trace
from time import sleep
import spatial_operators
import spatial_operators_uv2 as opuv
import scipy.sparse as scysparse
import scipy.sparse.linalg as spysparselinalg  # sparse linear algebra
import scipy.linalg as scylinalg               # non-sparse linear algebra
import pylab as plt
from matplotlib import rc as matplotlibrc
import iteration as it
import time # has the equivalent of tic/toc
from numpy import linalg as LA
def get_rhs1(intx,inty,div_x,div_y,grad_x,grad_y,u,v,nu):

	#get Fxc  at faces of u mesh
	uu_x=(intx.dot(u))**2
	uv_y=intx.dot(v)*inty.dot(u)
	#get Fxv  at faces of u mesh
	Txx=grad_x.dot(u)*2.0*nu
	Txy=(grad_x.dot(v)+grad_y.dot(u))*0.5*2.0*nu

	rhsu=div_x.dot(-uu_x+Txx)+div_y.dot(-uv_y+Txy)


	vv_y=inty.dot(v)**2
	vu_x=uv_y

	Tyx=Txy
	Tyy=grad_y.dot(v)*2.0*nu

	rhsv=div_x.dot(-vu_x+Tyx)+div_y.dot(-vv_y+Tyy)

	if False:
		fig = plt.figure(0)
		ax   = fig.add_axes([0.15,0.15,0.8,0.8])

		plt.axes(ax)#matrix_rank
		plt.contourf(rhsv.reshape(100,100))
		plt.colorbar()
		plt.show()

	return rhsu,rhsv


def get_rhs2(DivGrad,div_x,div_y,grad_x,grad_y,u_star,v_star,dt,pref,rho):

	#pressure correcrion

	#rhs q=div(u_star)/dt

	q=(div_x.dot(u_star)+div_y.dot(v_star))/dt

	#modify DivGrad, remove singular
	DivGrad[1,:]=0.
	DivGrad[1,1]=1.	
	q[1]=pref


	# DivGrad[1,:]=1.
	# q[1]=0.

	#solve p
	p=spysparselinalg.spsolve(DivGrad,q)

	#gradient p, go to velocity face
	rhsu2 = -grad_x.dot(p)/rho
	rhsv2 = -grad_y.dot(p)/rho


	return rhsu2,rhsv2,p

def get_rhs1_test(intx,inty,div_x,div_y,grad_x,grad_y,u,v,nu,dx):
	u=u.reshape(50,50)
	v=v.reshape(50,50)
	#get Fxc  at faces of u mesh
	uu_x=u**2
	uv_y=u*v
	#get Fxv  at faces of u mesh
	#Txx=grad_x.dot(u)*2*nu
	
	Txx=np.gradient(u,dx,axis=1)*2*nu
	#Txy=(grad_x.dot(v)+grad_y.dot(u))*0.5*2*nu
	Txy=(np.gradient(v,dx,axis=1)+np.gradient(u,dx,axis=0))*0.5*2*nu

	#rhsu=div_x.dot(-uu_x+Txx)+div_y.dot(-uv_y+Txy)
	rhsu=np.gradient(-uu_x+Txx,dx,axis=1)+np.gradient(-uv_y+Txy,dx,axis=0)


	vv_y=v**2
	vu_x=u*v

	Tyx=Txy
	#Tyy=grad_y.dot(v)*2*nu
	Tyy=np.gradient(v,dx,axis=0)*2*nu

	#rhsv=div_x.dot(-vu_x+Tyx)+div_y.dot(-vv_y+Tyy)
	rhsv=np.gradient(-vu_x+Tyx,dx,axis=1)+np.gradient(-vv_y+Tyy,dx,axis=0)	

	fig = plt.figure(0)
	ax   = fig.add_axes([0.15,0.15,0.8,0.8])

	plt.axes(ax)#matrix_rank

	set_trace()
	#aa=np.gradient(v,dx,axis=1)
	c=plt.contourf(rhsu)
	#c.set_clim(-1., 1.)
	plt.colorbar()
	plt.show()

	return rhsu,rhsv