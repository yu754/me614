import os
import sys
import numpy as np
import scipy.sparse as scysparse
from pdb import set_trace
from time import sleep
import spatial_operators
import spatial_operators_uv2 as opuv
import scipy.sparse as scysparse
import scipy.sparse.linalg as spysparselinalg  # sparse linear algebra
import scipy.linalg as scylinalg               # non-sparse linear algebra
import pylab as plt
from matplotlib import rc as matplotlibrc
import iteration as it
import time # has the equivalent of tic/toc
import rhs as rrhs

def report_cfl(u,v,dt,dx,nu):
	umag=(u**2+v**2)**0.5
	cfl_con	 =np.max(umag)*dt/dx
	cfl_vis  =nu*dt/dx**2	
	print "cfl_con:",cfl_con
	print "cfl_vis:",cfl_vis

def rk_rhs1(intx,inty,div_x,div_y,grad_x,grad_y,u_old,v_old,rk,dt,nu):

	if (rk==1):

		kx1,ky1=rrhs.get_rhs1(intx,inty,div_x,div_y,grad_x,grad_y,u_old,v_old,nu)
		u_new=u_old+kx1*dt
		v_new=v_old+ky1*dt
		return u_new,v_new

	if (rk==2):

		kx1,ky1=rrhs.get_rhs1(intx,inty,div_x,div_y,grad_x,grad_y,u_old,v_old,nu)
		u_bar=u_old+kx1*dt
		v_bar=v_old+ky1*dt

		#update u_new
		kx2,ky2=rrhs.get_rhs1(intx,inty,div_x,div_y,grad_x,grad_y,u_bar,v_bar,nu)
		u_bar=u_old+(kx1+kx2)*0.5*dt
		v_bar=v_old+(ky1+ky2)*0.5*dt

		return u_bar,v_bar

	if (rk==3):

		kx1,ky1=rrhs.get_rhs1(intx,inty,div_x,div_y,grad_x,grad_y,u_old,v_old,nu)

		# at get u(0.5dt)
		u_bar=u_old+kx1*dt*0.5	
		v_bar=v_old+ky1*dt*0.5
		kx2,ky2=rrhs.get_rhs1(intx,inty,div_x,div_y,grad_x,grad_y,u_bar,v_bar,nu)

		# at get u(1 dt)
		u_bar=u_old+(-kx1+2.*kx2)*dt
		v_bar=v_old+(-ky1+2.*ky2)*dt
		kx3,ky3=rrhs.get_rhs1(intx,inty,div_x,div_y,grad_x,grad_y,u_bar,v_bar,nu)	

		#update u_new
		u_bar=u_old+(kx1+4.*kx2+kx3)/6.*dt
		v_bar=v_old+(ky1+4.*ky2+ky3)/6.*dt

		return u_bar,v_bar



	if (rk==4):

		kx1,ky1=rrhs.get_rhs1(intx,inty,div_x,div_y,grad_x,grad_y,u_old,v_old,nu)

		# at get u(0.5dt)
		u_bar=u_old+kx1*dt*0.5	
		v_bar=v_old+ky1*dt*0.5
		kx2,ky2=rrhs.get_rhs1(intx,inty,div_x,div_y,grad_x,grad_y,u_bar,v_bar,nu)

		# at get u(0.5 dt) with k2
		u_bar=u_old+kx2*dt*0.5	
		v_bar=v_old+ky2*dt*0.5
		kx3,ky3=rrhs.get_rhs1(intx,inty,div_x,div_y,grad_x,grad_y,u_bar,v_bar,nu)	

		u_bar=u_old+kx3*dt
		v_bar=v_old+ky3*dt
		kx4,ky4=rrhs.get_rhs1(intx,inty,div_x,div_y,grad_x,grad_y,u_bar,v_bar,nu)

		#update u_new
		u_bar=u_old+(kx1+2.*kx2+2.*kx3+kx4)/6.*dt
		v_bar=v_old+(ky1+2.*ky2+2.*ky3+ky4)/6.*dt

		return u_bar,v_bar

def rk_rhs2(DivGrad,div_x,div_y,grad_x,grad_y,u_old,v_old,rk,dt,pref,rho):

	if (rk==1):
		kx1,ky1,p1=rrhs.get_rhs2(DivGrad,div_x,div_y,grad_x,grad_y,u_old,v_old,dt,pref,rho)
		u_new=u_old+kx1*dt
		v_new=v_old+ky1*dt		
		return u_new,v_new,p1

	if (rk==2):

		kx1,ky1,p1=rrhs.get_rhs2(DivGrad,div_x,div_y,grad_x,grad_y,u_old,v_old,dt,pref,rho)
		u_bar=u_old+kx1*dt
		v_bar=v_old+ky1*dt

		#update u_new
		kx2,ky2,p2=rrhs.get_rhs2(DivGrad,div_x,div_y,grad_x,grad_y,u_bar,v_bar,dt,pref,rho)
		u_bar=u_old+(kx1+kx2)*0.5*dt
		v_bar=v_old+(ky1+ky2)*0.5*dt

		return u_bar,v_bar,0.5*(p1+p2)

	if (rk==3):

		kx1,ky1,p1=rrhs.get_rhs2(DivGrad,div_x,div_y,grad_x,grad_y,u_old,v_old,dt,pref,rho)

		# at get u(0.5dt)
		u_bar=u_old+kx1*dt*0.5	
		v_bar=v_old+ky1*dt*0.5
		kx2,ky2,p2=rrhs.get_rhs2(DivGrad,div_x,div_y,grad_x,grad_y,u_bar,v_bar,dt,pref,rho)

		# at get u(1 dt)
		u_bar=u_old+(-kx1+2.*kx2)*dt
		v_bar=v_old+(-ky1+2.*ky2)*dt
		kx3,ky3,p3=rrhs.get_rhs2(DivGrad,div_x,div_y,grad_x,grad_y,u_bar,v_bar,dt,pref,rho)	

		#update u_new
		u_bar=u_old+(kx1+4.*kx2+kx3)/6.*dt
		v_bar=v_old+(ky1+4.*ky2+ky3)/6.*dt

		return u_bar,v_bar,(p1+4.*p2+p3)/6.

	if (rk==4):

		kx1,ky1,p1=rrhs.get_rhs2(DivGrad,div_x,div_y,grad_x,grad_y,u_old,v_old,dt,pref,rho)

		# at get u(0.5dt)
		u_bar=u_old+kx1*dt*0.5	
		v_bar=v_old+ky1*dt*0.5
		kx2,ky2,p2=rrhs.get_rhs2(DivGrad,div_x,div_y,grad_x,grad_y,u_bar,v_bar,dt,pref,rho)

		# at get u(0.5 dt) with k2
		u_bar=u_old+kx2*dt*0.5	
		v_bar=v_old+ky2*dt*0.5
		kx3,ky3,p3=rrhs.get_rhs2(DivGrad,div_x,div_y,grad_x,grad_y,u_bar,v_bar,dt,pref,rho)	

		u_bar=u_old+kx3*dt
		v_bar=v_old+ky3*dt
		kx4,ky4,p4=rrhs.get_rhs2(DivGrad,div_x,div_y,grad_x,grad_y,u_bar,v_bar,dt,pref,rho)

		#update u_new
		u_bar=u_old+(kx1+2.*kx2+2.*kx3+kx4)/6.*dt
		v_bar=v_old+(ky1+2.*ky2+2.*ky3+ky4)/6.*dt

		return u_bar,v_bar, (p1+ 2.*p2+ 2.*p3+ p4)/6.



def get_unew(DivGrad,intx,inty,div_x,div_y,grad_x,grad_y,u_old,v_old,rk,dt,pref,nu,rho):

	#projection
	u_star,v_star = rk_rhs1(intx,inty,div_x,div_y,grad_x,grad_y,u_old,v_old,rk,dt,nu)

	#correction always call rk1
	u_new, v_new,p = rk_rhs2(DivGrad,div_x,div_y,grad_x,grad_y,u_star,v_star,1,dt,pref,rho)
	# set_trace()
	#print "u_star","u_new",u_star,u_new
	return u_new,v_new,p
	# return u_new, v_new