import os
import sys
import numpy as np
import scipy.sparse as scysparse
from pdb import set_trace
from time import sleep
import spatial_operators
import scipy.sparse as scysparse
import scipy.sparse.linalg as spysparselinalg  # sparse linear algebra
import scipy.linalg as scylinalg               # non-sparse linear algebra
import pylab as plt
from matplotlib import rc as matplotlibrc
import time # has the equivalent of tic/toc

# Figure settings 
matplotlibrc('text.latex', preamble='\usepackage{color}')
matplotlibrc('text',usetex=True)
matplotlibrc('font', family='serif')


figwidth       = 10
figheight      = 6
lineWidth      = 1.5*2
textFontSize   = 28*0.5
gcafontSize    = 30*0.5


machine_epsilon = np.finfo(float).eps

#########################################
############### User Input ##############

# number of (pressure) cells = mass conservation cells
Nxc  = 5
Nyc  = 5
Np   = Nxc*Nyc
Lx   = 2.*np.pi
Ly   = 2.*np.pi

#########################################
######## Preprocessing Stage ############

# You might have to include ghost-cells here
# Depending on your application

# define grid for u and v velocity components first
xsi_u = np.linspace(0.,1.0,Nxc+1)
xsi_v = np.linspace(0.,1.0,Nyc+1)
# uniform grid
xu = (xsi_u)*Lx
yv = (xsi_v)*Ly
# (non-sense non-uniform grid)
#xu = (xsi_u**4.)*Lx # can be replaced by non-uniform grid
#yv = (xsi_v**4.)*Ly # can be replaced by non-uniform grid

# creating ghost cells
dxu0 = np.diff(xu)[0]
dxuL = np.diff(xu)[-1]
xu = np.concatenate([[xu[0]-dxu0],xu,[xu[-1]+dxuL]])

dyv0 = np.diff(yv)[0]
dyvL = np.diff(yv)[-1]
yv = np.concatenate([[yv[0]-dyv0],yv,[yv[-1]+dyvL]])

dxc = np.diff(xu)  # pressure-cells spacings
dyc = np.diff(yv)

xc = 0.5*(xu[:-1]+xu[1:])  # total of Nxc cells
yc = 0.5*(yv[:-1]+yv[1:])  # total of Nyc cells

# note that indexing is Xc[j_y,i_x] or Xc[j,i]
[Xc,Yc]   = np.meshgrid(xc,yc)     # these arrays are with ghost cells
[Dxc,Dyc] = np.meshgrid(dxc,dyc)   # these arrays are with ghost cells

### familiarize yourself with 'flattening' options
# phi_Fordering     = Phi.flatten(order='F') # F (column-major), Fortran/Matlab default
# phi_Cordering     = Phi.flatten(order='C') # C (row-major), C/Python default
# phi_PythonDefault = Phi.flatten()          # Python default
# compare Phi[:,0] and Phi[0,:] with phi[:Nxc]

# Pre-allocated at all False = no fluid points
pressureCells_Mask = np.zeros(Xc.shape)
pressureCells_Mask[1:-1,1:-1] = True

# Introducing obstacle in pressure Mask
obstacle_radius = 0.3*Lx # set equal to 0.0*Lx to remove obstacle
distance_from_center = np.sqrt(np.power(Xc-Lx/2.,2.0)+np.power(Yc-Ly/2.,2.0))
j_obstacle,i_obstacle = np.where(distance_from_center<obstacle_radius)
pressureCells_Mask[j_obstacle,i_obstacle] = True

# number of actual pressure cells
Np = len(np.where(pressureCells_Mask==True)[0])
q  = np.ones(Np,)

Di="Homogeneous Dirichlet"
Nu="Homogeneous Neumann"
BC=Nu
DivGrad = spatial_operators.create_DivGrad_operator(Dxc,Dyc,Xc,Yc,pressureCells_Mask,boundary_conditions=BC)
# if boundary_conditions are not specified, it defaults to "Homogeneous Neumann"

phi = spysparselinalg.spsolve(DivGrad,q) # solving nabla*phi = 1
# consider pre-factorization with LU decomposition
# will speed up Poisson solver by x10

# pouring flattened solution back in 2D array
Phi = np.zeros((Nyc+2,Nxc+2))*np.nan # remember ghost cells
# keyboard()
Phi[np.where(pressureCells_Mask==True)] = phi


# Plot solution
fig = plt.figure(0, figsize=(figwidth,figheight))
#ax   = fig.add_axes([0.15,0.15,0.8,0.8])
ax = fig.add_subplot(111)
ax.spy(DivGrad)
m=1.2
ax.set_xlabel(r"$x$",fontsize=textFontSize*m)
ax.set_ylabel(r"$y$",fontsize=textFontSize*m,rotation=90)
plt.title(BC,fontsize=textFontSize*m)
plt.setp(ax.get_xticklabels(),fontsize=gcafontSize*m)
plt.setp(ax.get_yticklabels(),fontsize=gcafontSize*m)
fig.tight_layout()

fig_name = 'p1_spy_Nu.png'
figure_path = './../figure/'
fig_fullpath = figure_path + fig_name
plt.savefig(fig_fullpath)
plt.show()
plt.close()

plt.show()