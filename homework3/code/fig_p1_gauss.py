import os
import sys
import numpy as np
import scipy.sparse as scysparse
from time import sleep
import scipy.sparse as scysparse
import scipy.sparse.linalg as spysparselinalg  # sparse linear algebra
import scipy.linalg as scylinalg               # non-sparse linear algebra
import pylab as plt
from matplotlib import rc as matplotlibrc
from pdb import set_trace
# Figure settings
matplotlibrc('text.latex', preamble='\usepackage{color}')
matplotlibrc('text',usetex=True)
matplotlibrc('font', family='serif')

fig_sol =0
fig_spy =1
figwidth       = 8
figheight      = 6
lineWidth      = 1.5
textFontSize   = 28*0.65
gcafontSize    = 30*0.65

fname="p1opt_N_64_n_1.dat"
fpath='./../data/opt/'
N64_1=np.loadtxt(fpath+fname)
N64_1=N64_1[np.argsort(N64_1[:,0])]

fname="p1opt_N_64_n_4.dat"
fpath='./../data/opt/'
N64_4=np.loadtxt(fpath+fname)
N64_4=N64_4[np.argsort(N64_4[:,0])]

fname="p1opt_N_64_n_8.dat"
fpath='./../data/opt/'
N64_8=np.loadtxt(fpath+fname)
N64_8=N64_8[np.argsort(N64_8[:,0])]



fname="p1opt_N_128_n_2.dat"
fpath='./../data/opt/'
N128_2=np.loadtxt(fpath+fname)
N128_2=N128_2[np.argsort(N128_2[:,0])]

fname="p1opt_N_128_n_8.dat"
fpath='./../data/opt/'
N128_8=np.loadtxt(fpath+fname)
N128_8=N128_8[np.argsort(N128_8[:,0])]


fname="p1opt_N_128_n_16.dat"
fpath='./../data/opt/'
N128_16=np.loadtxt(fpath+fname)
N128_16=N128_16[np.argsort(N128_16[:,0])]




fname="p1opt_N_256_n_2.dat"
fpath='./../data/opt/'
N256_2=np.loadtxt(fpath+fname)
N256_2=N256_2[np.argsort(N256_2[:,0])]

fname="p1opt_N_256_n_8.dat"
fpath='./../data/opt/'
N256_8=np.loadtxt(fpath+fname)
N256_8=N256_8[np.argsort(N256_8[:,0])]

fname="p1opt_N_256_n_32.dat"
fpath='./../data/opt/'
N256_32=np.loadtxt(fpath+fname)
N256_32=N256_32[np.argsort(N256_32[:,0])]


#########################################################################
#########################################################################

fname="p1res_N_64_n_1.dat"
fpath='./../data/opt/'
r64_1=np.loadtxt(fpath+fname)


fname="p1res_N_64_n_4.dat"
fpath='./../data/opt/'
r64_4=np.loadtxt(fpath+fname)


fname="p1res_N_64_n_8.dat"
fpath='./../data/opt/'
r64_8=np.loadtxt(fpath+fname)




fname="p1res_N_128_n_2.dat"
fpath='./../data/opt/'
r128_2=np.loadtxt(fpath+fname)


fname="p1res_N_128_n_8.dat"
fpath='./../data/opt/'
r128_8=np.loadtxt(fpath+fname)



fname="p1res_N_128_n_16.dat"
fpath='./../data/opt/'
r128_16=np.loadtxt(fpath+fname)





fname="p1res_N_256_n_2.dat"
fpath='./../data/opt/'
r256_2=np.loadtxt(fpath+fname)
r256_2=r256_2[np.argsort(r256_2[:,0])]

fname="p1res_N_256_n_8.dat"
fpath='./../data/opt/'
r256_8=np.loadtxt(fpath+fname)
r256_8=r256_8[np.argsort(r256_8[:,0])]

fname="p1res_N_256_n_32.dat"
fpath='./../data/opt/'
r256_32=np.loadtxt(fpath+fname)
r256_32=r256_32[np.argsort(r256_32[:,0])]



set_trace()
fig = plt.figure(0, figsize=(figwidth*1.8,figheight))

ax = fig.add_subplot(131)

ax.plot(N64_1[:,0],N64_1[:,1],'g:',label='N64 n1')
ax.plot(N64_4[:,0],N64_4[:,1],'g--',label='N64 n4')
ax.plot(N64_8[:,0],N64_8[:,1],'g-',label='N64 n8')
ax.scatter(N64_1[np.argmin(N64_1[:,1]),0],np.min(N64_1[:,1]),marker='^',color='g')
ax.scatter(N64_4[np.argmin(N64_4[:,1]),0],np.min(N64_4[:,1]),marker='s',color='g')
ax.scatter(N64_8[np.argmin(N64_8[:,1]),0],np.min(N64_8[:,1]),marker='o',color='g')

ax.plot( N128_2[:,0],N128_2[:,1],'r:',label='N128 n2')
ax.plot( N128_8[:,0],N128_8[:,1],'r--',label='N128 n8')
ax.plot(N128_16[:,0],N128_16[:,1],'r-',label='N128 n16')
ax.scatter(N128_2[np.argmin(N128_2[:,1]),0],np.min(N128_2[:,1]),marker='^',color='r')
ax.scatter(N128_8[np.argmin(N128_8[:,1]),0],np.min(N128_8[:,1]),marker='s',color='r')
ax.scatter(N128_16[np.argmin(N128_16[:,1]),0],np.min(N128_16[:,1]),marker='o',color='r')

ax.plot( N256_2[:,0], N256_2[:,1],'k:',label='N256 n2')
ax.plot( N256_8[:,0], N256_8[:,1],'k--',label='N256 n8')
ax.plot( N256_32[:,0],N256_32[:,1],'k-',label='N256 n32')
ax.scatter(N256_2[np.argmin(N256_2[:,1]),0],np.min(N256_2[:,1]),marker='^',color='k')
ax.scatter(N256_8[np.argmin(N256_8[:,1]),0],np.min(N256_8[:,1]),marker='s',color='k')
ax.scatter(N256_32[np.argmin(N256_32[:,1]),0],np.min(N256_32[:,1]),marker='o',color='k')
ax.set_yscale('log')
#ax.scatter(N64_1[np.min(N64_1[:,1],1],np.argmin(N64_1[:,1])))#,marker='s',mfc='g')
m=1.2
ax.set_xlabel(r"$sor:w$",fontsize=textFontSize*m)
ax.set_ylabel(r"iteration:k",fontsize=textFontSize*m,rotation=90)
plt.setp(ax.get_xticklabels(),fontsize=gcafontSize*m)
plt.setp(ax.get_yticklabels(),fontsize=gcafontSize*m)
leg = plt.legend(loc='upper left', ncol=1,shadow=True, fancybox=True,fontsize=textFontSize*0.8)
leg.get_frame().set_alpha(0.8)


ax = fig.add_subplot(132)
ax.loglog(r64_1[:,0],r64_1[:,1],'g:',label='N64 n1')
ax.loglog(r64_4[:,0],r64_4[:,1],'g--',label='N64 n4')
ax.loglog(r64_8[:,0],r64_8[:,1],'g-',label='N64 n8')
ax.loglog( r128_2[:,0],r128_2[:,1],'r:',label='N128 n2')
ax.loglog( r128_8[:,0],r128_8[:,1],'r--',label='N128 n8')
ax.loglog(r128_16[:,0],r128_16[:,1],'r-',label='N128 n16')
ax.loglog( r256_2[:,0], r256_2[:,1],'k:',label='N256 n2')
ax.loglog( r256_8[:,0], r256_8[:,1],'k--',label='N256 n8')
ax.loglog( r256_32[:,0],r256_32[:,1],'k-',label='N256 n32')
ax.set_xlabel("iteration:k",fontsize=textFontSize*m)
ax.set_ylabel(r"$||r^k||$",fontsize=textFontSize*m,rotation=90)
plt.setp(ax.get_xticklabels(),fontsize=gcafontSize*m)
plt.setp(ax.get_yticklabels(),fontsize=gcafontSize*m)


ax = fig.add_subplot(133)
ax.loglog(r64_1[:,0],r64_1[:,1]/r64_1[0,1],'g:',label='N64 n1')
ax.loglog(r64_4[:,0],r64_4[:,1]/r64_4[0,1],'g--',label='N64 n4')
ax.loglog(r64_8[:,0],r64_8[:,1]/r64_8[0,1],'g-',label='N64 n8')
ax.loglog( r128_2[:,0],r128_2[:,1]/r128_2[0,1],'r:',label='N128 n2')
ax.loglog( r128_8[:,0],r128_8[:,1]/r128_8[0,1],'r--',label='N128 n8')
ax.loglog(r128_16[:,0],r128_16[:,1]/r128_16[0,1],'r-',label='N128 n16')
ax.loglog( r256_2[:,0], r256_2[:,1]/r256_2[0,1],'k:',label='N256 n2')
ax.loglog( r256_8[:,0], r256_8[:,1]/r256_8[0,1],'k--',label='N256 n8')
ax.loglog( r256_32[:,0],r256_32[:,1]/r256_32[0,1],'k-',label='N256 n32')
ax.set_xlabel("iteration:k",fontsize=textFontSize*m)
ax.set_ylabel(r"$||r^k||/||r^0||$",fontsize=textFontSize*m,rotation=90)
plt.setp(ax.get_xticklabels(),fontsize=gcafontSize*m)
plt.setp(ax.get_yticklabels(),fontsize=gcafontSize*m)

#ax.set_yscale('log')
# ax.loglog(dt_list,tuw1,'o-',label='1st-upwind')
# ax.loglog(dt_list,tuw2,'o-',label='2nd-upwind')
# ax.loglog(dt_list,tcd2,'o-',label='2nd-cds')

# ax.loglog(dt_list,dt_list**(-2.)/1000000000000,'k--',label='2nd-ref',lw=lineWidth)
# m=1.2
# ax.set_xlabel(r"$1/dt$",fontsize=textFontSize*m)
# ax.set_ylabel(r"$RMS$",fontsize=textFontSize*m,rotation=90)
# plt.setp(ax.get_xticklabels(),fontsize=gcafontSize*m)
# plt.setp(ax.get_yticklabels(),fontsize=gcafontSize*m)
# leg = plt.legend(loc='best', ncol=1,  shadow=False, fancybox=True,fontsize=textFontSize)
# leg.get_frame().set_alpha(0.8)



fig.tight_layout()
#fig_name = 'p3_diff.png'


fig_name = 'p1_gauss_.png'
figure_path = './../figure/'
fig_fullpath = figure_path + fig_name
plt.savefig(fig_fullpath)
plt.show()
plt.close()