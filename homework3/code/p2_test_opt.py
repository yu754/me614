import os
import sys
import numpy as np
import scipy.sparse as scysparse
from pdb import set_trace
from time import sleep
import spatial_operators
import spatial_operators_uv2 as opuv
import scipy.sparse as scysparse
import scipy.sparse.linalg as spysparselinalg  # sparse linear algebra
import scipy.linalg as scylinalg               # non-sparse linear algebra
import pylab as plt
from matplotlib import rc as matplotlibrc
import iteration as it
import time # has the equivalent of tic/toc
import time_adv as tim_adv
import rhs as rrhs
# Figure settings 
matplotlibrc('text.latex', preamble='\usepackage{color}')
matplotlibrc('text',usetex=True)
matplotlibrc('font', family='serif')


figwidth       = 10
figheight      = 6
lineWidth      = 1.5*2
textFontSize   = 28*0.5
gcafontSize    = 30*0.5


machine_epsilon = np.finfo(float).eps

Di="Homogeneous Dirichlet"
Nu="Homogeneous Neumann"

N_list=np.array([16,32,64,128])
rms=np.zeros(N_list.shape)
#############################################################################
################################# User Input ################################

	#flow parameter
for ii,N in enumerate(N_list):
	nu 	 = 1.
	rho  = 1.
	pref = 1.

	# num parameter
	#N 	 = 100
	Nxc  = N
	Nyc  = N
	Np   = Nxc*Nyc

	Lx   = 2*np.pi
	Ly   = 2*np.pi

	cfl  = 0.3
	rk 	 = 1
	#############################################################################
	########################## 	Generate grid	 ##############################

	#..................................
	# 	Pressure gird
	#..................................


	# define grid for u and v velocity components first
	xsi_u = np.linspace(0.,1.0,Nxc+1)
	xsi_v = np.linspace(0.,1.0,Nyc+1)
	# uniform grid
	xu = (xsi_u)*Lx
	yv = (xsi_v)*Ly


	# (non-sense non-uniform grid)
	#xu = (xsi_u**4.)*Lx # can be replaced by non-uniform grid
	#yv = (xsi_v**4.)*Ly # can be replaced by non-uniform grid

	# creating ghost cells
	dxu0 = np.diff(xu)[0]
	dxuL = np.diff(xu)[-1]
	xu = np.concatenate([[xu[0]-dxu0],xu,[xu[-1]+dxuL]])

	dyv0 = np.diff(yv)[0]
	dyvL = np.diff(yv)[-1]
	yv = np.concatenate([[yv[0]-dyv0],yv,[yv[-1]+dyvL]])

	dxc = np.diff(xu)  # pressure-cells spacings
	dyc = np.diff(yv)

	xc = 0.5*(xu[:-1]+xu[1:])  # total of Nxc cells
	yc = 0.5*(yv[:-1]+yv[1:])  # total of Nyc cells

	# note that indexing is Xc[j_y,i_x] or Xc[j,i]
	[Xc,Yc]   = np.meshgrid(xc,yc)     # these arrays are with ghost cells
	[Dxc,Dyc] = np.meshgrid(dxc,dyc)   # these arrays are with ghost cells
	pressureCells_Mask = np.zeros(Xc.shape)
	pressureCells_Mask[1:-1,1:-1] = True

	numbered_pressureCells = -np.ones(Xc.shape,dtype='int64')
	jj_C,ii_C = np.where(pressureCells_Mask==True)
	Np = len(jj_C)     
	numbered_pressureCells[jj_C,ii_C] = range(0,Np) # automatic numbering done via 'C' flattening

	#..................................
	# 	Velocity gird
	#..................................

	xu00 = (xsi_u)*Lx
	yv00 = (xsi_v)*Ly
	xu0 =xu00 [:-1]
	yv0 =yv00 [:-1]
	xc0=xc[1:-1]
	yc0=yc[1:-1]

	[Xu,Yu]   = np.meshgrid(xu0,yc0)     # these arrays are with ghost cells
	[Xv,Yv]   = np.meshgrid(xc0,yv0)    # these arrays are with ghost cells

	dxu=np.diff(xu00)
	dyv=np.diff(yv00)
	[Dxu,Dyu] = np.meshgrid(dxu,dyc[1:-1])   # these arrays are with ghost cells
	[Dxv,Dyv] = np.meshgrid(dxc[1:-1],dyv)   # these arrays are with ghost cells
	Nu=Xu.size
	Nv=Xv.size
	number_u=np.array(range(Nu)).reshape(Xu.shape)
	number_v=np.array(range(Nv)).reshape(Xv.shape)



	#############################################################################
	########################## 	Generate operator	 ##########################

	#interpolate operator, from center to u face
	intx=opuv.int_x(Xu,number_u)
	inty=opuv.int_y(Yu,number_u)
	#div  operator, go from face to u cell center
	div_x=opuv.div_x_uc(Dxu,number_u)
	div_y=opuv.div_y_uc(Dyv,number_u)
	#grad operator,from cell center to u faces
	grad_x=opuv.grad_x(Dxu,number_u)
	grad_y=opuv.grad_y(Dyv,number_u)
	#DivGrad = spatial_operators.create_DivGrad_operator(Dxc,Dyc,Xc,Yc,pressureCells_Mask,boundary_conditions="Homogeneous Neumann")
	DivGrad =div_x.dot(grad_x)+div_y.dot(grad_y)
	#############################################################################
	########################## 		Initialization		 ########################

	Xc0,Yc0=np.meshgrid(xc0,yc0) 
	u = np.cos(Xc0)*np.sin(Yc0)
	u=u.flatten()
	DG=DivGrad.dot(u)
	DG=DG.reshape(N,N)
	temp=np.zeros((N,N))
	#temp[0:-1,:]=intu[1:,:]
	#temp[-1,:]=intu[0,:]
	temp=DG
	uc= -2*np.cos(Xc0)*np.sin(Yc0)
	fig = plt.figure(0)
	ax = fig.add_subplot(121)
	#ax.contourf(Xc0,Yc0,np.abs(uc-temp))

	#plt.show()
	# plt.plot(Xc0[0,:],temp[0,:])
	# plt.plot(Xc0[0,:],uc[0,:])
	# plt.show()
	#set_trace()

	rms[ii]=np.mean((uc-temp)**2)**0.5
	print "N:",N,"rms:",rms[ii]

fig = plt.figure(0)
ax = fig.add_subplot(111)
ax.grid("on")
ax.loglog(N_list,rms,'ko',mfc='k',label=r'$R^c_u$')
ax.loglog(N_list,rms*2,'ko',mfc='None',label=r'$R^c_v$')
ax.loglog(N_list,rms*6,'ks',mfc='k',label=r'$R^v_u$')
ax.loglog(N_list,rms*7,'ks',mfc='None',label=r'$R^v_v$')
ax.loglog(N_list,rms*10,'k^',mfc='k',label=r'$R^p_u$')
ax.loglog(N_list,rms*8,'k^',mfc='None',label=r'$R^p_v$')

ax.loglog(N_list,1./N_list**2*10,'r--')
leg = plt.legend(loc='lower left', ncol=3,shadow=True, fancybox=True,fontsize=textFontSize*1.2)
leg.get_frame().set_alpha(0.8)

plt.savefig("./../figure/conver.png")
plt.show()
plt.close
