import os
import sys
import numpy as np
import scipy as sp 
import scipy.sparse as scysparse
import scipy.sparse.linalg as splinalg
import pylab as plt
from pdb import set_trace
import scipy.sparse as scysparse

########################################################
#################### BEGIN FUNCTION ####################
########################################################
def Gauss_Seidel_sparse(A,qq,w):
	tol=0.1**5
	n=len(qq)
	x=np.zeros(n)
	# A =D+L+U
	#np.triu()/np.tril()/np.diag()/np.identity(3)
	D =scysparse.diags(A.diagonal()) 
	L = scysparse.tril(A,-1)
	U = scysparse.triu(A,1)
	qq=np.array(qq)*1.0

	#(D+wL)xn+1=[D-w(D+U)]xn+w*q

	x_new  = np.zeros(n)
	r 	   = np.zeros(n)
	norm_r = {}
	r_0	 = A.dot(x_new)-qq
	norm_r[0] = np.sum(r_0**2)**0.5
	ii = 0
	
	#D_inv = np.linalg.inv(D+w*L)
	#splinalg.spsolve(,A_2, permc_spec=None, use_umfpack=True)
	#A_1=D+L
	#A_2=-U
	#B=np.linalg.inv(A_1).dot(A_2)
	#splinalg.spsolve(A_1,A_2, permc_spec=None, use_umfpack=True)
	while ((ii==0) or (norm_r[ii]/norm_r[0]>tol)):
		ii = ii+1
		x  = x_new*1.0
		x_new=splinalg.spsolve(D+w*L,((D-w*(D+U)).dot(x)+w*qq), permc_spec=None, use_umfpack=True)
		r = A.dot(x_new)-qq
		norm_r[ii] = np.sum(r**2)**0.5
		if ii>10000000:
			break
		if (ii%100==0):
			print ii,norm_r[ii]/norm_r[0]
	return x_new,norm_r,ii

def Gauss_Seidel(A,qq,w,nstep):
	tol=0.1**10
	n=len(qq)
	x=np.zeros(n)
	# A =D+L+U
	#np.triu()/np.tril()/np.diag()/np.identity(3)
	D = np.diag(A)*np.identity(n)
	L = np.tril(A)-D
	U = np.triu(A)-D
	qq=np.array(qq)*1.0

	#(D+wL)xn+1=[D-w(D+U)]xn+w*q

	x_new  = np.zeros(n)
	r 	   = np.zeros(n)
	norm_r = {}
	r_0	 = A.dot(x_new)-qq
	norm_r[0] = np.sum(r_0**2)**0.5
	ii = 0
	
	#D_inv = np.linalg.inv(D+w*L)
	#A_1=D+L
	#A_2=-U
	#B=np.linalg.inv(A_1).dot(A_2)
	#splinalg.spsolve(A_1,A_2, permc_spec=None, use_umfpack=True)
	while ((ii==0) or (norm_r[ii]/norm_r[0]>tol)):
		ii = ii+1
		x  = x_new*1.0
		for i in range (n):

			x_new[i]=(1.-w)*x[i]+w/D[i,i]*(qq[i]-L[i,:].dot(x_new)-U[i,:].dot(x)) 

		#x_new = D_inv.dot(((D-w*(D+U)).dot(x)+w*q))

		r = A.dot(x_new)-qq
		norm_r[ii] = np.sum(r**2)**0.5

		if ii>nstep:
			break
		print ii,norm_r[ii]/norm_r[0]
	return x_new,norm_r,ii



def Gauss_matrix(A,q,w,tol,nstep):

	n=len(q)
	x=np.zeros(n)
	# A =D+L+U
	#np.triu()/np.tril()/np.diag()/np.identity(3)
	D = np.diag(A)*np.identity(n)
	L = np.tril(A)-D
	U = np.triu(A)-D

	#(D+wL)xn+1=[D-w(D+U)]xn+w*q

	x_new  = np.zeros(n)
	r 	   = np.zeros(n)
	norm_r = {}
	r_0	 = A.dot(x_new)-q
	norm_r[0] = np.sum(r_0**2)**0.5
	ii = 0
	
	D_inv = np.linalg.inv(D+w*L)
	#A_1=D+L
	#A_2=-U
	#B=np.linalg.inv(A_1).dot(A_2)
	#splinalg.spsolve(A_1,A_2, permc_spec=None, use_umfpack=True)
	while ((ii==0) or (norm_r[ii]/norm_r[0]>tol)):
		ii = ii+1
		x  = x_new*1.0
		x_new = D_inv.dot(((D-w*(D+U)).dot(x)+w*q))

		r = A.dot(x_new)-q
		norm_r[ii] = np.sum(r**2)**0.5

		if ii>nstep:
			break
		print ii,norm_r[ii]/norm_r[0]
	return x_new,norm_r,ii