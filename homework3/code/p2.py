import os
import sys
import numpy as np
import scipy.sparse as scysparse
from pdb import set_trace
from time import sleep
import spatial_operators
import spatial_operators_uv2 as opuv
import scipy.sparse as scysparse
import scipy.sparse.linalg as spysparselinalg  # sparse linear algebra
import scipy.linalg as scylinalg               # non-sparse linear algebra
import pylab as plt
from matplotlib import rc as matplotlibrc
import iteration as it
import time # has the equivalent of tic/toc
import time_adv as tim_adv
import rhs as rrhs
# Figure settings 
matplotlibrc('text.latex', preamble='\usepackage{color}')
matplotlibrc('text',usetex=True)
matplotlibrc('font', family='serif')


figwidth       = 10
figheight      = 6
lineWidth      = 1.5*2
textFontSize   = 28*0.5
gcafontSize    = 30*0.5


machine_epsilon = np.finfo(float).eps

Di="Homogeneous Dirichlet"
Nu="Homogeneous Neumann"

#############################################################################
################################# User Input ################################

#flow parameter

nu 	 = 1.
rho  = 1.
pref = 0.

# num parameter
N 	 = 64
Nxc  = N
Nyc  = N
Np   = Nxc*Nyc

Lx   = 2*np.pi
Ly   = 2*np.pi

cfl  = 0.1
rk 	 = 1
#############################################################################
########################## 	Generate grid	 ##############################

#..................................
# 	Pressure gird
#..................................


# define grid for u and v velocity components first
xsi_u = np.linspace(0.,1.0,Nxc+1)
xsi_v = np.linspace(0.,1.0,Nyc+1)
# uniform grid
xu = (xsi_u)*Lx
yv = (xsi_v)*Ly


# (non-sense non-uniform grid)
#xu = (xsi_u**4.)*Lx # can be replaced by non-uniform grid
#yv = (xsi_v**4.)*Ly # can be replaced by non-uniform grid

# creating ghost cells
dxu0 = np.diff(xu)[0]
dxuL = np.diff(xu)[-1]
xu = np.concatenate([[xu[0]-dxu0],xu,[xu[-1]+dxuL]])

dyv0 = np.diff(yv)[0]
dyvL = np.diff(yv)[-1]
yv = np.concatenate([[yv[0]-dyv0],yv,[yv[-1]+dyvL]])

dxc = np.diff(xu)  # pressure-cells spacings
dyc = np.diff(yv)

xc = 0.5*(xu[:-1]+xu[1:])  # total of Nxc cells
yc = 0.5*(yv[:-1]+yv[1:])  # total of Nyc cells

# note that indexing is Xc[j_y,i_x] or Xc[j,i]
[Xc,Yc]   = np.meshgrid(xc,yc)     # these arrays are with ghost cells
[Dxc,Dyc] = np.meshgrid(dxc,dyc)   # these arrays are with ghost cells
pressureCells_Mask = np.zeros(Xc.shape)
pressureCells_Mask[1:-1,1:-1] = True

numbered_pressureCells = -np.ones(Xc.shape,dtype='int64')
jj_C,ii_C = np.where(pressureCells_Mask==True)
Np = len(jj_C)     
numbered_pressureCells[jj_C,ii_C] = range(0,Np) # automatic numbering done via 'C' flattening

#..................................
# 	Velocity gird
#..................................

xu00 = (xsi_u)*Lx
yv00 = (xsi_v)*Ly
xu0 =xu00 [:-1]
yv0 =yv00 [:-1]
xc0=xc[1:-1]
yc0=yc[1:-1]

[Xu,Yu]   = np.meshgrid(xu0,yc0)     # these arrays are with ghost cells
[Xv,Yv]   = np.meshgrid(xc0,yv0)    # these arrays are with ghost cells

dxu=np.diff(xu00)
dyv=np.diff(yv00)
[Dxu,Dyu] = np.meshgrid(dxu,dyc[1:-1])   # these arrays are with ghost cells
[Dxv,Dyv] = np.meshgrid(dxc[1:-1],dyv)   # these arrays are with ghost cells
Nu=Xu.size
Nv=Xv.size
number_u=np.array(range(Nu)).reshape(Xu.shape)
number_v=np.array(range(Nv)).reshape(Xv.shape)


[Xc0,Yc0]   = np.meshgrid(xc0,yc0) 
#############################################################################
########################## 	Generate operator	 ##########################

#interpolate operator, from center to u face
intx=opuv.int_x(Xu,number_u)
inty=opuv.int_y(Yu,number_u)
#div  operator, go from face to u cell center
div_x=opuv.div_x_uc(Dxu,number_u)
div_y=opuv.div_y_uc(Dyv,number_u)
#grad operator,from cell center to u faces
grad_x=opuv.grad_x(Dxu,number_u)
grad_y=opuv.grad_y(Dyv,number_u)
#DivGrad = spatial_operators.create_DivGrad_operator(Dxc,Dyc,Xc,Yc,pressureCells_Mask,boundary_conditions="Homogeneous Neumann")
DivGrad =div_x.dot(grad_x)+div_y.dot(grad_y)
#############################################################################
########################## 		Initialization		 ########################

u = -np.cos(Xu)*np.sin(Yu)
v =  np.sin(Xv)*np.cos(Yv)
u0 = -np.cos(Xu)*np.sin(Yu)
v0 =  np.sin(Xv)*np.cos(Yv)

# u=Xu
# v=np.ones(Xu.shape)
p = -0.25*(np.cos(2*Xc0)+np.cos(2*Yc0))+0.5

umag=(u**2+v**2)**0.5

dx=np.mean(Dxu)
dt_v=np.min(cfl*dx/umag)
dt_c=np.min(cfl*dx*dx/nu)
dt=np.min([dt_c,dt_v])
print "initialized with dt = ",dt

dt = 0.001
u=u.flatten()
v=v.flatten()

# Xt=Xc[1:-1,1:-1]
# Yt=Yc[1:-1,1:-1]
# ut = -np.cos(Xt)*np.sin(Yt)
# vt =  np.sin(Xt)*np.cos(Yt)
# ut=ut.flatten()
# vt=vt.flatten()

#############################################################################
########################## 		Time advance		 ########################

t=0
it=0
\
it_list=np.array([0,100,200,500,1000])
frame_iter = 0
while((t<=1.001)):
	#print np.max(u)

	# figure
	if  (it in it_list):
	#if (np.abs(t-0.9)<=dt):

		print "................................."
		print "running with rk: ",rk
		print "time: ",t, "iter: ",it,
		tim_adv.report_cfl(u,v,dt,dx,nu)

		fig = plt.figure(0, figsize=(figheight*2.1,figheight*0.6))
		ax = fig.add_subplot(131,aspect='equal')
		plt.axes(ax)#matrix_rank
		c=plt.contourf(Xu,Yu,u.reshape(Xu.shape))
		d=plt.colorbar()
		c.set_clim(-1., 1.)
		d.set_clim(-1., 1.)		
		c.ax.tick_params(labelsize=textFontSize)		
		d.ax.tick_params(labelsize=textFontSize)
		plt.title("u",fontsize=textFontSize)
		plt.setp(ax.get_xticklabels(),fontsize=gcafontSize)
		plt.setp(ax.get_yticklabels(),fontsize=gcafontSize)
		time="time="+str(np.round(t,3))
		ax.text(2.2,5,time,fontsize=textFontSize*1.5,color='w')

		ax = fig.add_subplot(132,aspect='equal')
		plt.axes(ax)#matrix_rank
		c=plt.contourf(Xv,Yv,v.reshape(Xu.shape))
		d=plt.colorbar()
		c.set_clim(-1., 1.)
		d.set_clim(-1., 1.)		
		c.ax.tick_params(labelsize=textFontSize)		
		d.ax.tick_params(labelsize=textFontSize)
		plt.title("v",fontsize=textFontSize)
		plt.setp(ax.get_xticklabels(),fontsize=gcafontSize)
		plt.setp(ax.get_yticklabels(),fontsize=gcafontSize)

		ax = fig.add_subplot(133,aspect='equal')
		plt.axes(ax)#matrix_rank
		c=plt.contourf(Xc0,Yc0,p.reshape(Xu.shape))
		d=plt.colorbar()
		c.ax.tick_params(labelsize=textFontSize)		
		d.ax.tick_params(labelsize=textFontSize)
		#c.set_clim(-1., 1.)
		plt.title("p",fontsize=textFontSize)
		plt.setp(ax.get_xticklabels(),fontsize=gcafontSize)
		plt.setp(ax.get_yticklabels(),fontsize=gcafontSize)
		c.set_clim(0., 1.0)

		fig.tight_layout()
		framename = './../figure/taylor'+str(it)+".png"

		print framename," saved!"
		plt.savefig(framename)
		plt.close()

	# time advancement
	t=t+dt
	it=it+1


	u_new,v_new,p=tim_adv.get_unew(DivGrad,intx,inty,div_x,div_y,grad_x,grad_y,u,v,rk,dt,pref,nu,rho)
	u,v=u_new,v_new




