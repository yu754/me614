import os
import sys
import numpy as np
import scipy.sparse as scysparse
from pdb import set_trace
from time import sleep
import spatial_operators
import spatial_operators_uv2 as opuv
import scipy.sparse as scysparse
import scipy.sparse.linalg as spysparselinalg  # sparse linear algebra
import scipy.linalg as scylinalg               # non-sparse linear algebra
import pylab as plt
from matplotlib import rc as matplotlibrc
import iteration as it
import time # has the equivalent of tic/toc
import time_adv as tim_adv
import rhs as rrhs
# Figure settings 
matplotlibrc('text.latex', preamble='\usepackage{color}')
matplotlibrc('text',usetex=True)
matplotlibrc('font', family='serif')


figwidth       = 10
figheight      = 6
lineWidth      = 1.5*2
textFontSize   = 28*0.5
gcafontSize    = 30*0.5


machine_epsilon = np.finfo(float).eps

Di="Homogeneous Dirichlet"
Nu="Homogeneous Neumann"

#############################################################################
################################# User Input ################################

#flow parameter

nu 	 = 1.
rho  = 1.
pref = 0.
Lx   = 2*np.pi
Ly   = 2*np.pi

cfl  = 0.1
rk 	 = 2


N_list=np.array([32,64])

rms_u=np.zeros(len(N_list))
rms_v=np.zeros(len(N_list))
rms_p=np.zeros(len(N_list))

for ii,N in enumerate(N_list):
	# num parameter
	#N 	 = 64
	Nxc  = N
	Nyc  = N
	Np   = Nxc*Nyc

	#############################################################################
	########################## 	Generate grid	 ##############################

	#..................................
	# 	Pressure gird
	#..................................


	# define grid for u and v velocity components first
	xsi_u = np.linspace(0.,1.0,Nxc+1)
	xsi_v = np.linspace(0.,1.0,Nyc+1)
	# uniform grid
	xu = (xsi_u)*Lx
	yv = (xsi_v)*Ly


	# (non-sense non-uniform grid)
	#xu = (xsi_u**4.)*Lx # can be replaced by non-uniform grid
	#yv = (xsi_v**4.)*Ly # can be replaced by non-uniform grid

	# creating ghost cells
	dxu0 = np.diff(xu)[0]
	dxuL = np.diff(xu)[-1]
	xu = np.concatenate([[xu[0]-dxu0],xu,[xu[-1]+dxuL]])

	dyv0 = np.diff(yv)[0]
	dyvL = np.diff(yv)[-1]
	yv = np.concatenate([[yv[0]-dyv0],yv,[yv[-1]+dyvL]])

	dxc = np.diff(xu)  # pressure-cells spacings
	dyc = np.diff(yv)

	xc = 0.5*(xu[:-1]+xu[1:])  # total of Nxc cells
	yc = 0.5*(yv[:-1]+yv[1:])  # total of Nyc cells

	# note that indexing is Xc[j_y,i_x] or Xc[j,i]
	[Xc,Yc]   = np.meshgrid(xc,yc)     # these arrays are with ghost cells
	[Dxc,Dyc] = np.meshgrid(dxc,dyc)   # these arrays are with ghost cells
	pressureCells_Mask = np.zeros(Xc.shape)
	pressureCells_Mask[1:-1,1:-1] = True

	numbered_pressureCells = -np.ones(Xc.shape,dtype='int64')
	jj_C,ii_C = np.where(pressureCells_Mask==True)
	Np = len(jj_C)     
	numbered_pressureCells[jj_C,ii_C] = range(0,Np) # automatic numbering done via 'C' flattening

	#..................................
	# 	Velocity gird
	#..................................

	xu00 = (xsi_u)*Lx
	yv00 = (xsi_v)*Ly
	xu0 =xu00 [:-1]
	yv0 =yv00 [:-1]
	xc0=xc[1:-1]
	yc0=yc[1:-1]

	[Xu,Yu]   = np.meshgrid(xu0,yc0)     # these arrays are with ghost cells
	[Xv,Yv]   = np.meshgrid(xc0,yv0)    # these arrays are with ghost cells

	dxu=np.diff(xu00)
	dyv=np.diff(yv00)
	[Dxu,Dyu] = np.meshgrid(dxu,dyc[1:-1])   # these arrays are with ghost cells
	[Dxv,Dyv] = np.meshgrid(dxc[1:-1],dyv)   # these arrays are with ghost cells
	Nu=Xu.size
	Nv=Xv.size
	number_u=np.array(range(Nu)).reshape(Xu.shape)
	number_v=np.array(range(Nv)).reshape(Xv.shape)


	[Xc0,Yc0]   = np.meshgrid(xc0,yc0) 
	#############################################################################
	########################## 	Generate operator	 ##########################

	#interpolate operator, from center to u face
	intx=opuv.int_x(Xu,number_u)
	inty=opuv.int_y(Yu,number_u)
	#div  operator, go from face to u cell center
	div_x=opuv.div_x_uc(Dxu,number_u)
	div_y=opuv.div_y_uc(Dyv,number_u)
	#grad operator,from cell center to u faces
	grad_x=opuv.grad_x(Dxu,number_u)
	grad_y=opuv.grad_y(Dyv,number_u)
	#DivGrad = spatial_operators.create_DivGrad_operator(Dxc,Dyc,Xc,Yc,pressureCells_Mask,boundary_conditions="Homogeneous Neumann")
	DivGrad =div_x.dot(grad_x)+div_y.dot(grad_y)
	#############################################################################
	########################## 		Initialization		 ########################

	u = -np.cos(Xu)*np.sin(Yu)
	v =  np.sin(Xv)*np.cos(Yv)
	u0 = -np.cos(Xu)*np.sin(Yu)
	v0 =  np.sin(Xv)*np.cos(Yv)
	p0 = -0.25*(np.cos(2*Xc0)+np.cos(2*Yc0))-(-0.25*(np.cos(2*Xc0[0,1])+np.cos(2*Yc0[0,1])))
	# u=Xu
	# v=np.ones(Xu.shape)
	p = -0.25*(np.cos(2*Xc0)+np.cos(2*Yc0))+0.5

	umag=(u**2+v**2)**0.5


	dt = 0.0001	
	print "initialized with dt = ",dt


	u=u.flatten()
	v=v.flatten()

	# Xt=Xc[1:-1,1:-1]
	# Yt=Yc[1:-1,1:-1]
	# ut = -np.cos(Xt)*np.sin(Yt)
	# vt =  np.sin(Xt)*np.cos(Yt)
	# ut=ut.flatten()
	# vt=vt.flatten()

	#############################################################################
	########################## 		Time advance		 ########################

	t=0
	it=0

	frame_iter = 0
	while((t<=1.001)):
		#print np.max(u)
		if (np.abs(t-1.)<=dt):
			u_ana=u0*np.exp(-2.*t)
			v_ana=v0*np.exp(-2.*t)
			p_ana=p0*np.exp(-4.*t)
			p_ana=p_ana-p_ana[0,1]
			rms_u[ii]=np.mean((u_ana.flatten()-u)**2)**0.5
			rms_v[ii]=np.mean((v_ana.flatten()-v)**2)**0.5
			rms_p[ii]=np.mean((p_ana.flatten()-p)**2)**0.5	
	
		# time advancement
		t=t+dt
		it=it+1

		u_new,v_new,p=tim_adv.get_unew(DivGrad,intx,inty,div_x,div_y,grad_x,grad_y,u,v,rk,dt,pref,nu,rho)
		u,v=u_new,v_new
	print "finish N=",N,rms_u[ii],rms_v[ii],rms_p[ii]





if True:
	fname="rms_rk"+str(rk)+".dat"	
	fpath='./../data/'
	fout = open(fpath+fname, 'w')
	for k in range(len(N_list)):
	     fout.write(str(N_list[k])+" "+str(rms_u[k])+" "+str(rms_v[k])+" "+str(rms_p[k])+"\n")
	fout.close()	
# consider pre-factorization with LU decomposition
# will speed up Poisson solver by x10

if False:
	# pouring flattened solution back in 2D array
	Phi = np.zeros((Nyc+2,Nxc+2))*np.nan # remember ghost cells
	#set_trace()	
	Phi[np.where(pressureCells_Mask==True)] =0# phi

	fig = plt.figure(0, figsize=(figwidth,figheight))
	ax   = fig.add_axes([0.15,0.15,0.8,0.8])
	plt.axes(ax)#matrix_rank
	plt.contourf(Xc,Yc,Phi)
	plt.colorbar()
	# plt.plot(Phi[3,1:-2])
	ax.grid('on',which='both')
	plt.setp(ax.get_xticklabels(),fontsize=gcafontSize)
	plt.setp(ax.get_yticklabels(),fontsize=gcafontSize)
	ax.set_xlabel(r"$x$",fontsize=textFontSize)
	ax.set_ylabel(r"$y$",fontsize=textFontSize,rotation=90)
	plt.axis("tight")
	#plt.axis("equal")

	plt.show()
