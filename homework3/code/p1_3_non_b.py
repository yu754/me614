import os
import sys
import numpy as np
import scipy.sparse as scysparse
from pdb import set_trace
from time import sleep
import spatial_operators
import scipy.sparse as scysparse
import scipy.sparse.linalg as spysparselinalg  # sparse linear algebra
import scipy.linalg as scylinalg               # non-sparse linear algebra
import pylab as plt
from matplotlib import rc as matplotlibrc
import iteration as it
import time # has the equivalent of tic/toc

# Figure settings 
matplotlibrc('text.latex', preamble='\usepackage{color}')
matplotlibrc('text',usetex=True)
matplotlibrc('font', family='serif')


figwidth       = 10
figheight      = 6
lineWidth      = 1.5*2
textFontSize   = 28*0.5
gcafontSize    = 30*0.5


machine_epsilon = np.finfo(float).eps

Di="Homogeneous Dirichlet"
Nu="Homogeneous Neumann"
BC=Di
Re=13.
n=2.
N=192
#########################################
############### User Input ##############

# number of (pressure) cells = mass conservation cells
Nxc  = N
Nyc  = N
Np   = Nxc*Nyc
Lx   = 1.
Ly   = 1.

#########################################
######## Preprocessing Stage ############

# You might have to include ghost-cells here
# Depending on your application

# define grid for u and v velocity components first
xsi_u = np.linspace(0.,1.0,Nxc+1)
xsi_v = np.linspace(0.,1.0,Nyc+1)
# uniform grid
xu = (xsi_u)*Lx
yv = (xsi_v)*Ly
# (non-sense non-uniform grid)
#xu = (xsi_u**4.)*Lx # can be replaced by non-uniform grid
#yv = (xsi_v**4.)*Ly # can be replaced by non-uniform grid

# creating ghost cells
dxu0 = np.diff(xu)[0]
dxuL = np.diff(xu)[-1]
xu = np.concatenate([[xu[0]-dxu0],xu,[xu[-1]+dxuL]])

dyv0 = np.diff(yv)[0]
dyvL = np.diff(yv)[-1]
yv = np.concatenate([[yv[0]-dyv0],yv,[yv[-1]+dyvL]])

dxc = np.diff(xu)  # pressure-cells spacings
dyc = np.diff(yv)

xc = 0.5*(xu[:-1]+xu[1:])  # total of Nxc cells
yc = 0.5*(yv[:-1]+yv[1:])  # total of Nyc cells

# note that indexing is Xc[j_y,i_x] or Xc[j,i]
[Xc,Yc]   = np.meshgrid(xc,yc)     # these arrays are with ghost cells
[Dxc,Dyc] = np.meshgrid(dxc,dyc)   # these arrays are with ghost cells


pressureCells_Mask = np.zeros(Xc.shape)
pressureCells_Mask[1:-1,1:-1] = True


xc0=xc[1:-1]
yc0=yc[1:-1]
[Xc0,Yc0]   = np.meshgrid(xc0,yc0)

# number of actual pressure cells
Np = len(np.where(pressureCells_Mask==True)[0])
q_ana=np.sin(2*np.pi*n*Xc0)*np.sin(2*np.pi*n*Yc0)

f=2*np.pi*n*np.cos(2*np.pi*n*Xc)*np.sin(2*np.pi*n*Yc)*np.sin(2*np.pi*n*Xc)*np.sin(2*np.pi*n*Yc)\
	-1./Re*(-8.*np.pi*np.pi*n*n*np.sin(2*np.pi*n*Xc)*np.sin(2*np.pi*n*Yc))



f=f[np.where(pressureCells_Mask==True)]

DivGrad = spatial_operators.create_DivGrad_operator(Dxc,Dyc,Xc,Yc,pressureCells_Mask,boundary_conditions=BC)
Grad  = spatial_operators.Grad_px_operator(Dxc,Dyc,Xc,Yc,pressureCells_Mask,boundary_conditions=Di)
Grad_x=scysparse.csr_matrix((Np,Np),dtype="float64")
Grad_x=1.0*Grad

k=0
rms=100
phi=np.zeros(q_ana.size)#+0.01
#phi[2]=1.
eps=-1./Re
t1=time.time()	
while ((rms>0.00001)&(k<100)):
	phi_o=phi*1.0

	rhs=1./Re*DivGrad.dot(phi_o)+f+eps*DivGrad.dot(phi_o)

	phi_sub=phi_o*1.0
	l=0
	rms2=10

	while ((rms2>0.0001)&(l<200)):
		phi_sub_o=phi_sub*1.0
		Grad_x=scysparse.diags(phi_sub_o).dot(Grad)
		AA=eps*DivGrad+Grad_x			
		phi_sub=spysparselinalg.spsolve(AA,rhs)
		rms2=np.mean((phi_sub_o-phi_sub)**2)**0.5
		l=l+1
		print "inner,",l,rms2

	phi=1.0*phi_sub
	rms=np.mean((phi-phi_o)**2)**0.5
	k=k+1
	print "outer,",k, rms

t2=time.time()	

final_rms=np.mean((phi-q_ana.flatten())**2)**0.5
print str(2),Re,k,t2-t1,final_rms




if False:
	fname="p2opt_N_"+str(N)+"_n_"+str(n) +".dat"	
	fpath='./../data/opt/'
	fout = open(fpath+fname, 'w')
	for k in range(len(w_list)):
	     fout.write(str(w_list[k])+" "+str(kk[k])+"\n")
	fout.close()	
# consider pre-factorization with LU decomposition
# will speed up Poisson solver by x10

if False:
	# pouring flattened solution back in 2D array
	Phi = np.zeros((Nyc+2,Nxc+2))*np.nan # remember ghost cells
	#set_trace()
	Phi[np.where(pressureCells_Mask==True)] = phi

	fig = plt.figure(0, figsize=(figwidth,figheight))
	ax   = fig.add_axes([0.15,0.15,0.8,0.8])
	plt.axes(ax)#matrix_rank
	plt.contourf(Xc,Yc,Phi)
	plt.colorbar()
	# plt.plot(Phi[3,1:-2])
	ax.grid('on',which='both')
	plt.setp(ax.get_xticklabels(),fontsize=gcafontSize)
	plt.setp(ax.get_yticklabels(),fontsize=gcafontSize)
	ax.set_xlabel(r"$x$",fontsize=textFontSize)
	ax.set_ylabel(r"$y$",fontsize=textFontSize,rotation=90)
	plt.axis("tight")
	#plt.axis("equal")

	plt.show()
