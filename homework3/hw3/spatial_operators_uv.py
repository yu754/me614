import numpy as np
import scipy.sparse as scysparse
import sys
from pdb import set_trace 
import scipy.sparse as scysparse
import scipy.sparse.linalg as spysparselinalg
import scipy.linalg as scylinalg        # non-sparse linear algebra
import pylab as plt
############################################################
############################################################

#u[j,i]
# get mean value (ui+ui-1)/2 at left face of u cell
# first result is the left face of u cell(outside)
def int_x_ul(Xu,number_u):
	Nu=Xu.size
	Nx=Xu.shape[1]

	A = scysparse.lil_matrix((Nu,Nu),dtype="float64")

	#B_E = number_u[:,-1]
	#B_W = number_u[:,0]
	mask=number_u*1.0
	mask[:,0]=-2
	fla=mask.flatten()

	ii=np.array(np.where(fla>=0))

	A[ii,ii]=0.5
	A[ii,ii-1]=0.5

	ii=np.array(np.where(fla<0))
	A[ii,ii]=0.5
	A[ii,ii-1+Nx]=0.5
	return A

#v[j,i]
# get mean value (vi+vi-1)/2 at bottom face of u cell
# first result is the bottom face of u cell( p grid conner)

def int_x_ub(Xv,number_v):
	Nv=Xv.size
	Nx=Xv.shape[1]

	A = scysparse.lil_matrix((Nv,Nv),dtype="float64")

	#B_E = number_u[:,-1]
	#B_W = number_u[:,0]
	mask=number_v*1.0
	mask[:,0]=-2
	fla=mask.flatten()

	ii=np.array(np.where(fla>=0))

	A[ii,ii]=0.5
	A[ii,ii-1]=0.5

	ii=np.array(np.where(fla<0))
	A[ii,ii]=0.5
	A[ii,ii-1+Nx]=0.5
	#set_trace()	
	return A

#u[j,i]
# get mean value (ui+ui-1)/2 at bottom face of u cell
# first result is the bottom face of u cell( p grid conner)

def int_y_ub(Yv,number_u):
	Nu=number_u.size
	Ny=number_u.shape[0]
	Nx=number_u.shape[1]
	A = scysparse.lil_matrix((Nu,Nu),dtype="float64")

	mask=number_u*1.0
	mask[0,:]=-2
	fla=mask.flatten()

	ii=np.array(np.where(fla>=0))
	xx=Yv.flatten()
	A[ii,ii]=0.5
	A[ii,ii-Nx]=0.5

	ii=np.array(np.where(fla<0))
	A[ii,ii]=0.5
	A[ii,ii+(Ny-1)*Nx]=0.5

	return A	

#u[j,i]
# repare div operator,from u l/r face to u center
# first result is the first u point (at left boundary)
def div_x_uc(uu_x,Xu,number_u):
	Nu=Xu.size
	Nx=Xu.shape[1]

	A = scysparse.lil_matrix((Nu,Nu),dtype="float64")

	#B_E = number_u[:,-1]
	#B_W = number_u[:,0]
	mask=number_u*1.0
	mask[:,-1]=-2
	fla=mask.flatten()

	ii=np.array(np.where(fla>=0))
	xx=Xu.flatten()
	A[ii,ii]=-1./xx[ii]
	A[ii,ii+1]=1./xx[ii]

	ii=np.array(np.where(fla<0))
	A[ii,ii]=-1./xx[ii]
	A[ii,ii+1-Nx]=1./xx[ii]

	return A	

#u[j,i]
# prepare div operator,from u b/u face to u center
# first result is the first u point (at left boundary)
def div_y_uc(uv_x,X,number_u):
	Nu=X.size
	Nx=X.shape[1]
	Ny=X.shape[0]
	A = scysparse.lil_matrix((Nu,Nu),dtype="float64")

	#B_E = number_u[:,-1]
	#B_W = number_u[:,0]
	mask=number_u*1.0
	mask[-1,:]=-2
	fla=mask.flatten()

	ii=np.array(np.where(fla>=0))
	xx=X.flatten()
	A[ii,ii]=-1./xx[ii]
	A[ii,ii+Ny]=1./xx[ii]

	ii=np.array(np.where(fla<0))
	A[ii,ii]=-1./xx[ii]
	A[ii,ii-Nx*(Ny-1)]=1./xx[ii]
	return A	

# get dudx at left face of u cell
# first result is the left face of u cell(outside)
def grad_x_ul(u,Xu,number_u):
	Nu=Xu.size
	Nx=Xu.shape[1]

	A = scysparse.lil_matrix((Nu,Nu),dtype="float64")

	#B_E = number_u[:,-1]
	#B_W = number_u[:,0]
	mask=number_u*1.0
	mask[:,0]=-2
	fla=mask.flatten()

	ii=np.array(np.where(fla>=0))
	xx=Xu.flatten()
	A[ii,ii]=1./xx[ii]
	A[ii,ii-1]=-1./xx[ii]

	ii=np.array(np.where(fla<0))
	A[ii,ii]=1./xx[ii]
	A[ii,ii-1+Nx]=-1./xx[ii]

	return A		

# get dudy at bottom face of u cell
# first result is the bottom face of u cell( p grid conner)
def grad_y_ub(u,Dyu,number_u):


	Nu=Dyu.size
	Ny=Dyu.shape[0]
	Nx=Dyu.shape[1]

	A = scysparse.lil_matrix((Nu,Nu),dtype="float64")

	mask=number_u*1.0
	mask[0,:]=-2
	fla=mask.flatten()

	ii=np.array(np.where(fla>=0))
	yy=Dyu.flatten()
	A[ii,ii]=1./yy[ii]
	A[ii,ii-Nx]=-1./yy[ii]

	ii=np.array(np.where(fla<0))
	A[ii,ii]=1./yy[ii]

	A[ii,ii+(Ny-1)*Nx]=-1./yy[ii]
	#set_trace()
	return A	

# get dvdx at bottom face of u cell
# first result is the bottom face of u cell( p grid conner)
def grad_x_ub(u,Dyu,number_u):
	Nv=Dyu.size
	Nx=Dyu.shape[1]

	A = scysparse.lil_matrix((Nv,Nv),dtype="float64")

	mask=number_u*1.0
	mask[:,0]=-2
	fla=mask.flatten()

	ii=np.array(np.where(fla>=0))
	xx=Dyu.flatten()
	A[ii,ii]=1./xx[ii]
	A[ii,ii-1]=-1./xx[ii]

	ii=np.array(np.where(fla<0))
	A[ii,ii]=1./xx[ii]
	A[ii,ii-1+Nx]=-1./xx[ii]
	return A			