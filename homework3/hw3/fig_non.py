import os
import sys
import numpy as np
import scipy.sparse as scysparse
from time import sleep
import scipy.sparse as scysparse
import scipy.sparse.linalg as spysparselinalg  # sparse linear algebra
import scipy.linalg as scylinalg               # non-sparse linear algebra
import pylab as plt
from matplotlib import rc as matplotlibrc
from pdb import set_trace
# Figure settings
matplotlibrc('text.latex', preamble='\usepackage{color}')
matplotlibrc('text',usetex=True)
matplotlibrc('font', family='serif')

fig_sol =0
fig_spy =1
figwidth       = 8
figheight      = 6
lineWidth      = 1.5
textFontSize   = 28*0.65
gcafontSize    = 30*0.65

fname="non_comp.dat"
fpath='./../data/non/'
data=np.loadtxt(fpath+fname)

kk=data[:,2]
tt=data[:,3]
rr=data[:,4]
re=np.array([0.01,1.0,10])

set_trace()
fig = plt.figure(0, figsize=(figwidth*1.8,figheight*0.7))

ax = fig.add_subplot(131)

ax.plot(re,kk[0:3],'k-',marker='s',mfc='None',label='method (a)')
ax.plot(re,kk[3:6],'k-',marker='^',mfc='None',label='method (b)')
ax.plot(re,kk[6:9],'k-',marker='o',mfc='None',label='method (b)+LU')

ax.set_xscale('log')
m=1.2
ax.set_xlabel(r"$Re$",fontsize=textFontSize*m)
ax.set_ylabel(r"iteration:k",fontsize=textFontSize*m,rotation=90)
plt.setp(ax.get_xticklabels(),fontsize=gcafontSize*m)
plt.setp(ax.get_yticklabels(),fontsize=gcafontSize*m)
leg = plt.legend(loc='upper left', ncol=1,shadow=True, fancybox=True,fontsize=textFontSize*0.8)
leg.get_frame().set_alpha(0.8)


ax = fig.add_subplot(132)
ax.plot(re,tt[0:3],'k-',marker='s',mfc='None',label='Re 0.01')
ax.plot(re,tt[3:6],'k-',marker='^',mfc='None',label='Re 1.0')
ax.plot(re,tt[6:9],'k-',marker='o',mfc='None',label='Re 10.0')
ax.set_xscale('log')
ax.set_xlabel(r"$Re$",fontsize=textFontSize*m)
ax.set_ylabel(r"$time$",fontsize=textFontSize*m,rotation=90)
plt.setp(ax.get_xticklabels(),fontsize=gcafontSize*m)
plt.setp(ax.get_yticklabels(),fontsize=gcafontSize*m)


ax = fig.add_subplot(133)
ax.plot(re,rr[0:3],'k-',marker='s',mfc='None',label='Re 0.01')
ax.plot(re,rr[3:6],'k-',marker='^',mfc='None',label='Re 1.0')
ax.plot(re,rr[6:9],'k-',marker='o',mfc='None',label='Re 10.0')
ax.set_xscale('log')
ax.set_xlabel(r'$Re$',fontsize=textFontSize*m)
ax.set_ylabel(r"$rms$",fontsize=textFontSize*m,rotation=90)
plt.setp(ax.get_xticklabels(),fontsize=gcafontSize*m)
plt.setp(ax.get_yticklabels(),fontsize=gcafontSize*m)

#ax.set_yscale('log')
# ax.loglog(dt_list,tuw1,'o-',label='1st-upwind')
# ax.loglog(dt_list,tuw2,'o-',label='2nd-upwind')
# ax.loglog(dt_list,tcd2,'o-',label='2nd-cds')

# ax.loglog(dt_list,dt_list**(-2.)/1000000000000,'k--',label='2nd-ref',lw=lineWidth)
# m=1.2
# ax.set_xlabel(r"$1/dt$",fontsize=textFontSize*m)
# ax.set_ylabel(r"$RMS$",fontsize=textFontSize*m,rotation=90)
# plt.setp(ax.get_xticklabels(),fontsize=gcafontSize*m)
# plt.setp(ax.get_yticklabels(),fontsize=gcafontSize*m)
# leg = plt.legend(loc='best', ncol=1,  shadow=False, fancybox=True,fontsize=textFontSize)
# leg.get_frame().set_alpha(0.8)



fig.tight_layout()
#fig_name = 'p3_diff.png'


fig_name = 'p1_non_.png'
figure_path = './../figure/'
fig_fullpath = figure_path + fig_name
plt.savefig(fig_fullpath)
plt.show()
plt.close()