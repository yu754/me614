import os
import sys
import numpy as np
import scipy.sparse as scysparse
from pdb import set_trace
from time import sleep
import spatial_operators
import scipy.sparse as scysparse
import scipy.sparse.linalg as spysparselinalg  # sparse linear algebra
import scipy.linalg as scylinalg               # non-sparse linear algebra
import pylab as plt
from matplotlib import rc as matplotlibrc
import time # has the equivalent of tic/toc

# Figure settings 
matplotlibrc('text.latex', preamble='\usepackage{color}')
matplotlibrc('text',usetex=True)
matplotlibrc('font', family='serif')


figwidth       = 10
figheight      = 6
lineWidth      = 1.5*2
textFontSize   = 28*0.5
gcafontSize    = 30*0.5


machine_epsilon = np.finfo(float).eps

#########################################
############### User Input ##############

# number of (pressure) cells = mass conservation cells
Nxc  = 5
Nyc  = 5
Np   = Nxc*Nyc
Lx   = 2.*np.pi
Ly   = 2.*np.pi

#########################################
######## Preprocessing Stage ############

# You might have to include ghost-cells here
# Depending on your application

# define grid for u and v velocity components first
xsi_u = np.linspace(0.,1.0,Nxc+1)
xsi_v = np.linspace(0.,1.0,Nyc+1)
# uniform grid
xu = (xsi_u)*Lx
yv = (xsi_v)*Ly
# (non-sense non-uniform grid)
#xu = (xsi_u**4.)*Lx # can be replaced by non-uniform grid
#yv = (xsi_v**4.)*Ly # can be replaced by non-uniform grid

# creating ghost cells
dxu0 = np.diff(xu)[0]
dxuL = np.diff(xu)[-1]
xu = np.concatenate([[xu[0]-dxu0],xu,[xu[-1]+dxuL]])

dyv0 = np.diff(yv)[0]
dyvL = np.diff(yv)[-1]
yv = np.concatenate([[yv[0]-dyv0],yv,[yv[-1]+dyvL]])

dxc = np.diff(xu)  # pressure-cells spacings
dyc = np.diff(yv)

xc = 0.5*(xu[:-1]+xu[1:])  # total of Nxc cells
yc = 0.5*(yv[:-1]+yv[1:])  # total of Nyc cells

# note that indexing is Xc[j_y,i_x] or Xc[j,i]
[Xc,Yc]   = np.meshgrid(xc,yc)     # these arrays are with ghost cells
[Dxc,Dyc] = np.meshgrid(dxc,dyc)   # these arrays are with ghost cells

### familiarize yourself with 'flattening' options
# phi_Fordering     = Phi.flatten(order='F') # F (column-major), Fortran/Matlab default
# phi_Cordering     = Phi.flatten(order='C') # C (row-major), C/Python default
# phi_PythonDefault = Phi.flatten()          # Python default
# compare Phi[:,0] and Phi[0,:] with phi[:Nxc]

# Pre-allocated at all False = no fluid points
pressureCells_Mask = np.zeros(Xc.shape)
pressureCells_Mask[1:-1,1:-1] = True

# Introducing obstacle in pressure Mask
obstacle_radius = 0.3*Lx # set equal to 0.0*Lx to remove obstacle
distance_from_center = np.sqrt(np.power(Xc-Lx/2.,2.0)+np.power(Yc-Ly/2.,2.0))
j_obstacle,i_obstacle = np.where(distance_from_center<obstacle_radius)
pressureCells_Mask[j_obstacle,i_obstacle] = True

# number of actual pressure cells
Np = len(np.where(pressureCells_Mask==True)[0])
q  = np.ones(Np,)

Di="Homogeneous Dirichlet"
Nu="Homogeneous Neumann"
BC=Nu
DivGrad = spatial_operators.create_DivGrad_operator(Dxc,Dyc,Xc,Yc,pressureCells_Mask,boundary_conditions=BC)
# if boundary_conditions are not specified, it defaults to "Homogeneous Neumann"

phi = spysparselinalg.spsolve(DivGrad,q) # solving nabla*phi = 1
# consider pre-factorization with LU decomposition
# will speed up Poisson solver by x10

# pouring flattened solution back in 2D array
Phi = np.zeros((Nyc+2,Nxc+2))*np.nan # remember ghost cells
# keyboard()
Phi[np.where(pressureCells_Mask==True)] = phi


# Plot solution
fig = plt.figure(0, figsize=(figwidth,figheight))
#ax   = fig.add_axes([0.15,0.15,0.8,0.8])
ax = fig.add_subplot(111)
ax.spy(DivGrad)
m=1.2
ax.set_xlabel(r"$x$",fontsize=textFontSize*m)
ax.set_ylabel(r"$y$",fontsize=textFontSize*m,rotation=90)
plt.title(BC,fontsize=textFontSize*m)
plt.setp(ax.get_xticklabels(),fontsize=gcafontSize*m)
plt.setp(ax.get_yticklabels(),fontsize=gcafontSize*m)
fig.tight_layout()

fig_name = 'p1_spy_Nu.png'
figure_path = './../figure/'
fig_fullpath = figure_path + fig_name
plt.savefig(fig_fullpath)
plt.close()

##############################################################################################
########								Linear Gau								##############
##############################################################################################
##############################################################################################

Di="Homogeneous Dirichlet"
Nu="Homogeneous Neumann"
BC=Di
Re=1.
n=1
N=128
n_list=np.array([2,8,16])
for iin,n in enumerate(n_list):
	#########################################
	############### User Input ##############

	# number of (pressure) cells = mass conservation cells
	Nxc  = N
	Nyc  = N
	Np   = Nxc*Nyc
	Lx   = 1.
	Ly   = 1.

	#########################################
	######## Preprocessing Stage ############

	# You might have to include ghost-cells here
	# Depending on your application

	# define grid for u and v velocity components first
	xsi_u = np.linspace(0.,1.0,Nxc+1)
	xsi_v = np.linspace(0.,1.0,Nyc+1)
	# uniform grid
	xu = (xsi_u)*Lx
	yv = (xsi_v)*Ly
	# (non-sense non-uniform grid)
	#xu = (xsi_u**4.)*Lx # can be replaced by non-uniform grid
	#yv = (xsi_v**4.)*Ly # can be replaced by non-uniform grid

	# creating ghost cells
	dxu0 = np.diff(xu)[0]
	dxuL = np.diff(xu)[-1]
	xu = np.concatenate([[xu[0]-dxu0],xu,[xu[-1]+dxuL]])

	dyv0 = np.diff(yv)[0]
	dyvL = np.diff(yv)[-1]
	yv = np.concatenate([[yv[0]-dyv0],yv,[yv[-1]+dyvL]])

	dxc = np.diff(xu)  # pressure-cells spacings
	dyc = np.diff(yv)

	xc = 0.5*(xu[:-1]+xu[1:])  # total of Nxc cells
	yc = 0.5*(yv[:-1]+yv[1:])  # total of Nyc cells

	# note that indexing is Xc[j_y,i_x] or Xc[j,i]
	[Xc,Yc]   = np.meshgrid(xc,yc)     # these arrays are with ghost cells
	[Dxc,Dyc] = np.meshgrid(dxc,dyc)   # these arrays are with ghost cells


	pressureCells_Mask = np.zeros(Xc.shape)
	pressureCells_Mask[1:-1,1:-1] = True


	# number of actual pressure cells
	Np = len(np.where(pressureCells_Mask==True)[0])


	f=-2*np.pi*n*np.cos(2*np.pi*n*Xc)*np.sin(2*np.pi*n*Yc)\
		+1./Re*(-8.*np.pi*np.pi*n*n*np.sin(2*np.pi*n*Xc)*np.sin(2*np.pi*n*Yc))

	f=f[np.where(pressureCells_Mask==True)]
	print "constructing operators.."
	DivGrad = spatial_operators.create_DivGrad_operator(Dxc,Dyc,Xc,Yc,pressureCells_Mask,boundary_conditions=BC)
	Grad_x = spatial_operators.Grad_px_operator(Dxc,Dyc,Xc,Yc,pressureCells_Mask,boundary_conditions=Di)

	AA=DivGrad+Grad_x
	#phi = spysparselinalg.spsolve(AA,f) # solving nabla*phi = 1
	#w1=np.array([0.2,0.4,0.6,0.8])


	w_list=np.linspace(1,1.99,10)
	#w_list= w_list[5:7]





	###########################################################
	# 					find w uniformly
	kk=np.zeros(w_list.shape)
	for ii,w in enumerate(w_list):
		phi,rr,kk[ii]=it.Gauss_Seidel_sparse(AA,f,w)

	kk_mask=1.0*kk

	ic=np.argmin(kk)
	a=w_list[ic-1]
	b=w_list[ic+1]
	###########################################################
	# 					find w through half

	a0=1.0*a
	b0=1.0*b
	phi,rr,kka=it.Gauss_Seidel_sparse(AA,f,a)
	phi,rr,kkb=it.Gauss_Seidel_sparse(AA,f,b)
	kka0=kka*1.0
	kkb0=kkb*1.0
	c=np.zeros(10)
	kkk=np.zeros(10)
	print "begin loop"
	for i in range(10):
		c[i]=0.5*(a+b)
		phi,rr,kkk[i]=it.Gauss_Seidel_sparse(AA,f,c[i])
		print '==========================='
		print a,b,c[i]
		print kka,kkb,kkk[i]
		if (kka<np.abs(kkb)):
			b=c[i]*1.0
			phi,rr,kkb=it.Gauss_Seidel_sparse(AA,f,b)
		elif (np.abs(kka)>np.abs(kkb)):
			a=c[i]*1.0	
			phi,rr,kka=it.Gauss_Seidel_sparse(AA,f,a)
		else:
			d=0.25*(b-a)+a
			e=0.75*(b-a)+a		
			phi,rr,kkd=it.Gauss_Seidel_sparse(AA,f,d)
			phi,rr,kke=it.Gauss_Seidel_sparse(AA,f,e)
			if (kkd<kke):
				b=c[i]*1.0
				phi,rr,kkb=it.Gauss_Seidel_sparse(AA,f,b)	
			else :
				a=c[i]*1.0	
				phi,rr,kka=it.Gauss_Seidel_sparse(AA,f,a)				





	fname="p1opt_N_"+str(N)+"_n_"+str(n) +".dat"	
	fpath='./../data/opt/'
	fout = open(fpath+fname, 'w')
	for k in range(len(w_list)):
	     fout.write(str(w_list[k])+" "+str(kk[k])+"\n")

	for k in range(len(c)):
	     fout.write(str(c[k])+" "+str(kkk[k])+"\n")
	fout.close()

	fname="p1res_N_"+str(N)+"_n_"+str(n) +".dat"	
	fpath='./../data/opt/'
	fout = open(fpath+fname, 'w')
	for k in range(len(rr)):
	     fout.write(str(k+1)+' '+str(rr[k])+"\n")

	fout.close()	



##############################################################################################
########								Linear Re								##############
##############################################################################################
##############################################################################################



		# number of actual pressure cells
	Np = len(np.where(pressureCells_Mask==True)[0])


	f=-2*np.pi*n*np.cos(2*np.pi*n*Xc)*np.sin(2*np.pi*n*Yc)\
		+1./Re*(-8.*np.pi*np.pi*n*n*np.sin(2*np.pi*n*Xc)*np.sin(2*np.pi*n*Yc))

	f=f[np.where(pressureCells_Mask==True)]
	print "constructing operators.."
	DivGrad = spatial_operators.create_DivGrad_operator(Dxc,Dyc,Xc,Yc,pressureCells_Mask,boundary_conditions=BC)
	Grad_x = spatial_operators.Grad_px_operator(Dxc,Dyc,Xc,Yc,pressureCells_Mask,boundary_conditions=Di)

	AA=DivGrad+Grad_x
	#phi = spysparselinalg.spsolve(AA,f) # solving nabla*phi = 1
	#w1=np.array([0.2,0.4,0.6,0.8])


	w_list=np.linspace(1,1.99,10)
	#w_list= w_list[5:7]





	###########################################################
	# 					find w uniformly
	kk=np.zeros(w_list.shape)
	for ii,w in enumerate(w_list):
		phi,rr,kk[ii]=it.Gauss_Seidel_sparse(AA,f,w)

	kk_mask=1.0*kk

	ic=np.argmin(kk)
	a=w_list[ic-1]
	b=w_list[ic+1]
	###########################################################
	# 					find w through half

	a0=1.0*a
	b0=1.0*b
	phi,rr,kka=it.Gauss_Seidel_sparse(AA,f,a)
	phi,rr,kkb=it.Gauss_Seidel_sparse(AA,f,b)
	kka0=kka*1.0
	kkb0=kkb*1.0
	c=np.zeros(10)
	kkk=np.zeros(10)
	print "begin loop"
	for i in range(10):
		c[i]=0.5*(a+b)
		phi,rr,kkk[i]=it.Gauss_Seidel_sparse(AA,f,c[i])
		print '==========================='
		print a,b,c[i]
		print kka,kkb,kkk[i]
		if (kka<np.abs(kkb)):
			b=c[i]*1.0
			phi,rr,kkb=it.Gauss_Seidel_sparse(AA,f,b)
		elif (np.abs(kka)>np.abs(kkb)):
			a=c[i]*1.0	
			phi,rr,kka=it.Gauss_Seidel_sparse(AA,f,a)
		else:
			d=0.25*(b-a)+a
			e=0.75*(b-a)+a		
			phi,rr,kkd=it.Gauss_Seidel_sparse(AA,f,d)
			phi,rr,kke=it.Gauss_Seidel_sparse(AA,f,e)
			if (kkd<kke):
				b=c[i]*1.0
				phi,rr,kkb=it.Gauss_Seidel_sparse(AA,f,b)	
			else :
				a=c[i]*1.0	
				phi,rr,kka=it.Gauss_Seidel_sparse(AA,f,a)				





	fname="p2_N256n16"+"_Re_"+str(Re) +".dat"	
	fpath='./../data/opt/'
	fout = open(fpath+fname, 'w')
	for k in range(len(w_list)):
	     fout.write(str(w_list[k])+" "+str(kk[k])+"\n")

	for k in range(len(c)):
	     fout.write(str(c[k])+" "+str(kkk[k])+"\n")
	fout.close()

	fname="p2_N256n16"+"_Re_"+str(Re) +"_res.dat"	
	fpath='./../data/opt/'
	fout = open(fpath+fname, 'w')
	for k in range(len(rr)):
	     fout.write(str(k+1)+' '+str(rr[k])+"\n")

	fout.close()	
##############################################################################################
########								Non a									##############
##############################################################################################
##############################################################################################
n=2
N=192

Re=45.
if True:
	#########################################
	############### User Input ##############

	# number of (pressure) cells = mass conservation cells
	Nxc  = N
	Nyc  = N
	Np   = Nxc*Nyc
	Lx   = 1.
	Ly   = 1.

	#########################################
	######## Preprocessing Stage ############

	# You might have to include ghost-cells here
	# Depending on your application

	# define grid for u and v velocity components first
	xsi_u = np.linspace(0.,1.0,Nxc+1)
	xsi_v = np.linspace(0.,1.0,Nyc+1)
	# uniform grid
	xu = (xsi_u)*Lx
	yv = (xsi_v)*Ly
	# (non-sense non-uniform grid)
	#xu = (xsi_u**4.)*Lx # can be replaced by non-uniform grid
	#yv = (xsi_v**4.)*Ly # can be replaced by non-uniform grid

	# creating ghost cells
	dxu0 = np.diff(xu)[0]
	dxuL = np.diff(xu)[-1]
	xu = np.concatenate([[xu[0]-dxu0],xu,[xu[-1]+dxuL]])

	dyv0 = np.diff(yv)[0]
	dyvL = np.diff(yv)[-1]
	yv = np.concatenate([[yv[0]-dyv0],yv,[yv[-1]+dyvL]])

	dxc = np.diff(xu)  # pressure-cells spacings
	dyc = np.diff(yv)

	xc = 0.5*(xu[:-1]+xu[1:])  # total of Nxc cells
	yc = 0.5*(yv[:-1]+yv[1:])  # total of Nyc cells

	# note that indexing is Xc[j_y,i_x] or Xc[j,i]
	[Xc,Yc]   = np.meshgrid(xc,yc)     # these arrays are with ghost cells
	[Dxc,Dyc] = np.meshgrid(dxc,dyc)   # these arrays are with ghost cells


	pressureCells_Mask = np.zeros(Xc.shape)
	pressureCells_Mask[1:-1,1:-1] = True

	# Introducing obstacle in pressure Mask
	obstacle_radius = 0.3*Lx # set equal to 0.0*Lx to remove obstacle
	distance_from_center = np.sqrt(np.power(Xc-Lx/2.,2.0)+np.power(Yc-Ly/2.,2.0))
	j_obstacle,i_obstacle = np.where(distance_from_center<obstacle_radius)
	pressureCells_Mask[j_obstacle,i_obstacle] = True

	# number of actual pressure cells
	Np = len(np.where(pressureCells_Mask==True)[0])
	q  = np.ones(Np,)

	xc0=xc[1:-1]
	yc0=yc[1:-1]
	[Xc0,Yc0]   = np.meshgrid(xc0,yc0)

	# number of actual pressure cells
	Np = len(np.where(pressureCells_Mask==True)[0])
	q_ana=np.sin(2*np.pi*n*Xc0)*np.sin(2*np.pi*n*Yc0)


	f=2*np.pi*n*np.cos(2*np.pi*n*Xc)*np.sin(2*np.pi*n*Yc)*np.sin(2*np.pi*n*Xc)*np.sin(2*np.pi*n*Yc)\
		-1./Re*(-8.*np.pi*np.pi*n*n*np.sin(2*np.pi*n*Xc)*np.sin(2*np.pi*n*Yc))

	f=f[np.where(pressureCells_Mask==True)]

	DivGrad = spatial_operators.create_DivGrad_operator(Dxc,Dyc,Xc,Yc,pressureCells_Mask,boundary_conditions=BC)
	Grad_x = spatial_operators.Grad_px_operator(Dxc,Dyc,Xc,Yc,pressureCells_Mask,boundary_conditions=Di)

	k=0
	rms=100
	phi=np.zeros(q.shape)

	t1=time.time()


	while ((rms>0.00001)&(k<100)):
		phi_o=phi*1.0
		rhs=(phi_o *Grad_x.dot(phi_o)-f)*Re
		phi=spysparselinalg.spsolve(DivGrad,rhs) # solving nabla*phi = 1
		if (k==0):
			r0= DivGrad.dot(phi)-rhs
			r=1000000.
		else:
			r= DivGrad.dot(phi)-rhs				
		rms=np.mean((phi-phi_o)**2)
		#rms=np.mean((r-r0)**2)
		k=k+1
		print Re,k, rms
	t2=time.time()


	final_rms=np.mean((phi-q_ana.flatten())**2)**0.5
	print str(1),Re,k,t2-t1,final_rms
##############################################################################################
########								Non b									##############
##############################################################################################
##############################################################################################
DivGrad = spatial_operators.create_DivGrad_operator(Dxc,Dyc,Xc,Yc,pressureCells_Mask,boundary_conditions=BC)
Grad  = spatial_operators.Grad_px_operator(Dxc,Dyc,Xc,Yc,pressureCells_Mask,boundary_conditions=Di)
Grad_x=scysparse.csr_matrix((Np,Np),dtype="float64")
Grad_x=1.0*Grad

k=0
rms=100
phi=np.zeros(q_ana.size)#+0.01
#phi[2]=1.
eps=-1./Re
t1=time.time()	
while ((rms>0.00001)&(k<100)):
	phi_o=phi*1.0

	rhs=1./Re*DivGrad.dot(phi_o)+f+eps*DivGrad.dot(phi_o)

	phi_sub=phi_o*1.0
	l=0
	rms2=10

	while ((rms2>0.0001)&(l<200)):
		phi_sub_o=phi_sub*1.0
		Grad_x=scysparse.diags(phi_sub_o).dot(Grad)
		AA=eps*DivGrad+Grad_x			
		phi_sub=spysparselinalg.spsolve(AA,rhs)
		rms2=np.mean((phi_sub_o-phi_sub)**2)**0.5
		l=l+1
		print "inner,",l,rms2

	phi=1.0*phi_sub
	rms=np.mean((phi-phi_o)**2)**0.5
	k=k+1
	print "outer,",k, rms

t2=time.time()	

final_rms=np.mean((phi-q_ana.flatten())**2)**0.5
print str(2),Re,k,t2-t1,final_rms

##############################################################################################
########								Non LU									##############
##############################################################################################
##############################################################################################
DivGrad = spatial_operators.create_DivGrad_operator(Dxc,Dyc,Xc,Yc,pressureCells_Mask,boundary_conditions=BC)
Grad  = spatial_operators.Grad_px_operator(Dxc,Dyc,Xc,Yc,pressureCells_Mask,boundary_conditions=Di)
Grad_x=scysparse.csr_matrix((Np,Np),dtype="float64")
Grad_x=1.0*Grad

k=0
rms=100
phi=np.zeros(q_ana.size)#+0.01
#phi[2]=1.
eps=-1./Re
t1=time.time()
while ((rms>0.00001)&(k<1000)):
	phi_o=phi*1.0

	rhs=1./Re*DivGrad.dot(phi_o)+f+eps*DivGrad.dot(phi_o)

	phi_sub=phi_o*1.0
	l=0
	rms2=10

	while ((rms2>0.0001)&(l<20)):
		phi_sub_o=phi_sub*1.0
		Grad_x=scysparse.diags(phi_sub_o).dot(Grad)
		AA=eps*DivGrad+Grad_x			
		BB=splu(AA)
		phi_sub=BB.solve(rhs)
		#phi_sub=spysparselinalg.spsolve(AA,rhs)
		rms2=np.mean((phi_sub_o-phi_sub)**2)**0.5
		l=l+1
		print "inner,",l,rms2

	phi=1.0*phi_sub
	rms=np.mean((phi-phi_o)**2)**0.5
	k=k+1
	print "outer,",k, rms
t2=time.time()	
print "final rms:",np.mean((phi-q_ana.flatten())**2)**0.5

final_rms=np.mean((phi-q_ana.flatten())**2)**0.5
print str(3),Re,k,t2-t1,final_rms



##############################################################################################
########								Taylor Green							##############
##############################################################################################
##############################################################################################




import os
import sys
import numpy as np
import scipy.sparse as scysparse
from pdb import set_trace
from time import sleep
import spatial_operators
import spatial_operators_uv2 as opuv
import scipy.sparse as scysparse
import scipy.sparse.linalg as spysparselinalg  # sparse linear algebra
import scipy.linalg as scylinalg               # non-sparse linear algebra
import pylab as plt
from matplotlib import rc as matplotlibrc
import iteration as it
import time # has the equivalent of tic/toc
import time_adv as tim_adv
import rhs as rrhs
# Figure settings 
matplotlibrc('text.latex', preamble='\usepackage{color}')
matplotlibrc('text',usetex=True)
matplotlibrc('font', family='serif')


figwidth       = 10
figheight      = 6
lineWidth      = 1.5*2
textFontSize   = 28*0.5
gcafontSize    = 30*0.5


machine_epsilon = np.finfo(float).eps

Di="Homogeneous Dirichlet"
Nu="Homogeneous Neumann"

#############################################################################
################################# User Input ################################

#flow parameter

nu 	 = 1.
rho  = 1.
pref = 0.
Lx   = 2*np.pi
Ly   = 2*np.pi

cfl  = 0.1
rk 	 = 2


N_list=np.array([32,64])

rms_u=np.zeros(len(N_list))
rms_v=np.zeros(len(N_list))
rms_p=np.zeros(len(N_list))

for ii,N in enumerate(N_list):
	# num parameter
	#N 	 = 64
	Nxc  = N
	Nyc  = N
	Np   = Nxc*Nyc

	#############################################################################
	########################## 	Generate grid	 ##############################

	#..................................
	# 	Pressure gird
	#..................................


	# define grid for u and v velocity components first
	xsi_u = np.linspace(0.,1.0,Nxc+1)
	xsi_v = np.linspace(0.,1.0,Nyc+1)
	# uniform grid
	xu = (xsi_u)*Lx
	yv = (xsi_v)*Ly


	# (non-sense non-uniform grid)
	#xu = (xsi_u**4.)*Lx # can be replaced by non-uniform grid
	#yv = (xsi_v**4.)*Ly # can be replaced by non-uniform grid

	# creating ghost cells
	dxu0 = np.diff(xu)[0]
	dxuL = np.diff(xu)[-1]
	xu = np.concatenate([[xu[0]-dxu0],xu,[xu[-1]+dxuL]])

	dyv0 = np.diff(yv)[0]
	dyvL = np.diff(yv)[-1]
	yv = np.concatenate([[yv[0]-dyv0],yv,[yv[-1]+dyvL]])

	dxc = np.diff(xu)  # pressure-cells spacings
	dyc = np.diff(yv)

	xc = 0.5*(xu[:-1]+xu[1:])  # total of Nxc cells
	yc = 0.5*(yv[:-1]+yv[1:])  # total of Nyc cells

	# note that indexing is Xc[j_y,i_x] or Xc[j,i]
	[Xc,Yc]   = np.meshgrid(xc,yc)     # these arrays are with ghost cells
	[Dxc,Dyc] = np.meshgrid(dxc,dyc)   # these arrays are with ghost cells
	pressureCells_Mask = np.zeros(Xc.shape)
	pressureCells_Mask[1:-1,1:-1] = True

	numbered_pressureCells = -np.ones(Xc.shape,dtype='int64')
	jj_C,ii_C = np.where(pressureCells_Mask==True)
	Np = len(jj_C)     
	numbered_pressureCells[jj_C,ii_C] = range(0,Np) # automatic numbering done via 'C' flattening

	#..................................
	# 	Velocity gird
	#..................................

	xu00 = (xsi_u)*Lx
	yv00 = (xsi_v)*Ly
	xu0 =xu00 [:-1]
	yv0 =yv00 [:-1]
	xc0=xc[1:-1]
	yc0=yc[1:-1]

	[Xu,Yu]   = np.meshgrid(xu0,yc0)     # these arrays are with ghost cells
	[Xv,Yv]   = np.meshgrid(xc0,yv0)    # these arrays are with ghost cells

	dxu=np.diff(xu00)
	dyv=np.diff(yv00)
	[Dxu,Dyu] = np.meshgrid(dxu,dyc[1:-1])   # these arrays are with ghost cells
	[Dxv,Dyv] = np.meshgrid(dxc[1:-1],dyv)   # these arrays are with ghost cells
	Nu=Xu.size
	Nv=Xv.size
	number_u=np.array(range(Nu)).reshape(Xu.shape)
	number_v=np.array(range(Nv)).reshape(Xv.shape)


	[Xc0,Yc0]   = np.meshgrid(xc0,yc0) 
	#############################################################################
	########################## 	Generate operator	 ##########################

	#interpolate operator, from center to u face
	intx=opuv.int_x(Xu,number_u)
	inty=opuv.int_y(Yu,number_u)
	#div  operator, go from face to u cell center
	div_x=opuv.div_x_uc(Dxu,number_u)
	div_y=opuv.div_y_uc(Dyv,number_u)
	#grad operator,from cell center to u faces
	grad_x=opuv.grad_x(Dxu,number_u)
	grad_y=opuv.grad_y(Dyv,number_u)
	#DivGrad = spatial_operators.create_DivGrad_operator(Dxc,Dyc,Xc,Yc,pressureCells_Mask,boundary_conditions="Homogeneous Neumann")
	DivGrad =div_x.dot(grad_x)+div_y.dot(grad_y)
	#############################################################################
	########################## 		Initialization		 ########################

	u = -np.cos(Xu)*np.sin(Yu)
	v =  np.sin(Xv)*np.cos(Yv)
	u0 = -np.cos(Xu)*np.sin(Yu)
	v0 =  np.sin(Xv)*np.cos(Yv)
	p0 = -0.25*(np.cos(2*Xc0)+np.cos(2*Yc0))-(-0.25*(np.cos(2*Xc0[0,1])+np.cos(2*Yc0[0,1])))
	# u=Xu
	# v=np.ones(Xu.shape)
	p = -0.25*(np.cos(2*Xc0)+np.cos(2*Yc0))+0.5

	umag=(u**2+v**2)**0.5


	dt = 0.0001	
	print "initialized with dt = ",dt


	u=u.flatten()
	v=v.flatten()

	# Xt=Xc[1:-1,1:-1]
	# Yt=Yc[1:-1,1:-1]
	# ut = -np.cos(Xt)*np.sin(Yt)
	# vt =  np.sin(Xt)*np.cos(Yt)
	# ut=ut.flatten()
	# vt=vt.flatten()

	#############################################################################
	########################## 		Time advance		 ########################

	t=0
	it=0

	frame_iter = 0
	while((t<=1.001)):
		#print np.max(u)
		if (np.abs(t-1.)<=dt):
			u_ana=u0*np.exp(-2.*t)
			v_ana=v0*np.exp(-2.*t)
			p_ana=p0*np.exp(-4.*t)
			p_ana=p_ana-p_ana[0,1]
			rms_u[ii]=np.mean((u_ana.flatten()-u)**2)**0.5
			rms_v[ii]=np.mean((v_ana.flatten()-v)**2)**0.5
			rms_p[ii]=np.mean((p_ana.flatten()-p)**2)**0.5	
	
		# time advancement
		t=t+dt
		it=it+1

		u_new,v_new,p=tim_adv.get_unew(DivGrad,intx,inty,div_x,div_y,grad_x,grad_y,u,v,rk,dt,pref,nu,rho)
		u,v=u_new,v_new
	print "finish N=",N,rms_u[ii],rms_v[ii],rms_p[ii]

