import os
import sys
import numpy as np
import scipy.sparse as scysparse
from time import sleep
import scipy.sparse as scysparse
import scipy.sparse.linalg as spysparselinalg  # sparse linear algebra
import scipy.linalg as scylinalg               # non-sparse linear algebra
import pylab as plt
from matplotlib import rc as matplotlibrc
from pdb import set_trace
# Figure settings
matplotlibrc('text.latex', preamble='\usepackage{color}')
matplotlibrc('text',usetex=True)
matplotlibrc('font', family='serif')

fig_sol =0
fig_spy =1
figwidth       = 8
figheight      = 6
lineWidth      = 1.5
textFontSize   = 28*0.65
gcafontSize    = 30*0.65

fname="p2_N256n16_Re_0.01.dat"
fpath='./../data/opt/'
N64_1=np.loadtxt(fpath+fname)
N64_1=N64_1[np.argsort(N64_1[:,0])]

fname="p2_N256n16_Re_1.0.dat"
fpath='./../data/opt/'
N64_4=np.loadtxt(fpath+fname)
N64_4=N64_4[np.argsort(N64_4[:,0])]


fname="p2_N256n16_Re_100.0.dat"
fpath='./../data/opt/'
N64_8=np.loadtxt(fpath+fname)
N64_8=N64_8[np.argsort(N64_8[:,0])]



fname="p2_N256n16_Re_10000.0.dat"
fpath='./../data/opt/'
N128_2=np.loadtxt(fpath+fname)
N128_2=N128_2[np.argsort(N128_2[:,0])]

fname="p2_N256n16_Re_1000000.0.dat"
fpath='./../data/opt/'
N128_8=np.loadtxt(fpath+fname)
N128_8=N128_8[np.argsort(N128_8[:,0])]


fname="p2_N256n16_Re_100000000.0.dat"
fpath='./../data/opt/'
N128_16=np.loadtxt(fpath+fname)
N128_16=N128_16[np.argsort(N128_16[:,0])]


#########################################################################
#########################################################################

fname="p2_N256n16_Re_0.01_res.dat"
fpath='./../data/opt/'
r64_1=np.loadtxt(fpath+fname)


fname="p2_N256n16_Re_1.0_res.dat"
fpath='./../data/opt/'
r64_4=np.loadtxt(fpath+fname)


fname="p2_N256n16_Re_100.0_res.dat"
fpath='./../data/opt/'
r64_8=np.loadtxt(fpath+fname)




fname="p2_N256n16_Re_10000.0_res.dat"
fpath='./../data/opt/'
r128_2=np.loadtxt(fpath+fname)


fname="p2_N256n16_Re_1000000.0_res.dat"
fpath='./../data/opt/'
r128_8=np.loadtxt(fpath+fname)



fname="p2_N256n16_Re_100000000.0_res.dat"
fpath='./../data/opt/'
r128_16=np.loadtxt(fpath+fname)



set_trace()
fig = plt.figure(0, figsize=(figwidth*1.8,figheight))

ax = fig.add_subplot(131)

ax.plot(N64_1[:,0],N64_1[:,1],'k-',marker='s',mfc='None',label='Re 0.01',alpha=0.3)
ax.plot(N64_4[:,0],N64_4[:,1],'k-',marker='o',mfc='None',label='Re 1.0',alpha=0.4)
ax.plot(N64_8[:,0],N64_8[:,1],'k-',marker='^',mfc='None',label='Re 100.0',alpha=0.5)

ax.plot( N128_2[:,0],N128_2[:,1], 'k-',marker='*',mfc='None',label='Re 10000.0' ,alpha=0.6)
ax.plot( N128_8[:,0],N128_8[:,1], 'k-',marker='<',mfc='None',label='Re 1000000.0',alpha=0.7)
ax.plot(N128_16[:,0],N128_16[:,1],'k-',marker='>',mfc='None',label='Re 100000000.0',alpha=0.8)



ax.set_yscale('log')
#ax.scatter(N64_1[np.min(N64_1[:,1],1],np.argmin(N64_1[:,1])))#,marker='s',mfc='g')
m=1.2
ax.set_xlabel(r"$sor:w$",fontsize=textFontSize*m)
ax.set_ylabel(r"iteration:k",fontsize=textFontSize*m,rotation=90)
plt.setp(ax.get_xticklabels(),fontsize=gcafontSize*m)
plt.setp(ax.get_yticklabels(),fontsize=gcafontSize*m)
leg = plt.legend(loc='upper left', ncol=1,shadow=True, fancybox=True,fontsize=textFontSize*0.8)
leg.get_frame().set_alpha(0.8)


ax = fig.add_subplot(132)
ax.loglog(r64_1[:,0],r64_1[:,1],'k-',marker='s',mfc='None',label='Re 0.01',alpha=0.3)
ax.loglog(r64_4[:,0],r64_4[:,1],'k-',marker='o',mfc='None',label='Re 1.0',alpha=0.4)
ax.loglog(r64_8[:,0],r64_8[:,1],'k-',marker='^',mfc='None',label='Re 100.0',alpha=0.5)
ax.loglog( r128_2[:,0],r128_2[:,1],'k-',marker='*',mfc='None',label='Re 10000.0' ,alpha=0.6)
ax.loglog( r128_8[:,0],r128_8[:,1],'k-',marker='<',mfc='None',label='Re 1000000.0',alpha=0.7)
ax.loglog(r128_16[:,0],r128_16[:,1],'k-',marker='>',mfc='None',label='Re 100000000.0',alpha=0.8)

ax.set_xlabel("iteration:k",fontsize=textFontSize*m)
ax.set_ylabel(r"$||r^k||$",fontsize=textFontSize*m,rotation=90)
plt.setp(ax.get_xticklabels(),fontsize=gcafontSize*m)
plt.setp(ax.get_yticklabels(),fontsize=gcafontSize*m)


ax = fig.add_subplot(133)
ax.loglog(r64_1[:,0],r64_1[:,1]/r64_1[0,1],'k-',marker='s',mfc='None',label='Re 0.01',alpha=0.3)
ax.loglog(r64_4[:,0],r64_4[:,1]/r64_4[0,1],'k-',marker='o',mfc='None',label='Re 1.0',alpha=0.4)
ax.loglog(r64_8[:,0],r64_8[:,1]/r64_8[0,1],'k-',marker='^',mfc='None',label='Re 100.0',alpha=0.5)
ax.loglog( r128_2[:,0],r128_2[:,1]/r128_2[0,1],'k-',marker='*',mfc='None',label='Re 10000.0' ,alpha=0.6)
ax.loglog( r128_8[:,0],r128_8[:,1]/r128_8[0,1],'k-',marker='<',mfc='None',label='Re 1000000.0',alpha=0.7)
ax.loglog(r128_16[:,0],r128_16[:,1]/r128_16[0,1],'k-',marker='>',mfc='None',label='Re 100000000.0',alpha=0.8)

ax.set_xlabel("iteration:k",fontsize=textFontSize*m)
ax.set_ylabel(r"$||r^k||/||r^0||$",fontsize=textFontSize*m,rotation=90)
plt.setp(ax.get_xticklabels(),fontsize=gcafontSize*m)
plt.setp(ax.get_yticklabels(),fontsize=gcafontSize*m)

#ax.set_yscale('log')
# ax.loglog(dt_list,tuw1,'o-',label='1st-upwind')
# ax.loglog(dt_list,tuw2,'o-',label='2nd-upwind')
# ax.loglog(dt_list,tcd2,'o-',label='2nd-cds')

# ax.loglog(dt_list,dt_list**(-2.)/1000000000000,'k--',label='2nd-ref',lw=lineWidth)
# m=1.2
# ax.set_xlabel(r"$1/dt$",fontsize=textFontSize*m)
# ax.set_ylabel(r"$RMS$",fontsize=textFontSize*m,rotation=90)
# plt.setp(ax.get_xticklabels(),fontsize=gcafontSize*m)
# plt.setp(ax.get_yticklabels(),fontsize=gcafontSize*m)
# leg = plt.legend(loc='best', ncol=1,  shadow=False, fancybox=True,fontsize=textFontSize)
# leg.get_frame().set_alpha(0.8)



fig.tight_layout()
#fig_name = 'p3_diff.png'


fig_name = 'p1_reynolds_.png'
figure_path = './../figure/'
fig_fullpath = figure_path + fig_name
plt.savefig(fig_fullpath)
plt.show()
plt.close()