import os
import sys
import numpy as np 
import scipy.sparse as scysparse
from pdb import set_trace
import spac_disc as sd 
import matplotlib.pyplot as plt
from matplotlib import rc as matplotlibrc

def Problem3():
	Older_2nd=1
	Older_4th=1
	mesh	 =1

	matplotlibrc('text.latex', preamble='\usepackage{color}')
	matplotlibrc('text',usetex=True)
	matplotlibrc('font', family='serif')

	fig_width = 8
	fig_height = 8
	textFontSize   = 10*1.5
	gcafontSize    = 26
	lineWidth      = 2  

	N0=40
	n=10

	a_list=np.array([0.0001,0.001,0.01,0.1])
	rms_2=np.zeros(n)
	rms_4=np.zeros(n)
	dx_list=np.zeros(n)
	A_2=np.zeros((len(a_list),n))
	A_4=np.zeros((len(a_list),n))
	grid=np.zeros((len(a_list),N0))

	fig = plt.figure(0,figsize=(fig_width*0.8,fig_height*0.6))
	ax = fig.add_subplot(111)

	for ia,a in enumerate(a_list):
		for it in range(n):
			N=N0*2**it
			x0=np.linspace(0,1,N)
			dx=x0[2]-x0[1]
			dx_list[it]=dx		

			#Disteration
			sigma=a
			s = dx*np.random.normal(0, sigma, N)	
			x=x0+s

			#ax.plot(x0,x)
			#plt.show()
			#set_trace()
			f=np.cos(4*np.pi*x)
			fx=-4*np.pi*np.sin(4*np.pi*x)
			fxx=-16*np.pi*np.pi*np.cos(4*np.pi*x)


			if Older_2nd:
				Ax=np.zeros((N,N))
				for i in range(N):
					if (i==0):
						xl=x[0:3]
						Ax[0,0:3]=sd.Lagr_der_base(xl,x[0])
					elif (i==N-1):
						xl=x[-3:]
						Ax[N-1,-3:]=sd.Lagr_der_base(xl,x[N-1])
					else:
						xl=x[i-1:i+2]
						Ax[i,i-1:i+2]=sd.Lagr_der_base(xl,x[i])

				fx_sol=Ax.dot(f)
				fxx_sol=Ax.dot(fx_sol)

				rms_2[it]=(np.mean((fxx_sol[2:-2]-fxx[2:-2])**2))**0.5
			

			if Older_4th:
				Ax=np.zeros((N,N))
				for i in range(N):
					if (i==0):
						xl=x[0:5]
						Ax[0,0:5]=sd.Lagr_der_base(xl,x[0])
					elif (i==1):
						xl=x[0:5]
						Ax[1,0:5]=sd.Lagr_der_base(xl,x[1])				
					elif (i==N-1):
						xl=x[-5:]
						Ax[N-1,-5:]=sd.Lagr_der_base(xl,x[N-1])
					elif (i==N-2):
						xl=x[-5:]
						Ax[N-2,-5:]=sd.Lagr_der_base(xl,x[N-2])				
					else:
						xl=x[i-2:i+3]
						Ax[i,i-2:i+3]=sd.Lagr_der_base(xl,x[i])

				fx_sol=Ax.dot(f)
				fxx_sol=Ax.dot(fx_sol)

				rms_4[it]=(np.mean((fxx_sol[4:-4]-fxx[4:-4])**2))**0.5
			if (it==0):
				grid[ia,:]=x		
		A_2[ia,:]=rms_2
		A_4[ia,:]=rms_4


	if Older_2nd:		
		for i in range (len(a_list)):
			labelss="sigma="+str(a_list[i])
			ax.loglog(dx_list**(-1),A_2[i,:],marker='o',mfc="None",label=labelss)
		ax.loglog(dx_list**(-1),dx_list**(2)*4000,'k--',label="2nd order ref")

		handles, labels = ax.get_legend_handles_labels()
		ax.legend(handles, labels,loc=0,numpoints=1)
		#plt.ylim(1e-16, 1)
		plt.setp(ax.get_xticklabels(),fontsize=gcafontSize)
		plt.setp(ax.get_yticklabels(),fontsize=gcafontSize)
		ax.grid('on',which='both')
		handles, labels = ax.get_legend_handles_labels()
		ax.legend(handles, labels,fontsize=textFontSize,loc=0,numpoints=1)

		ax.set_xlabel(r'$1/dx$',fontsize=gcafontSize)
		ax.set_ylabel(r'$RMS$',fontsize=gcafontSize)
		plt.setp(ax.get_xticklabels(),fontsize=textFontSize)
		plt.setp(ax.get_yticklabels(),fontsize=textFontSize)
		fig.tight_layout()
		fig_name = 'p3_2oder.png'
		figure_path = './../figure/'
		fig_fullpath = figure_path + fig_name
		plt.savefig(fig_fullpath)
		plt.show()
		plt.close()
		print fig_name+' saved!'	

	if Older_4th:		
		fig = plt.figure(0,figsize=(fig_width*0.8,fig_height*0.6))
		ax = fig.add_subplot(111)	
		for i in range (len(a_list)):
			labelss="sigma="+str(a_list[i])
			ax.loglog(dx_list**(-1),A_4[i,:],marker='o',mfc="None",label=labelss)
		ax.loglog(dx_list**(-1),dx_list**(4)*100000,'k--',label="4th order ref")

		handles, labels = ax.get_legend_handles_labels()
		ax.legend(handles, labels,loc=0,numpoints=1)
		#plt.ylim(1e-16, 1)
		plt.setp(ax.get_xticklabels(),fontsize=gcafontSize)
		plt.setp(ax.get_yticklabels(),fontsize=gcafontSize)
		ax.grid('on',which='both')
		handles, labels = ax.get_legend_handles_labels()
		ax.legend(handles, labels,fontsize=textFontSize,loc=0,numpoints=1)

		ax.set_xlabel(r'$1/dx$',fontsize=gcafontSize)
		ax.set_ylabel(r'$RMS$',fontsize=gcafontSize)
		plt.setp(ax.get_xticklabels(),fontsize=textFontSize)
		plt.setp(ax.get_yticklabels(),fontsize=textFontSize)
		fig.tight_layout()
		fig_name = 'p3_4oder.png'
		figure_path = './../figure/'
		fig_fullpath = figure_path + fig_name
		plt.savefig(fig_fullpath)
		plt.show()
		plt.close()
		print fig_name+' saved!'	





