import os
import sys
import numpy as np 
import scipy.sparse as scysparse
from pdb import set_trace
import spac_disc as sd 
import matplotlib.pyplot as plt
from matplotlib import rc as matplotlibrc

def Problem1():
	#	P1 :Largranjin Poly
	#	P2: Comparison of collection and staggered scheme
	#	P4:	Vandermonde matrix

	P1_1 = 1
	P1_2 = 1
	P1_4 = 1


	matplotlibrc('text.latex', preamble='\usepackage{color}')
	matplotlibrc('text',usetex=True)
	matplotlibrc('font', family='serif')

	fig_width = 8
	fig_height = 8
	textFontSize   = 10*1.5
	gcafontSize    = 26
	lineWidth      = 2  
	########################################################################################
	###############################			P1_2					########################
	########################################################################################
	if P1_1:
		stencil=np.linspace(-1,1,3)
		h1=sd.Lagr_base(stencil,-1)
		h2=sd.Lagr_base(stencil,0)
		h3=sd.Lagr_base(stencil,1)	

		h11 = np.polyfit(stencil,h1,2)
		curve1 = np.polyval(h11,np.linspace(-1,1,100))
		h22 = np.polyfit(stencil,h2,2)
		curve2 = np.polyval(h22,np.linspace(-1,1,100))
		h33 = np.polyfit(stencil,h3,2)
		curve3 = np.polyval(h33,np.linspace(-1,1,100))

		fig = plt.figure(0,figsize=(fig_width*0.8,fig_height*0.5))
		ax = fig.add_subplot(111)	
		ax.axhline(0,0,1,c='k')
		ax.scatter(stencil,np.zeros(3),marker='o',c='k')

		ax.plot(np.linspace(-1,1,100),curve1,'r')
		ax.plot(np.array([-1,-1]),np.array([0,1]),ls='--',c='r')
		ax.scatter(-1,1,marker='s',c='r')	

		ax.plot(np.linspace(-1,1,100),curve2,'g')
		ax.plot(np.array([0,0]),np.array([0,1]),ls='--',c='g')
		ax.scatter(0,1,marker='s',c='g')	

		ax.plot(np.linspace(-1,1,100),curve3,'b')
		ax.plot(np.array([1,1]),np.array([0,1]),ls='--',c='b')
		ax.scatter(1,1,marker='s',c='b')	
		ax.set_xlabel(r'$x$',fontsize=gcafontSize)
		ax.set_ylabel(r'$\phi_m$',fontsize=gcafontSize)
		plt.setp(ax.get_xticklabels(),fontsize=textFontSize)
		plt.setp(ax.get_yticklabels(),fontsize=textFontSize)
		fig.tight_layout()
		fig_name = 'p1_1a.pdf'
		figure_path = './../figure/'
		fig_fullpath = figure_path + fig_name
		plt.savefig(fig_fullpath)
		plt.show()
		plt.close()
		print fig_name+' saved!'	

		stencil=np.linspace(-3,3,7)

		h1=sd.Lagr_base(stencil,-3)	
		h2=sd.Lagr_base(stencil,-1)
		h3=sd.Lagr_base(stencil,0)
		h4=sd.Lagr_base(stencil,2)	

		h11 = np.polyfit(stencil,h1,6)
		curve1 = np.polyval(h11,np.linspace(-3,3,100))
		h22 = np.polyfit(stencil,h2,6)
		curve2 = np.polyval(h22,np.linspace(-3,3,100))
		h33 = np.polyfit(stencil,h3,6)
		curve3 = np.polyval(h33,np.linspace(-3,3,100))
		h44 = np.polyfit(stencil,h4,6)
		curve4 = np.polyval(h44,np.linspace(-3,3,100))

		fig = plt.figure(0,figsize=(fig_width*0.8,fig_height*0.5))
		ax = fig.add_subplot(111)	
		ax.axhline(0,0,1,c='k')
		ax.scatter(stencil,np.zeros(7),marker='o',c='k')

		ax.plot(np.linspace(-3,3,100),curve1,'r')
		ax.plot(np.array([-3,-3]),np.array([0,1]),ls='--',c='r')
		ax.scatter(-3,1,marker='s',c='r')	

		ax.plot(np.linspace(-3,3,100),curve2,'g')
		ax.plot(np.array([-1,-1]),np.array([0,1]),ls='--',c='g')
		ax.scatter(-1,1,marker='s',c='g')	

		ax.plot(np.linspace(-3,3,100),curve3,'b')
		ax.plot(np.array([0,0]),np.array([0,1]),ls='--',c='b')
		ax.scatter(0,1,marker='s',c='b')	

		ax.plot(np.linspace(-3,3,100),curve4,'y')
		ax.plot(np.array([2,2]),np.array([0,1]),ls='--',c='y')
		ax.scatter(2,1,marker='s',c='y')

		ax.set_xlabel(r'$x$',fontsize=gcafontSize)
		ax.set_ylabel(r'$\phi_m$',fontsize=gcafontSize)
		plt.setp(ax.get_xticklabels(),fontsize=textFontSize)
		plt.setp(ax.get_yticklabels(),fontsize=textFontSize)
		fig.tight_layout()
		fig_name = 'p1_1b.pdf'
		figure_path = './../figure/'
		fig_fullpath = figure_path + fig_name
		plt.savefig(fig_fullpath)
		plt.show()
		plt.close()
		print fig_name+' saved!'			



	########################################################################################
	###############################			P1_2					########################
	########################################################################################

	if P1_2:

		x_eval  = 0
		nn      = 20
		trun_error = list(range(12))
		fig        = list(range(12))
		case       = list(range(12))
		xx         = np.ones(nn+1,)
		#f(x) = np.tanh(x)*np.sin(5*x+1.5)
		dfdx_analytical = np.sin(5*x_eval+1.5)/np.cosh(x_eval)**2+5*np.cos(5*x_eval+1.5)*np.tanh(x_eval)

		case[0] = np.linspace(-1,1,3) 
		case[1] = np.linspace(-2,2,5)
		case[2] = np.linspace(-3,3,7)

		case[3] = np.linspace(0,1,2)
		case[4] = np.linspace(0,2,3)
		case[5] = np.linspace(0,3,4)

		case[6] = np.linspace(-1,1,2)
		case[7] = np.hstack((np.linspace(-2,-1,2),np.linspace(1,2,2)))
		case[8] = np.hstack((np.linspace(-3,-1,3),np.linspace(1,3,3)))

		case[9]  = np.hstack((np.array([-1]),np.array([1])))
		case[10] = np.hstack((np.array([-1]),np.linspace(1,2,2)))
		case[11] = np.hstack((np.array([-1]),np.linspace(1,3,3)))



		for i,x_stencil in enumerate (case):
			dis_error = np.zeros(nn+1,)
			dis_error_percent=  np.zeros(nn+1,)

			for j in range(nn+1):
				f = np.tanh(x_stencil)*np.sin(5*x_stencil+1.5)
				weight = sd.Lagr_der_base(x_stencil,x_eval)
				dfdx_hat = weight.dot(f)	
				dis_error[j] = np.abs(dfdx_hat-dfdx_analytical)

				x_stencil = x_stencil*0.1

			trun_error[i] = dis_error


		for j in range(nn+1):
			xx[j]=xx[j]*(0.1**j)
		xx    = xx**(-1)

		error_1 = xx**(-1)
		error_2 = xx**(-2)
		error_3 = xx**(-3)
		error_4 = xx**(-4)
		error_5 = xx**(-5)
		error_6 = xx**(-6)


		ax1=plt.figure(1,figsize=(16,8))
		for ii in range(6):
			plt.figure(1)
			fig[ii]=plt.subplot(231+ii)
			plt.sca(fig[ii])

			aa,=plt.loglog(xx, trun_error[ii],'k-',marker='o',ms='3',lw=lineWidth)  
	 

			bb,=plt.loglog(xx, error_1,'r--')  
			cc,=plt.loglog(xx, error_2,'g--')  
			dd,=plt.loglog(xx, error_3,'y--')  	
			ee,=plt.loglog(xx, error_4,'m--')
			ff,=plt.loglog(xx, error_5,'c--')
			gg,=plt.loglog(xx, error_6,'k--')
			plt.xlabel('1/dx',fontsize=textFontSize*1.5)
			plt.title('collocated-stencil  :%s'% case[ii],fontsize=textFontSize*1.2)
			plt.ylim(1e-16, 1)		
			plt.setp(fig[ii].get_xticklabels(),fontsize=textFontSize*1.2)
			plt.setp(fig[ii].get_yticklabels(),fontsize=textFontSize*1.2)		
			if (ii==0):
				plt.ylabel('trun error',fontsize=textFontSize*1.5)				
				plt.legend([aa,bb,cc,dd,ee,ff,gg], ('trun error','order:1','order:2','order:3','order:4','order:5','order:6'),loc='best',fontsize=textFontSize*1.2)

			if (ii==3):
				plt.ylabel('trun error',fontsize=textFontSize*1.5)				

			plt.subplots_adjust(left=0.1, bottom=0.1, right=0.9, top=0.9, hspace=0.5, wspace=0.3)
		#fig.tight_layout()
		fig_name = 'p1_2a.png'
		figure_path = './../figure/'
		fig_fullpath = figure_path + fig_name
		plt.savefig(fig_fullpath)
		plt.show()
		plt.close()
		print fig_name+' saved!'	

		ax1=plt.figure(2,figsize=(16,8))
		for ii in range(6):
			plt.figure(2)
			fig[ii]=plt.subplot(231+ii)
			plt.sca(fig[ii])
			aa,=plt.loglog(xx, trun_error[ii+6],'k-',marker='o',ms='3',lw=lineWidth)   

			bb,=plt.loglog(xx, error_1,'r--')  
			cc,=plt.loglog(xx, error_2,'g--')  
			dd,=plt.loglog(xx, error_3,'y--')  	
			ee,=plt.loglog(xx, error_4,'m--')
			ff,=plt.loglog(xx, error_5,'c--')
			gg,=plt.loglog(xx, error_6,'k--')
			plt.xlabel('1/dx',fontsize=textFontSize*1.5)	
			plt.ylim(1e-16, 1)
			plt.title('staggered-stencil  :%s'% case[ii+6],fontsize=textFontSize*1.2)
			plt.setp(fig[ii].get_xticklabels(),fontsize=textFontSize*1.2)
			plt.setp(fig[ii].get_yticklabels(),fontsize=textFontSize*1.2)		
			if (ii==0):		
				plt.ylabel('trun error',fontsize=textFontSize*1.5)	
				plt.legend([aa,bb,cc,dd,ee,ff,gg], ('trun error','order:1','order:2','order:3','order:4','order:5','order:6'),loc='best',fontsize=textFontSize*1.2)

			if (ii==3):
				plt.ylabel('trun error',fontsize=textFontSize*1.5)						
			plt.subplots_adjust(left=0.1, bottom=0.1, right=0.9, top=0.9, hspace=0.5, wspace=0.3)
		#fig.tight_layout()
		fig_name = 'p1_2b.png'
		figure_path = './../figure/'
		fig_fullpath = figure_path + fig_name
		plt.savefig(fig_fullpath)
		plt.show()
		plt.close()
		print fig_name+' saved!'	






	########################################################################################
	###############################			P1_2					########################
	########################################################################################
	if P1_4:

		x_eval  = 0
		nn      = 20
		trun_error = list(range(12))
		xx         = np.ones(nn+1,)
		#f(x) = np.tanh(x)*np.sin(5*x+1.5)
		dfdx_analytical = np.sin(5*x_eval+1.5)/np.cosh(x_eval)**2+5*np.cos(5*x_eval+1.5)*np.tanh(x_eval)

		stencil = np.linspace(-2,2,5)


		Lar_error = np.zeros(nn+1,)
		Van_error = np.zeros(nn+1,)	
		dis_error_percent=  np.zeros(nn+1,)

		for j in range(nn+1):

			f = np.tanh(stencil)*np.sin(5*stencil+1.5)

			#	Largrangin
			weight = sd.Lagr_der_base(stencil,x_eval)
			dfdx_hat = weight.dot(f)	
			Lar_error[j] = np.abs(dfdx_hat-dfdx_analytical)
	 
			#	A:Vandermonde matrix
			N=len(stencil)
			A=np.ones((N,N))
			van_der_base=np.ones(N)
			for i, x in enumerate(stencil):
				A[:,i]=stencil**(i)
				if i==0:
					van_der_base[i]=0
				else:
					van_der_base[i]=x_eval**(i-1)

			coef=(np.linalg.inv(A)).dot(f)

			der_coef=coef*np.array(range(len(stencil)))
			Van_sol=np.sum(der_coef*van_der_base)	
			Van_error[j] = np.abs(Van_sol-dfdx_analytical)

			stencil = stencil*0.1
		

		#	Figure

		fig = plt.figure(0,figsize=(fig_width*0.8,fig_height*0.5))
		ax = fig.add_subplot(111)


		for j in range(nn+1):
			xx[j]=xx[j]*(0.1**j)
		xx    = xx**(-1)

		error_1 = xx**(-1)
		error_2 = xx**(-2)
		error_3 = xx**(-3)
		error_4 = xx**(-4)
		error_5 = xx**(-5)
		error_6 = xx**(-6)

		ax.loglog(xx, Lar_error,'k-',marker='+',ms='7',lw=lineWidth,label='Lar error') 
		ax.loglog(xx, Van_error,'r-',marker='s',ms='3',lw=lineWidth,label='Van error') 

		#ax.loglog(xx, error_3,'b--',label='order:3')  	
		ax.loglog(xx, error_4,'g--',label='order:4')
		plt.ylim(1e-18, 1)	
		handles, labels = ax.get_legend_handles_labels()
		ax.legend(handles, labels,fontsize=textFontSize,loc=0,numpoints=1)
		plt.xlabel('1/dx',fontsize=textFontSize*1.5)	
		plt.ylabel('RMS',fontsize=textFontSize*1.5)	
		plt.setp(ax.get_xticklabels(),fontsize=textFontSize*1.2)
		plt.setp(ax.get_yticklabels(),fontsize=textFontSize*1.2)	
		fig_name = 'p1_4.png'
		figure_path = './../figure/'
		fig_fullpath = figure_path + fig_name
		fig.tight_layout()
		plt.savefig(fig_fullpath)
		plt.show()
		plt.close()
		print fig_name+' saved!'	

