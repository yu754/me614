import os
import sys
import numpy as np 
import scipy.sparse as scysparse
from pdb import set_trace as keyboard 
import matplotlib.pyplot as plt
import copy
def Lagr_base(xg,x):
	h=np.ones((len(xg)))
	for j in range(len(xg)):
		for i in range(len(xg)):
			if (i!=j):
				m=(x-xg[i])/(xg[j]-xg[i])
				h[j]=h[j]*m
	return h
# first order base func need orign y_value		

def Lagr_der_base(xl,x):
	k={}
	h=np.ones((len(xl)))		

	temp=np.zeros(len(xl))		

	for j in range(len(xl)):
		b=np.ones((len(xl)-1))		

		xx=np.ones((len(xl)-1))*x
		for i in range(len(xl)):
			k=list(copy.deepcopy(xl))
			bb=1
			k.remove(xl[j])
			b=xl[j]-k
			t=xx-k
			tt=np.ones((len(xl)-1))			
			for z,value in enumerate(t):
				bb=bb*b[z]#over
				for zz in range(len(t)):
					if (zz!=z):
						tt[z]=tt[z]*t[zz]
		temp[j]=np.sum(tt)
		h[j]=temp[j]/bb
		#set_trace()
	return h	

def Vandm_mathix(xx):

	return h		