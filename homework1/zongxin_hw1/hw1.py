import os
import sys
import numpy as np 
import scipy.sparse as scysparse
from pdb import set_trace
import spac_disc as sd 
import matplotlib.pyplot as plt
from matplotlib import rc as matplotlibrc
import p1 as PP1
import p2 as PP2
import p3 as PP3

PP1.Problem1()

PP2.Problem2()

PP3.Problem3()