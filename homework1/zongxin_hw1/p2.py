import os
import sys
import numpy as np 
import scipy.sparse as scysparse
from pdb import set_trace
import spac_disc as sd 
import matplotlib.pyplot as plt
from matplotlib import rc as matplotlibrc
from scipy import sparse


def Problem2():
	matplotlibrc('text.latex', preamble='\usepackage{color}')
	matplotlibrc('text',usetex=True)
	matplotlibrc('font', family='serif')

	fig_width = 8
	fig_height = 8
	textFontSize   = 10*1.5
	gcafontSize    = 26
	lineWidth      = 2  

	Accu_2 = 1
	Accu_3 = 1


	N0=10
	n=10
	rms_2=np.zeros(n)
	rms_3=np.zeros(n)
	dx_list=np.zeros(n)
	for it in range(n):
		N=N0*2**it
		x=np.linspace(0,1,N)
		dx=x[2]-x[1]
		f=np.cos(4*np.pi*x)
		fx=-4*np.pi*np.sin(4*np.pi*x)
		fxx=-16*np.pi*np.pi*np.cos(4*np.pi*x)

		#	Second order Matrix:	A_3	(3 order accuracy)
		#	interior:	Ai1::-1,0,1,2,3 :	    11,-20,6,4,-1	12dx^2
		#	interior:	Ai2::-3,-2-1,0,1:	    -1,4,6,-20,11
		#	boundary:	Ab1:: 0,1,2,3,4 :	 	35,-104,114,-56,11	:12dx^2
		#	boundary:	Ab2::--4,-3,-2,-1,0:	11,-56,114,-104,35
		A_3=np.zeros((N,N))
		ai1=np.array([11,-20,6,4,-1])
		ai2=np.array([-1,4,6,-20,11])
		bi1=np.array([35,-104,114,-56,11])
		bi2=np.array([11,-56,114,-104,35])
		for i in range(N):
			if ((i!=0)&(i!=N-1)):
				if (i<np.int(N/2)):
					A_3[i,i-1:i+4]=ai1
				else:
					A_3[i,i-3:i+2]=ai2
		#A[0,0:5]=bi1
		#A[N-1,N-5:]=bi2	

		A_3=A_3/(12*dx**2)		

		if (it==0):		
			sA_3 = sparse.csr_matrix(A_3)
			sA_3[0,0:5]=1
			sA_3[-1,-5:]=1		




		#	Second order Matrix:	A_2	(2 order accuracy)
		#	interior:	Ai1::-1,0,1 :	    1,-2,1 :dx^2

		A_2=np.zeros((N,N))
		ai1=np.array([1,-2,1])

		for i in range(N):
			if ((i!=0)&(i!=N-1)):
				A_2[i,i-1:i+2]=ai1
		#A[0,0:5]=bi1
		#A[N-1,N-5:]=bi2	

		A_2=A_2/(dx**2)	

		if (it==0):		
			sA_2 = sparse.csr_matrix(A_2)
			sA_2[0,0:4]=1
			sA_2[-1,-4:]=1	

		#	First order Matrix:	B_2	(2 order accuracy)
		#	interior:	Bi1::-1,0,1:	    -1,0,1:2dx

		#	boundary:	Bb1:: 0,1,2:	 	-3,4,-1:6dx
		#	boundary:	Bb2:: -2,-1,0:		1,-4,3
		B_2=np.zeros((N,N))
		bi1=np.array([-1,0,1])
		bb1=np.array([-3,4,-1])
		bb2=np.array([1,-4,3])
		for i in range(N):
			if ((i!=0)&(i!=N-1)):

				B_2[i,i-1:i+2]=bi1

		#B[0,0:3]=bb1
		B_2[N-1,N-3:]=bb2	

		B_2=B_2/(2*dx)	
		B_2[-1,:]=B_2[-1,:]/3
		if (it==0):		
			sB_2 = sparse.csr_matrix(B_2)
			sB_2[0,0:3]=1
			sB_2[-1,-3:]=1	

		#	First order Matrix:	B_3	(3 order accuracy)
		#	interior:	Bi1::-1,0,1,2:	    -2,-3,6,-1::6dx
		#	interior:	Bi2::-2,1,0,1:	    1,-6,3,2::6dx
		#	boundary:	Bb1:: 0,1,2,3:	 	-11,18,-9,2	
		#	boundary:	Bb2:: -3,-2,-1,0:		-2,9,-18,11::6dx
		B_3=np.zeros((N,N))
		bi1=np.array([-2,-3,6,-1])
		bi2=np.array([1,-6,3,2])	
		bb1=np.array([-11,18,-9,2	])
		bb2=np.array([-2,9,-18,11])
		for i in range(N):
			if ((i!=0)&(i!=N-1)):
				if (i<np.int(N/2)):
					B_3[i,i-1:i+3]=bi1
				else:
					B_3[i,i-2:i+2]=bi2
		#A[0,0:5]=bi1
		#A[N-1,N-5:]=bi2	
		B_3[-1,N-4:]=bb2	

		B_3=B_3/(6*dx)	

		if (it==0):		
			sB_3 = sparse.csr_matrix(B_3)
			sB_3[0,0:4]=1
			sB_3[-1,-4:]=1	


		if Accu_2:
			A=A_2*1.0
			B=B_2*1.0

			C0=A+B
			C0[0,0]=1

			q0=fx+fxx
			q0[0]=f[0]
			q0[-1]=fx[-1]



			f_sol=(np.linalg.inv(C0)).dot(q0)
			rms_2[it]=(np.mean((f_sol-f)**2))**0.5

			if False:
				C0=A+B
				q0=fx+fxx
				q0[0]=f[0]
				q0[-1]=fx[-1]
				C1=C0*1.0

				C1[1,:]=C0[1,:]-C0[1,0]*1.0
				C1[-2,:]=C0[-2,:]-C0[-1,:]/C0[-1,-1]*C0[-2,-1]

				q1=q0*1.0
				q1[1]=q1[1]-C0[1,0]*q1[0]
				q1[-2]=q1[-2]-C0[-2,-1]*q1[-1]

				C=1.0*C1[1:N-1,1:N-1]
				q=1.0*q1[1:N-1]

				f_sol=(np.linalg.inv(C)).dot(q)
				rms_2[it]=(np.mean((f_sol-f[1:N-1])**2))**0.5

		if Accu_3:
			A=A_3*1.0
			B=B_3*1.0

			C0=A+B
			C0[0,0]=1

			q0=fx+fxx
			q0[0]=f[0]
			q0[-1]=fx[-1]



			f_sol=(np.linalg.inv(C0)).dot(q0)
			rms_3[it]=(np.mean((f_sol-f)**2))**0.5	
		dx_list[it]=dx


	fig = plt.figure(0,figsize=(fig_width*0.8,fig_height*0.5))
	ax = fig.add_subplot(111)

	ax.loglog(dx_list**(-1), rms_2,'r-',marker='+',ms='7',lw=lineWidth,label='2nd RMS')
	ax.loglog(dx_list**(-1), rms_3,'k-',marker='+',ms='7',lw=lineWidth,label='3th RMS')
	ax.loglog(dx_list**(-1), dx_list**(2)*100,'g--',label='2nd ref')  
	ax.loglog(dx_list**(-1), dx_list**(3)*1000,'b--',label='3th ref')
	handles, labels = ax.get_legend_handles_labels()
	ax.legend(handles, labels,fontsize=textFontSize,loc=0,numpoints=1)

	ax.set_xlabel(r'$1/dx$',fontsize=gcafontSize)
	ax.set_ylabel(r'$RMS$',fontsize=gcafontSize)
	plt.setp(ax.get_xticklabels(),fontsize=textFontSize)
	plt.setp(ax.get_yticklabels(),fontsize=textFontSize)
	fig.tight_layout()
	fig_name = 'p2_rms.png'
	figure_path = './../figure/'
	fig_fullpath = figure_path + fig_name
	plt.savefig(fig_fullpath)
	plt.close()
	print fig_name+' saved!'	 


	fig = plt.figure(0,figsize=(fig_width,fig_height))

	ax = fig.add_subplot(221)
	ax.spy(sB_2)
	plt.title('Dx 2 order accuracy')

	ax = fig.add_subplot(222)
	ax.spy(sB_3)
	plt.title('Dx 3 order accuracy')

	ax = fig.add_subplot(223)
	ax.spy(sA_2)
	plt.title('Dxx 2 order accuracy')

	ax = fig.add_subplot(224)
	ax.spy(sA_3)
	plt.title('Dxx 3 order accuracy')


	fig.tight_layout()
	fig_name = 'p2_spy.png'
	figure_path = './../figure/'
	fig_fullpath = figure_path + fig_name
	plt.savefig(fig_fullpath)
	plt.close()
	print fig_name+' saved!'
	plt.show()







