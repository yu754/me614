import numpy as np
import scipy.sparse as scysparse
import sys
from pdb import set_trace as keyboard

def Generate_Weights(x_stencil,x_eval,der_order):

    if x_stencil.ndim>1:
        sys.exit("stencil array is not a 1D numpy array")

    derivation_order = int(der_order) # making sure derivation order is integer
    polynomial_order = len(x_stencil)-1
    
    weights	= np.zeros(x_stencil.shape)
    N		= x_stencil.size

    for ix,x in enumerate(x_stencil):
        base_func = np.zeros(N,)
        base_func[ix] = 1.0
        poly_coefs = np.polyfit(x_stencil,base_func,polynomial_order)
        weights[ix] = np.polyval(np.polyder(poly_coefs,der_order),x_eval)

    return weights


############################################################
############################################################


def Generate_Spatial_Operators(x_mesh,order_scheme,der_order):

    N = x_mesh.size

    # you should pre-allocate a sparse matrix predicting already the number of non-zero entries
    A = scysparse.lil_matrix((N, N), dtype=np.float64)

    if order_scheme == "2nd-order":

        for i,x_eval in enumerate(x_mesh):
        
            if i==0:
                x_stencil = x_mesh[:3] # this includes points 0,1,2
            
            if i==N-1:
                x_stencil = x_mesh[-3:] # this includes points 0,1,2
        
            else:
                x_stencil = None
            
            print "Here is where you loop over the points of the stencil and deploy your weights in the matrix"

    # convering to csr format, which appears to be more efficient for matrix operations
    return A.tocsr()