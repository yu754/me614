import os
import sys
import numpy as np   # library to handle arrays like Matlab
import scipy.sparse as scysparse
from pdb import set_trace as keyboard # pdb package allows you to interrupt the python script with the keyboard() command
import spatial_discretization as sd # like include .h file in C++, calls another file

Plot_Problem1 = True
Plot_Problem2 = False
Plot_Problem3 = False

############################################################
############################################################

if Plot_Problem1:

    N = 6   # what are the 
    L = 0.1
    x_stencil = np.linspace(-L,L,N)

    x_eval  = 0.0
    f = np.sin(2.*x_stencil+0.1)
    dfdx_analytical = 2.*np.cos(x_eval+0.1)
    
    derivation_order = 1
    w_der0 = sd.Generate_Weights(x_stencil,x_eval,derivation_order)
           
    dfdx_hat = w_der0.dot(f)
    
    print "dfdx_hat:        " + "%2.18f" %dfdx_hat
    print "dfdx_analytical: " + "%2.18f" %dfdx_analytical
    
    discretization_error = np.abs(dfdx_hat-dfdx_analytical)
    
    discretization_error_percent = discretization_error/dfdx_analytical*100.
    
    # feeling nostalgic of printf-based string formatting..
    print "error is: " + "%2.8f" %discretization_error_percent + " %"

#    keyboard() # feeling nostalgic of Matlab here..

    # Use sample script in Homework 0 to plot results in publication quality

############################################################
############################################################

if Plot_Problem2:

    N = 1000
    x_mesh = np.linspace(-.3,.3,N)
    order_scheme = "2nd-order"
    derivative_order = 1
    D = sd.Generate_Spatial_Operators(x_mesh,order_scheme,derivative_order)
    # do something like: ux_hat = D.dot(f)
    keyboard() # <<<<<
    sys.exit("Nothing here yet...")