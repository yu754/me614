import os
import sys
import numpy as np 
import scipy.sparse as scysparse
from pdb import set_trace
#import spac_disc as sd 
import matplotlib.pyplot as plt
from matplotlib import rc as matplotlibrc
from scipy import sparse
from mpi4py import MPI

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

def get_MPI_extent(Nglobal, myrank=rank, totalsize = size):
  Nlocal = int(Nglobal/totalsize)
  if myrank == (totalsize-1):
    extent = np.arange(0, Nglobal, dtype=int)[Nlocal*myrank:]
  else:
    extent = np.arange(0, Nglobal, dtype=int)[Nlocal*(myrank):Nlocal*(myrank+1)]
  return extent

m=6
RMS=np.zeros(m)
for ii in range(m):
  print ii
  N=16**(ii+1)
  x=np.linspace(0,1,N,endpoint=False)
  dx=x[2]-x[1]
  f=np.cos(4*np.pi*x)
  fx=-4*np.pi*np.sin(4*np.pi*x)

  myextent = get_MPI_extent(N) 
  n=len(myextent)
  my_x=x[myextent]
  my_f=f[myextent]
  my_fx=fx[myextent]

  if (size==1):
    a=f[-1]
    b=f[0]
  else:
    if (rank==0):
      a=f[-1]
      b=f[myextent[-1]+1]
    elif (rank==size-1):
      a=f[myextent[0]-1]
      b=f[0]
    else:
      a=f[myextent[0]-1]
      b=f[myextent[-1]+1]
       
  my_fg=np.zeros(n+2)
  my_fg[0]=a
  my_fg[n+1]=b 
  my_fg[1:n+1]=my_f



  my_fx_sol=(my_fg[2:n+2]-my_fg[0:n])/(2*dx)

  my_rms=np.sum((my_fx_sol-my_fx)**2)

  rms=np.zeros(1)

  comm.Reduce(my_rms,rms,root=0,op=MPI.SUM)

  if (rank==0):
    RMS[ii]=(1.0*rms/N)**0.5

if (rank==0):  
  ppath="./../data/"  
  fout_name="p1_1_"+str(size)+".dat"
  fout = open(ppath+fout_name, 'w')
  for k in range(len(RMS)):
     fout.write(str(RMS[k])+" "+"\n")
  fout.close()

  ############################################################################################################
  ############################################################################################################
  ############################################################################################################   

N=1024*64000
#x=np.linspace(0,1,N,endpoint=False)
dx=1./N
#f=np.cos(4*np.pi*x)
#fx=-4*np.pi*np.sin(4*np.pi*x)


myextent = get_MPI_extent(N) 
n=len(myextent)
my_x=dx*myextent
my_f=np.cos(4*np.pi*my_x)
my_fx=-4*np.pi*np.sin(4*np.pi*my_x)


t1=time.time()

x0=0
xn=dx*N
a=np.cos(4*np.pi*xn)
b=np.cos(4*np.pi*x0)
if (size!=1):
  if (rank==0):
    b=np.cos(4*np.pi*(my_x[-1]+dx))
  elif (rank==size-1):
    a=np.cos(4*np.pi*(my_x[0]-dx))
  else:
    a=np.cos(4*np.pi*(my_x[0]-dx))
    b=np.cos(4*np.pi*(my_x[-1]+dx))

t2=time.time()


my_fg=np.zeros(n+2)
my_fg[0]=a
my_fg[n+1]=b 
my_fg[1:n+1]=my_f

my_fx_sol=(my_fg[2:n+2]-my_fg[0:n])/(2*dx)

my_rms=np.sum((my_fx_sol-my_fx)**2)

rms=np.zeros(1)

t3=time.time()
#comm.Reduce(my_rms,rms,root=0,op=MPI.SUM)
rms=comm.allreduce(my_rms,op=MPI.SUM)

if (rank==0):
  rms=(1.0*rms/N)**0.5
  t5=time.time()

  t55=t5-t1
  t21=t2-t1
  t32=t3-t2
  t53=t5-t3
  print "begin time=",t21
  print "compute time =",t32
  print "reduce time=",t53
  print "total mpi=",t21+t53
  print "total time=",t55


  ############################################################################################################
  ############################################################################################################
  ############################################################################################################   



N=100000*size
#x=np.linspace(0,1,N,endpoint=False)
dx=1./N
#f=np.cos(4*np.pi*x)
#fx=-4*np.pi*np.sin(4*np.pi*x)


myextent = get_MPI_extent(N) 
n=len(myextent)
my_x=dx*myextent
my_f=np.cos(4*np.pi*my_x)
my_fx=-4*np.pi*np.sin(4*np.pi*my_x)


t1=time.time()

x0=0
xn=dx*N
a=np.cos(4*np.pi*xn)
b=np.cos(4*np.pi*x0)
if (size!=1):
  if (rank==0):
    b=np.cos(4*np.pi*(my_x[-1]+dx))
  elif (rank==size-1):
    a=np.cos(4*np.pi*(my_x[0]-dx))
  else:
    a=np.cos(4*np.pi*(my_x[0]-dx))
    b=np.cos(4*np.pi*(my_x[-1]+dx))


my_fg=np.zeros(n+2)
my_fg[0]=a
my_fg[n+1]=b 
my_fg[1:n+1]=my_f

my_fx_sol=(my_fg[2:n+2]-my_fg[0:n])/(2*dx)

my_rms=np.sum((my_fx_sol-my_fx)**2)

rms=np.zeros(1)

comm.Reduce(my_rms,rms,root=0,op=MPI.SUM)

if (rank==0):
  rms=(1.0*rms/N)**0.5
  t5=time.time()
  t55=t5-t1
  print t55

  ############################################################################################################
  ############################################################################################################
  ############################################################################################################ 
m=8
RMS=np.zeros(m)
for ii in range(m):
  print ii
  N=10**(ii+1)
  x=np.linspace(0,1,N,endpoint=False)
  dx=x[2]-x[1]
  f=np.cos(4*np.pi*x)
  fx=-4*np.pi*np.sin(4*np.pi*x)

  myextent = get_MPI_extent(N) 
  n=len(myextent)
  my_x=x[myextent]
  my_f=f[myextent]
  my_fx=fx[myextent]
  my_fg=my_f*1.0
  if (size!=1):
    tail=my_f[-1]
    head=my_f[0]
    tail_recv=np.empty(tail.size, dtype=np.float64) 
    head_recv=np.empty(head.size, dtype=np.float64) 

    s_tail_tag = (rank)
    r_tail_tag = (rank-1) if (rank!=0) else (size-1)

    s_head_tag = (rank+1000)
    r_head_tag = (rank+1+1000) if (rank!=size-1) else (1000)

    head_dest = rank-1  if (rank!=0) else (size-1)
    tail_dest = rank+1  if (rank!=size-1) else (0)

    comm.Send(tail,dest=tail_dest,tag=s_tail_tag)
    comm.Send(head,dest=head_dest,tag=s_head_tag)

    comm.Recv(tail_recv,source=head_dest,tag=r_tail_tag)
    comm.Recv(head_recv,source=tail_dest,tag=r_head_tag)

    b= head_recv
    a= tail_recv
  else:
    b=f[0]
    a=f[-1]

  my_fg=np.zeros(n+2)
  my_fg[0]=a
  my_fg[n+1]=b 
  my_fg[1:n+1]=my_f

  my_fx_sol=(my_fg[2:n+2]-my_fg[0:n])/(2*dx)

  my_rms=np.sum((my_fx_sol-my_fx)**2)

  rms=np.zeros(1)

  comm.Reduce(my_rms,rms,root=0,op=MPI.SUM)

  if (rank==0):
    RMS[ii]=(1.0*rms/N)**0.5
    print "i=",ii,RMS[ii]
if (rank==0):  

  ppath="./../data/"  
  fout_name="p2_1_"+str(size)+".dat"
  fout = open(ppath+fout_name, 'w')
  for k in range(len(RMS)):
     fout.write(str(RMS[k])+" "+"\n")
  fout.close()

  ############################################################################################################
  ############################################################################################################
  ############################################################################################################ 

N=1024*64000
#x=np.linspace(0,1,N,endpoint=False)
dx=1./N
#f=np.cos(4*np.pi*x)
#fx=-4*np.pi*np.sin(4*np.pi*x)



myextent = get_MPI_extent(N) 
n=len(myextent)
my_x=dx*myextent
my_f=np.cos(4*np.pi*my_x)
my_fx=-4*np.pi*np.sin(4*np.pi*my_x)

t1=time.time()
if (size!=1):
  tail=my_f[-1]
  head=my_f[0]
  tail_recv=np.empty(tail.size, dtype=np.float64) 
  head_recv=np.empty(head.size, dtype=np.float64) 

  s_tail_tag = (rank)
  r_tail_tag = (rank-1) if (rank!=0) else (size-1)

  s_head_tag = (rank+1000)
  r_head_tag = (rank+1+1000) if (rank!=size-1) else (1000)

  head_dest = rank-1  if (rank!=0) else (size-1)
  tail_dest = rank+1  if (rank!=size-1) else (0)

  comm.Send(tail,dest=tail_dest,tag=s_tail_tag)
  comm.Send(head,dest=head_dest,tag=s_head_tag)

  comm.Recv(tail_recv,source=head_dest,tag=r_tail_tag)
  comm.Recv(head_recv,source=tail_dest,tag=r_head_tag)

  b= head_recv
  a= tail_recv
else:
  x0=0
  xn=dx*N
  a=np.cos(4*np.pi*xn)
  b=np.cos(4*np.pi*x0)

t2=time.time()

my_fg=np.zeros(n+2)
my_fg[0]=a
my_fg[n+1]=b 
my_fg[1:n+1]=my_f

my_fx_sol=(my_fg[2:n+2]-my_fg[0:n])/(2*dx)

my_rms=np.sum((my_fx_sol-my_fx)**2)

rms=np.zeros(1)
t3=time.time()
#comm.Reduce(my_rms,rms,root=0,op=MPI.SUM)
rms=comm.allreduce(my_rms,op=MPI.SUM)
if (rank==0):
  rms=(1.0*rms/N)**0.5
  t5=time.time()
  t55=t5-t1
  t21=t2-t1
  t32=t3-t2
  t53=t5-t3
  print "begin time=",t21
  print "compute time =",t32
  print "reduce time=",t53
  print "total mpi=",t21+t53
  print "total time=",t55

  ############################################################################################################
  ############################################################################################################
  ############################################################################################################ 

N=100000*size
#x=np.linspace(0,1,N,endpoint=False)
dx=1./N
#f=np.cos(4*np.pi*x)
#fx=-4*np.pi*np.sin(4*np.pi*x)



myextent = get_MPI_extent(N) 
n=len(myextent)
my_x=dx*myextent
my_f=np.cos(4*np.pi*my_x)
my_fx=-4*np.pi*np.sin(4*np.pi*my_x)

t1=time.time()
if (size!=1):
  tail=my_f[-1]
  head=my_f[0]
  tail_recv=np.empty(tail.size, dtype=np.float64) 
  head_recv=np.empty(head.size, dtype=np.float64) 

  s_tail_tag = (rank)
  r_tail_tag = (rank-1) if (rank!=0) else (size-1)

  s_head_tag = (rank+1000)
  r_head_tag = (rank+1+1000) if (rank!=size-1) else (1000)

  head_dest = rank-1  if (rank!=0) else (size-1)
  tail_dest = rank+1  if (rank!=size-1) else (0)

  comm.Send(tail,dest=tail_dest,tag=s_tail_tag)
  comm.Send(head,dest=head_dest,tag=s_head_tag)

  comm.Recv(tail_recv,source=head_dest,tag=r_tail_tag)
  comm.Recv(head_recv,source=tail_dest,tag=r_head_tag)

  b= head_recv
  a= tail_recv
else:
  x0=0
  xn=dx*N
  a=np.cos(4*np.pi*xn)
  b=np.cos(4*np.pi*x0)


my_fg=np.zeros(n+2)
my_fg[0]=a
my_fg[n+1]=b 
my_fg[1:n+1]=my_f

my_fx_sol=(my_fg[2:n+2]-my_fg[0:n])/(2*dx)

my_rms=np.sum((my_fx_sol-my_fx)**2)

rms=np.zeros(1)

comm.Reduce(my_rms,rms,root=0,op=MPI.SUM)

if (rank==0):
  rms=(1.0*rms/N)**0.5
  t5=time.time()
  t55=t5-t1
  print t55   

  ############################################################################################################
  ############################################################################################################
  ############################################################################################################  





matplotlibrc('text.latex', preamble='\usepackage{color}')
matplotlibrc('text',usetex=True)
matplotlibrc('font', family='serif')

fig_width = 8
fig_height = 8
textFontSize   = 10*1.5
gcafontSize    = 26
lineWidth      = 2  

nf=5
kk=2**np.array(range(nf))
a=np.zeros((nf,8))
fpath="./../data/"
for i in range (nf):
  fname="p1_1_"+str(kk[i])+".dat"
  m=np.loadtxt(fpath+fname)
  a[i,0:len(m)]=np.loadtxt(fpath+fname)

x=10.**(np.array(range(8))+1)
xx=16.**(np.array(range(6))+1)

fig = plt.figure(0,figsize=(fig_width*0.8,fig_height*0.5))
ax = fig.add_subplot(111)
ax.loglog(x,a[0,:],label='F:N=1')
ax.loglog(x,a[1,:],label='F:N=2')
ax.loglog(x,a[2,:],label='F:N=4')
ax.loglog(x,a[3,:],label='F:N=8')
ax.loglog(xx,a[4,0:-2],label='F:N=16')

nf=5
kk=2**np.array(range(nf))
b=np.zeros((nf,8))
fpath="./../data/"
for i in range (nf):
  fname="p2_1_"+str(kk[i])+".dat"
  m=np.loadtxt(fpath+fname)
  b[i,0:len(m)]=np.loadtxt(fpath+fname)

ax.loglog(x,b[0,:],label='T:N=1')
ax.loglog(x,b[1,:],label='T:N=2')
ax.loglog(x,b[2,:],label='T:N=4')
ax.loglog(x,b[3,:],label='T:N=8')
ax.loglog(xx,b[4,0:-2],label='T:N=16')
y=x**(-2)
ax.loglog(x,y,'r--',label="ref:2nd order")

#handles, labels = ax.get_legend_handles_labels()
#ax.legend(handles, labels,fontsize=textFontSize,loc=0,numpoints=1)
leg = plt.legend(loc='best', ncol=3, mode="expand", shadow=True, fancybox=True)
leg.get_frame().set_alpha(0.5)

plt.xlabel('1/dx',fontsize=textFontSize*1.5)  
plt.ylabel('RMS',fontsize=textFontSize*1.5) 
plt.setp(ax.get_xticklabels(),fontsize=textFontSize*1.2)
plt.setp(ax.get_yticklabels(),fontsize=textFontSize*1.2)  
fig_name = 'p1.png'
figure_path = './../figure/'
fig_fullpath = figure_path + fig_name
fig.tight_layout()
plt.savefig(fig_fullpath)
plt.show()
plt.close()
print fig_name+' saved!'

  ############################################################################################################
  ############################################################################################################
  ############################################################################################################  


fig = plt.figure(0,figsize=(fig_width*0.8,fig_height*0.5))
ax = fig.add_subplot(111)

nf=12
a=np.zeros((12,nf))
fpath="./../data/"
for i in range (nf):
  fname="loop_"+str(i+1)+".dat"
  a[:,i]=np.loadtxt(fpath+fname)

n=2**np.array(range(6))
aa=np.mean(a,axis=1)
a1=aa[0:23:2]
a2=aa[1:24:2]
plt.plot(n,a1[0]/(a1*n),'b--',marker='o',mfc="None",label="F:Strong scaling ")
plt.plot(n,a2[0]/(a2*n),'b-',marker='o',label="T:Strong scaling ")

nf=12
b=np.zeros((12,nf))
fpath="./../data/"
for i in range (nf):
  fname="weak_"+str(i+1)+".dat"
  b[:,i]=np.loadtxt(fpath+fname)

n=2**np.array(range(6))
bb=np.mean(b,axis=1)
b1=bb[0:23:2]
b2=bb[1:24:2]
plt.plot(n,b1[0]/(b1),'g--',marker='s',mfc="None",label="F:Weak scaling ")
plt.plot(n,b2[0]/(b2),'g-',marker='s',label="T:Weak scaling ")

ax.axhline(0.5,0,1,ls='--',c='k')
ax.axhline(0.75,0,1,ls='--',c='k')
ax.axhline(1.0,0,1,ls='--',c='k')

ax.text(27,0.4,'e=0.5',fontsize=textFontSize*1.2)
ax.text(27,0.65,'e=0.75',fontsize=textFontSize*1.2)
ax.text(27,0.9,'e=1.0',fontsize=textFontSize*1.2)

leg = plt.legend(loc='lower center', ncol=2, mode="expand", shadow=False, fancybox=True,fontsize=textFontSize)
leg.get_frame().set_alpha(0.8)

plt.xlabel('N',fontsize=textFontSize*1.5) 
plt.ylabel('efficiency',fontsize=textFontSize*1.5)  
plt.setp(ax.get_xticklabels(),fontsize=textFontSize*1.2)
plt.setp(ax.get_yticklabels(),fontsize=textFontSize*1.2)  
fig_name = 'p2.png'
figure_path = './../figure/'
fig_fullpath = figure_path + fig_name
fig.tight_layout()
plt.savefig(fig_fullpath)
plt.show()
plt.close()
print fig_name+' saved!'
