import os
import sys
import numpy as np 
import scipy.sparse as scysparse
from pdb import set_trace
#import spac_disc as sd 
import matplotlib.pyplot as plt
from matplotlib import rc as matplotlibrc
from scipy import sparse

nf=5
kk=2**np.array(range(nf))
a=np.zeros((nf,8))
fpath="./../data/"
for i in range (nf):
	fname="p1_1_"+str(kk[i])+".dat"
	m=np.loadtxt(fpath+fname)
	a[i,0:len(m)]=np.loadtxt(fpath+fname)

x=10**np.array(range(8)+1)
xx=16**np.array(range(6)+1)
aa=np.mean(a,axis=1)
a1=aa[0:23:2]
a2=aa[1:24:2]
plt.plot(x,a[0,:])
plt.plot(x,a[1,:])
plt.plot(x,a[2,:])
plt.plot(x,a[3,:])
plt.plot(xx,a[4,0:-2])
plt.show()