import os
import sys
import numpy as np 
import scipy.sparse as scysparse
from pdb import set_trace
#import spac_disc as sd 
import matplotlib.pyplot as plt
from matplotlib import rc as matplotlibrc
from scipy import sparse
from mpi4py import MPI
import time

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

def get_MPI_extent(Nglobal, myrank=rank, totalsize = size):
  Nlocal = int(Nglobal/totalsize)
  if myrank == (totalsize-1):
    extent = np.arange(0, Nglobal, dtype=int)[Nlocal*myrank:]
  else:
    extent = np.arange(0, Nglobal, dtype=int)[Nlocal*(myrank):Nlocal*(myrank+1)]
  return extent


N=1024*64000
#x=np.linspace(0,1,N,endpoint=False)
dx=1./N
#f=np.cos(4*np.pi*x)
#fx=-4*np.pi*np.sin(4*np.pi*x)



myextent = get_MPI_extent(N) 
n=len(myextent)
my_x=dx*myextent
my_f=np.cos(4*np.pi*my_x)
my_fx=-4*np.pi*np.sin(4*np.pi*my_x)

t1=time.time()
if (size!=1):
  tail=my_f[-1]
  head=my_f[0]
  tail_recv=np.empty(tail.size, dtype=np.float64) 
  head_recv=np.empty(head.size, dtype=np.float64) 

  s_tail_tag = (rank)
  r_tail_tag = (rank-1) if (rank!=0) else (size-1)

  s_head_tag = (rank+1000)
  r_head_tag = (rank+1+1000) if (rank!=size-1) else (1000)

  head_dest = rank-1  if (rank!=0) else (size-1)
  tail_dest = rank+1  if (rank!=size-1) else (0)

  comm.Send(tail,dest=tail_dest,tag=s_tail_tag)
  comm.Send(head,dest=head_dest,tag=s_head_tag)

  comm.Recv(tail_recv,source=head_dest,tag=r_tail_tag)
  comm.Recv(head_recv,source=tail_dest,tag=r_head_tag)

  b= head_recv
  a= tail_recv
else:
  x0=0
  xn=dx*N
  a=np.cos(4*np.pi*xn)
  b=np.cos(4*np.pi*x0)

t2=time.time()

my_fg=np.zeros(n+2)
my_fg[0]=a
my_fg[n+1]=b 
my_fg[1:n+1]=my_f

my_fx_sol=(my_fg[2:n+2]-my_fg[0:n])/(2*dx)

my_rms=np.sum((my_fx_sol-my_fx)**2)

rms=np.zeros(1)
t3=time.time()
#comm.Reduce(my_rms,rms,root=0,op=MPI.SUM)
rms=comm.allreduce(my_rms,op=MPI.SUM)
if (rank==0):
  rms=(1.0*rms/N)**0.5
  t5=time.time()
  t55=t5-t1
  t21=t2-t1
  t32=t3-t2
  t53=t5-t3
  print "begin time=",t21
  print "compute time =",t32
  print "reduce time=",t53
  print "total mpi=",t21+t53
  print "total time=",t55



