import os
import sys
import numpy as np 
import scipy.sparse as scysparse
from pdb import set_trace
#import spac_disc as sd 
import matplotlib.pyplot as plt
from matplotlib import rc as matplotlibrc
from scipy import sparse
from mpi4py import MPI
import time

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

def get_MPI_extent(Nglobal, myrank=rank, totalsize = size):
  Nlocal = int(Nglobal/totalsize)
  if myrank == (totalsize-1):
    extent = np.arange(0, Nglobal, dtype=int)[Nlocal*myrank:]
  else:
    extent = np.arange(0, Nglobal, dtype=int)[Nlocal*(myrank):Nlocal*(myrank+1)]
  return extent


N=100000*size
#x=np.linspace(0,1,N,endpoint=False)
dx=1./N
#f=np.cos(4*np.pi*x)
#fx=-4*np.pi*np.sin(4*np.pi*x)


myextent = get_MPI_extent(N) 
n=len(myextent)
my_x=dx*myextent
my_f=np.cos(4*np.pi*my_x)
my_fx=-4*np.pi*np.sin(4*np.pi*my_x)


t1=time.time()

x0=0
xn=dx*N
a=np.cos(4*np.pi*xn)
b=np.cos(4*np.pi*x0)
if (size!=1):
  if (rank==0):
    b=np.cos(4*np.pi*(my_x[-1]+dx))
  elif (rank==size-1):
    a=np.cos(4*np.pi*(my_x[0]-dx))
  else:
    a=np.cos(4*np.pi*(my_x[0]-dx))
    b=np.cos(4*np.pi*(my_x[-1]+dx))


my_fg=np.zeros(n+2)
my_fg[0]=a
my_fg[n+1]=b 
my_fg[1:n+1]=my_f

my_fx_sol=(my_fg[2:n+2]-my_fg[0:n])/(2*dx)

my_rms=np.sum((my_fx_sol-my_fx)**2)

rms=np.zeros(1)

comm.Reduce(my_rms,rms,root=0,op=MPI.SUM)

if (rank==0):
  rms=(1.0*rms/N)**0.5
  t5=time.time()
  t55=t5-t1
  print t55

