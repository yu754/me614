import os
import sys
import numpy as np 
import scipy.sparse as scysparse
from pdb import set_trace
#import spac_disc as sd 
import matplotlib.pyplot as plt
from matplotlib import rc as matplotlibrc
from scipy import sparse

matplotlibrc('text.latex', preamble='\usepackage{color}')
matplotlibrc('text',usetex=True)
matplotlibrc('font', family='serif')

fig_width = 8
fig_height = 8
textFontSize   = 10*1.5
gcafontSize    = 26
fig = plt.figure(0,figsize=(fig_width*0.8,fig_height*0.5))
ax = fig.add_subplot(111)

nf=12
a=np.zeros((12,nf))
fpath="./../data/"
for i in range (nf):
	fname="loop_"+str(i+1)+".dat"
	a[:,i]=np.loadtxt(fpath+fname)

n=2**np.array(range(6))
aa=np.mean(a,axis=1)
a1=aa[0:23:2]
a2=aa[1:24:2]
plt.plot(n,a1[0]/(a1*n),'b--',marker='o',mfc="None",label="F:Strong scaling ")
plt.plot(n,a2[0]/(a2*n),'b-',marker='o',label="T:Strong scaling ")

nf=12
b=np.zeros((12,nf))
fpath="./../data/"
for i in range (nf):
	fname="weak_"+str(i+1)+".dat"
	b[:,i]=np.loadtxt(fpath+fname)

n=2**np.array(range(6))
bb=np.mean(b,axis=1)
b1=bb[0:23:2]
b2=bb[1:24:2]
plt.plot(n,b1[0]/(b1),'g--',marker='s',mfc="None",label="F:Weak scaling ")
plt.plot(n,b2[0]/(b2),'g-',marker='s',label="T:Weak scaling ")

ax.axhline(0.5,0,1,ls='--',c='k')
ax.axhline(0.75,0,1,ls='--',c='k')
ax.axhline(1.0,0,1,ls='--',c='k')

ax.text(27,0.4,'e=0.5',fontsize=textFontSize*1.2)
ax.text(27,0.65,'e=0.75',fontsize=textFontSize*1.2)
ax.text(27,0.9,'e=1.0',fontsize=textFontSize*1.2)

leg = plt.legend(loc='lower center', ncol=2, mode="expand", shadow=False, fancybox=True,fontsize=textFontSize)
leg.get_frame().set_alpha(0.8)

plt.xlabel('N',fontsize=textFontSize*1.5)	
plt.ylabel('efficiency',fontsize=textFontSize*1.5)	
plt.setp(ax.get_xticklabels(),fontsize=textFontSize*1.2)
plt.setp(ax.get_yticklabels(),fontsize=textFontSize*1.2)	
fig_name = 'p2.png'
figure_path = './../figure/'
fig_fullpath = figure_path + fig_name
fig.tight_layout()
plt.savefig(fig_fullpath)
plt.show()
plt.close()
print fig_name+' saved!'

