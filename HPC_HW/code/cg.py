import sys
import os
import numpy as np 
import scipy
import copy
from pdb import set_trace
import leinit as leg
import matplotlib.pyplot as plt
from matplotlib import rc as matplotlibrc
from numpy.linalg import inv
import time

A=np.array([4.,1.],[1.,3.])
b=np.array([1.,2.])

r=b-A.dot(x)
p=1.0*r
rs_old=r.dot(r)

for i in range(len(b)):
	Ap=A.dot(p)
	alpha=rs_old/p.dot(Ap)
	x=x+alpha*p
	r=r-alpha*Ap
	rs_new=r.dot(r)
	if (np.sqrt(rs_new)<1e-8):
		break
	beita=rs_new/rs_old
	p=r+beita*p
	rs_old=rs_new

print x	