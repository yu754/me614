import os
import sys
import numpy as np 
import scipy.sparse as scysparse
from pdb import set_trace
#import spac_disc as sd 
import matplotlib.pyplot as plt
from matplotlib import rc as matplotlibrc
from scipy import sparse
matplotlibrc('text.latex', preamble='\usepackage{color}')
matplotlibrc('text',usetex=True)
matplotlibrc('font', family='serif')

fig_width = 8
fig_height = 8
textFontSize   = 10*1.5
gcafontSize    = 26
lineWidth      = 2  

nf=5
kk=2**np.array(range(nf))
a=np.zeros((nf,8))
fpath="./../data/"
for i in range (nf):
	fname="p1_1_"+str(kk[i])+".dat"
	m=np.loadtxt(fpath+fname)
	a[i,0:len(m)]=np.loadtxt(fpath+fname)

x=10.**(np.array(range(8))+1)
xx=16.**(np.array(range(6))+1)

fig = plt.figure(0,figsize=(fig_width*0.8,fig_height*0.5))
ax = fig.add_subplot(111)
ax.loglog(x,a[0,:],label='F:N=1')
ax.loglog(x,a[1,:],label='F:N=2')
ax.loglog(x,a[2,:],label='F:N=4')
ax.loglog(x,a[3,:],label='F:N=8')
ax.loglog(xx,a[4,0:-2],label='F:N=16')

nf=5
kk=2**np.array(range(nf))
b=np.zeros((nf,8))
fpath="./../data/"
for i in range (nf):
	fname="p2_1_"+str(kk[i])+".dat"
	m=np.loadtxt(fpath+fname)
	b[i,0:len(m)]=np.loadtxt(fpath+fname)

ax.loglog(x,b[0,:],label='T:N=1')
ax.loglog(x,b[1,:],label='T:N=2')
ax.loglog(x,b[2,:],label='T:N=4')
ax.loglog(x,b[3,:],label='T:N=8')
ax.loglog(xx,b[4,0:-2],label='T:N=16')
y=x**(-2)
ax.loglog(x,y,'r--',label="ref:2nd order")

#handles, labels = ax.get_legend_handles_labels()
#ax.legend(handles, labels,fontsize=textFontSize,loc=0,numpoints=1)
leg = plt.legend(loc='best', ncol=3, mode="expand", shadow=True, fancybox=True)
leg.get_frame().set_alpha(0.5)

plt.xlabel('1/dx',fontsize=textFontSize*1.5)	
plt.ylabel('RMS',fontsize=textFontSize*1.5)	
plt.setp(ax.get_xticklabels(),fontsize=textFontSize*1.2)
plt.setp(ax.get_yticklabels(),fontsize=textFontSize*1.2)	
fig_name = 'p1.png'
figure_path = './../figure/'
fig_fullpath = figure_path + fig_name
fig.tight_layout()
plt.savefig(fig_fullpath)
plt.show()
plt.close()
print fig_name+' saved!'
