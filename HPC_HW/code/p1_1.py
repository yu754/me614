import os
import sys
import numpy as np 
import scipy.sparse as scysparse
from pdb import set_trace
#import spac_disc as sd 
import matplotlib.pyplot as plt
from matplotlib import rc as matplotlibrc
from scipy import sparse
from mpi4py import MPI

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

def get_MPI_extent(Nglobal, myrank=rank, totalsize = size):
  Nlocal = int(Nglobal/totalsize)
  if myrank == (totalsize-1):
    extent = np.arange(0, Nglobal, dtype=int)[Nlocal*myrank:]
  else:
    extent = np.arange(0, Nglobal, dtype=int)[Nlocal*(myrank):Nlocal*(myrank+1)]
  return extent

m=6
RMS=np.zeros(m)
for ii in range(m):
  print ii
  N=16**(ii+1)
  x=np.linspace(0,1,N,endpoint=False)
  dx=x[2]-x[1]
  f=np.cos(4*np.pi*x)
  fx=-4*np.pi*np.sin(4*np.pi*x)

  myextent = get_MPI_extent(N) 
  n=len(myextent)
  my_x=x[myextent]
  my_f=f[myextent]
  my_fx=fx[myextent]

  if (size==1):
    a=f[-1]
    b=f[0]
  else:
    if (rank==0):
      a=f[-1]
      b=f[myextent[-1]+1]
    elif (rank==size-1):
      a=f[myextent[0]-1]
      b=f[0]
    else:
      a=f[myextent[0]-1]
      b=f[myextent[-1]+1]
       
  my_fg=np.zeros(n+2)
  my_fg[0]=a
  my_fg[n+1]=b 
  my_fg[1:n+1]=my_f



  my_fx_sol=(my_fg[2:n+2]-my_fg[0:n])/(2*dx)

  my_rms=np.sum((my_fx_sol-my_fx)**2)

  rms=np.zeros(1)

  comm.Reduce(my_rms,rms,root=0,op=MPI.SUM)

  if (rank==0):
    RMS[ii]=(1.0*rms/N)**0.5

if (rank==0):  
  ppath="./../data/"  
  fout_name="p1_1_"+str(size)+".dat"
  fout = open(ppath+fout_name, 'w')
  for k in range(len(RMS)):
     fout.write(str(RMS[k])+" "+"\n")
  fout.close()

